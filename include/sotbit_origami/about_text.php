<p style="text-align: left;">
</p>
<p>
</p>
<h1>LeLoo: интернет-магазин&nbsp;</h1>
 <b>Leloo.kz&nbsp;</b>- <i>новый Казахстанский интернет - магазин косметики и бытовой химии. Мы специализируемся на лучших товарах уходовой и декоративной косметики, а также бытовой химии от ведущих брендов из Южной Кореи, таких как:&nbsp;Kerasys, FarmStay, Dr.Jart, Welcos, Missha, Tony Moly, O’Clean, Mukunghwa и многих&nbsp;других.</i> <br>
 <b><span style="font-size: 11pt;"><br>
 </span></b><b><span style="font-size: 11pt;">Наши к</span></b><b><span style="font-size: 11pt;">лючевые особенности:</span></b><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<ul class="fonts__small_text" style="padding-left:15px;">
	<li><span style="font-size: 11pt;">Здесь ты найдешь&nbsp;самую&nbsp;трендовую и востребованную&nbsp;косметику бьюти индустрии из Южной Кореи&nbsp;</span></li>
	<li><span style="font-size: 11pt;">Большой выбор косметики и бытовой химии</span></li>
	<li><span style="font-size: 11pt;">Ассортимент&nbsp;постоянно пересматривается и пополняется&nbsp; &nbsp;</span></li>
</ul>
 <span style="font-size: 11pt;"> </span><i><span style="font-size: 11pt;">LeLoo&nbsp;– твоя&nbsp;территория вдохновения и красоты!&nbsp;</span></i><span style="font-size: 11pt;"> </span><br>
<p style="text-align: right; color: #f16d7e; font-style: italic;">
 <span>С заботой и любовью о тебе и твоих близких!<span><br>
 <b>LeLoo</b> </span></span>
</p>
 <a class="about__more" href="<?= SITE_DIR ?>about/">Узнать больше <i class="fas fa-angle-right about__fas"></i></a>