<picture>
    <source srcset="/upload/static/about-l.webp"  type="image/webp" media="(min-width: 600px)" />
    <source srcset="/upload/static/about-m.webp"  type="image/webp" media="(min-width: 370px)" />
    <source srcset="/upload/static/about-s.webp"  type="image/webp" />
    <img alt="/upload/static/about-l.jpg" src="/upload/static/about-l.jpg" title="about-company.jpg">
</picture>