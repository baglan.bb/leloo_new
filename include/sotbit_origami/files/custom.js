function getCookie(name) {
  let matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

function addNotif(title, text, type){
  if (type == 'once'){
    $(".notifBody").children().addClass('collapsed').eq(1).addClass('second').nextAll().hide();
    $(".notifBody").prepend();
  }
  else{
    $.ajax({
      type: "POST",
      cache: false,
      data: 'ajax_coupon&coupon='+input.val(),
      url: '?',
      success: function (result) {
        result=JSON.parse(result);
        if(result.status==='success'){
          window.reload();
        }
        console.log(result);
      }
    });
  }

}

function hideNotif(){
  $(".notifCentre").slideUp('fast');
  $(".notifBackdrop").fadeOut('fast');
}
function get(iblock, id){
  var data = {
    id: id,
    site_id: 's1',
    iblockId: iblock,
    basketData: 'YToxOntpOjA7czo1OiJPQllFTSI7fQ=='
  };
  $.ajax({
    type       :'POST',
    url        :'https://leloo.kz/include/ajax/oc.php',
    data       :data,
    cache:false,
    success    :function(result) {
      $(".ocBackDrop").fadeIn();
      $("#ocModal").html(result).fadeIn();
    },
    error:function(){
      location.reload();
    }
  });
}
$( document ).ready(function() {
  $(".lazyOnMain").hide();
  $('body').on("click", ".ocBackDrop .ocHead", function (e) {
    $(".ocBackDrop").fadeOut();
    $("#ocModal").html(result).fadeOut();
  })
  $("#payHand").click(function () {
    var params = window.location.search.replace('?', '').split('&').reduce(
      function (p, e) {
        var a = e.split('=')
        p[decodeURIComponent(a[0])] = decodeURIComponent(a[1])
        return p
      },
      {},
    )

    $id = params['ORDER_ID'];
    var sendData = {
      id: $id
    };

    $.ajax({
      url: '/ajax_auth/changePaymentType.php',
      type: 'POST',
      data: ({sendData: sendData}),
      dataType: "html",
      error: function () {
        location.reload();
      },
      success: function () {
        $(".modalSuccessPay").removeClass('d-none');
      }
    })
  })

  if (document.body.clientWidth < 767){
    $('body').on("click", ".product-card-inner__img-wrapper, .product-card-inner__title, .product-card-inner__price", function (e) {
      e.preventDefault();

      if (window.location.pathname == '/' || window.location.pathname == '/index.php')
      {
        $(this).parents('.slick-slide').siblings().find('form').slideUp(250);
      }
      else{
        $('html, body').animate({
          scrollTop: ($(this).offset().top-80)  // класс объекта к которому приезжаем
        }, 600); // Скорость прокрутки
        $(this).parents('.product_card__block_item').siblings().find('form').slideUp(250);
      }


      var form = $(this).siblings('form');
      form.slideToggle(250);
    })
  }
  if (window.location.href.includes('/order/make/?ORDER_ID')){
    $(document).mouseleave(function(e){
      if (e.clientY < 10) {
        $('#exampleModal111').modal('show')
      }
    });
  }


  $(".product-card-inner__in-basket").click(function(){
    ym(56518078,'reachGoal','addedToBasket');
  })
  $(".header-two .header-two__contact-arrow-link, .footer-block .button_call").click(function(){
    ym(56518078,'reachGoal','calback');
  })

  setTimeout(hideNotif, 3000 );

  $(".notifCentre").mouseleave(function () {
    hideNotif();
  })
  $(".notifCentre .open-basket-origami__close").click(function () {
      $(".notifCentre").slideUp('fast');
      $(".notifBackdrop").fadeOut('fast');
  })
  $(".notifBackdrop").click(function () {
    $(".notifCentre").slideUp('fast');
    $(".notifBackdrop").fadeOut('fast');
  })
  $(document).on("mouseenter", ".notif", function (e) {
    $(".notifCentre").slideDown('fast');
    $(".notifBackdrop").fadeIn('fast');
  })
  $(".notifBody").mouseenter(function () {
    $(this).children('.notifCard').removeClass('collapsed').removeClass('second');
  })
  $(".notifBody").mouseleave(function () {
    $(':nth-child(4)', this).addClass('collapsed');
    $(':nth-child(5)', this).addClass('collapsed').addClass('second');
    $(':nth-child(6)', this).addClass('collapsed').addClass('second');
    $(':nth-child(7)', this).addClass('collapsed').addClass('second');
  })


  $(".citiesList .popup-close, .citiesModal").click(function(e){
    if (e.target !== this)
      return;
    $('.citiesModal').fadeOut('fast');
  });

  $("#citySelect, .citySel .btn-warning").click(function(){
    $('.citiesModal').fadeIn('fast');
    $('.citySel').fadeOut('fast');
  })

  $(".citySel .btn-secondary").click(function(){
    document.cookie = "cityChosed=Y";
    $('.citySel').fadeOut('fast');
  })

  if (getCookie('cityChosed') != 'Y'){
    $(".citySel").show();
  }

  $('.citiesList li').not('.active').click(function () {
    // $('div.dropdown,div.dropdown-menu').removeClass('show');
    var newCity = $(this).text();
    $.ajax({
      type: "post",
      cache: false,
      data: 'city=' + newCity,
      url: "/ajax_auth/change_user_city.php?ajax",
      success: function (result) {
        window.location.reload();
      }
    });
    return false;
  });
  //order page
  $("#orderAuth #userPhone").keyup(function(){
    var val = $(this).val();
    if (val[15] != '_' && val[15] != '' && val[1] != 7)
      $.toast({
        hideAfter: 3000,
        heading:'Ошибка',
        bgColor:'#f45366',
        position:'top-right',
        loader:false,
        text : 'Введите корректный номер телефона через +7'
      })
    if (val[15] != '_' && val[1] == 7)
      $("#orderAuth #checkUser").prop("disabled", false);
  })
  $("#orderAuth #password").keyup(function(){
    var val = $(this).val();
    if (val[4] != '_')
      $("#orderAuth #goAuth").prop("disabled", false);
  })

  $("#orderAuth #checkUser").click(function(){
    var phone = $("#orderAuth #userPhone").val(),
      data = {'USER_PHONE':phone},
      icon = $(this).prev().find('.fa'),
      thisItem = $(this);
    if (false)
      alert (2);
    else{
      icon.removeClass('fa-phone').addClass('fa-spinner');
      $.ajax({
        type     :'POST',
        url      :'/ajax_auth/checkUser.php',
        data	 :data,
        cache:false,
        success    :function(result) {
          result=JSON.parse(result);
          icon.removeClass('fa-spinner').addClass('fa-check');
          $("#orderAuth #checkUser").hide();
          thisItem.next().fadeIn();
          if (result.status == 'userExist'){
            $.toast({
              heading:'С возвращением, '+result.message,
              position:'top-right',
              hideAfter: 7000,
              bgColor:'#2dcc70',
              text : "Введите ваш пароль или запросите новый"
            })
          }
          else if(result.status == 'sendPsw'){
            $("#password").next().find('span').text('Не получили SMS?');
            $("#resend").data("action", "resendSMS");
            $("#sendPsw").removeClass('d-none');
            $(".nameFields").removeClass('d-none');
            $("#goAuth").data("url", "/ajax_auth/register.php");
            $.toast({
              heading:'Проверьте телефон',
              position:'top-right',
              hideAfter: 6000,
              bgColor:'#2dcc70',
              text : "Вам было отправелено SMS с паролем"
            })
            resendTimer = 30;
            startSmsTimer();
          }
        },
        error:function(){
          location.reload();
        }
      });
    }
  })
  $("#orderAuth #goAuth").click(function(){
    var
      phone = $("#orderAuth #userPhone").val(),
      password = $("#orderAuth #password").val(),
      name = $("#orderAuth #userName").val(),
      // lastName = $("#orderAuth #userLastName").val(),
      userID = $("#orderAuth #userID").val(),
      data = {'USER_PHONE':phone, 'USER_PASSWORD':password, 'USER_NAME':name, 'FORGOT':userID},
      thisItem = $(this);
    $(".orderLoader").show();
    $.ajax({
      type     :'POST',
      url      :thisItem.data("url"),
      data	   :data,
      cache:false,
      success    :function(result) {
        result=JSON.parse(result);
        if (result.status == 'success'){
          $.toast({
            heading:'Успешно',
            position:'top-right',
            hideAfter: 6000,
            bgColor:'#2dcc70',
            text : "Страница будет перезагружена"
          })
          location.reload();
        }
        else{
          result.message.forEach(function(entry) {
            $.toast({
              hideAfter: 8000,
              heading:'Ошибка',
              bgColor:'#f45366',
              position:'top-right',
              loader:false,
              text : entry
            })
          });
        }
        $(".orderLoader").hide();
      },
      error:function(){
        location.reload();
      }
    })
  })
  $("#orderAuth #resend").click(function(){
    if ($(this).data("action") == "resendSMS"){
      if (resendTimer > 0){
        $.toast({
          heading:'Подождите',
          position:'top-right',
          hideAfter: 4000,
          bgColor:'#cc842d',
          text : "Повторно отправить СМС можно будет через "+resendTimer+" секунд"
        })
      }
      else{
        $("#orderAuth #checkUser").click();
      }
    }
    else{
      if (resendTimer > 0){
        $.toast({
          heading:'Подождите',
          position:'top-right',
          hideAfter: 4000,
          bgColor:'#cc842d',
          text : "Повторно отправить СМС можно будет через "+resendTimer+" секунд"
        })
      }
      else{
        var
          phone = $("#orderAuth #userPhone").val(),
          data = {'USER_PHONE':phone, 'FORGOT':'Y'},
          thisItem = $(this);
        $.ajax({
          type     :'POST',
          url      :'/ajax_auth/checkUser.php',
          data	 :data,
          cache:false,
          success    :function(result) {
            result=JSON.parse(result);
            $("#userID").val(result.id);
            $.toast({
              heading:'Проверьте телефон',
              position:'top-right',
              hideAfter: 6000,
              bgColor:'#2dcc70',
              text : "Вам было отправелено SMS с паролем"
            })
            resendTimer = 30;
            startSmsTimer();
          },
          error:function(){
            location.reload();
          }
        });
      }
    }
  })

  $("#isHome").change( function(){
    if ($(this).prop('checked')){
      $('*[data-property-id-row="24"]').hide().find('input').val('Частный дом');
    }
    else
      $('*[data-property-id-row="24"]').show();
  });

  if(window.location.href.includes('/personal/order/make/')){
    if ($('*[data-property-id-row="24"]').find('input').val() == 'Частный дом'){
      $("#isHome").prop( "checked", true );
      $('*[data-property-id-row="24"]').hide();
    }
  }
  //order page end

  //cabinet
  $("#cabinetForgotPassBtn").click(function(){
    var
      phone = $(".bx-authform-formgroup-container #user_login").val(),
      data = {'USER_PHONE':phone, 'FORGOT':'Y', 'CHECK':'Y'};
    $.ajax({
      type     :'POST',
      url      :'/ajax_auth/checkUser.php',
      data	   :data,
      cache    :false,
      success    :function(result) {
        result=JSON.parse(result);
        if  (result.status == 'sendPsw'){
          $("#uID").val(result.id);
          $.toast({
            heading:'Проверьте телефон',
            position:'top-right',
            hideAfter: 6000,
            bgColor:'#2dcc70',
            text : "Вам было отправелено SMS с паролем"
          })
          resendTimer = 30;
          startSmsTimer();
        }
        else if(result.status == 'userNotFound'){
          $.toast({
            heading:'Ошибка',
            position:'top-right',
            hideAfter: 6000,
            bgColor:'#f45366',
            text : result.message
          })
        }
        else{
          location.reload();
        }
      },
      error:function(){
        location.reload();
      }
    });
  })

  $('#getPass').click(function (e) {
    e.preventDefault();
    var
      thisItem = $(this),
      name = $('input[name="USER_NAME"]').val(),
      lastName = $('input[name="USER_LAST_NAME"]').val(),
      phone = $('input[name="USER_PHONE"]').val(),
      data = {'USER_PHONE': phone},
      error = [];

    if (name.length < 3)
      error.push('Имя не должно быть короче 3-х символов');
    // if (lastName.length < 2)
    //   error.push('Фамилия не должняа быть короче 2-х символов');
    if (phone[15] == '_' || phone[1] != 7)
      error.push('Введите корректный номер телефона');
    if(error.length > 0){
      error.forEach(function(entry) {
        $.toast({
          hideAfter: 8000,
          heading:'Ошибка',
          bgColor:'#f45366',
          position:'top-right',
          loader:false,
          text : entry
        })
      });
    }
    else{
      if (resendTimer > 0){
        $.toast({
          heading:'Подождите',
          position:'top-right',
          hideAfter: 4000,
          bgColor:'#cc842d',
          text : "Повторно отправить СМС можно будет через "+resendTimer+" секунд"
        })
      }
      else{
        $.ajax({
          type: 'POST',
          url: '/ajax_auth/checkUser.php',
          data: data,
          cache: false,
          success: function (result) {
            result=JSON.parse(result);
            if (result.status == 'userExist'){
              $.toast({
                heading:'Пользователь существует',
                position:'top-right',
                hideAfter: 4000,
                bgColor:'#cc842d',
                text : "Пользователь с таким номером уже существует"
              })
            }
            else{
              thisItem.hide();
              $(".hidden-auth").show();
              $.toast({
                heading: 'Проверьте телефон',
                position: 'top-right',
                hideAfter: 6000,
                bgColor: '#2dcc70',
                text: "Вам было отправелено SMS с паролем"
              })
              resendTimer = 30;
              startSmsTimer();
            }
          },
          error: function () {
            location.reload();
          }
        });
      }
    }
  })

  $('#resendPswReg').click(function () {
    if (resendTimer > 0) {
      $.toast({
        heading: 'Подождите',
        position: 'top-right',
        hideAfter: 4000,
        bgColor: '#cc842d',
        text: 'Повторно отправить СМС можно будет через ' + resendTimer +
          ' секунд',
      })
    }
    else {
      $('#getPass').click()
    }

  })
  //cabinet end

});