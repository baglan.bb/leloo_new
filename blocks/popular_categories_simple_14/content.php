<?
global $arrFilter;
use Sotbit\Origami\Helper\Config;

$arrFilter = array();
$arrFilter["UF_SHOW_ON_MAIN_PAGE"] = 1;
$arrFilter[">PICTURE"] = 0;

$block = new SvaBlock(__DIR__);
$settings = $block->getSettings();
$style = $block->getStyle();
?>
<div class="block-wrapper-inner">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"origami_popular_categories_icons",
	array(
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "8640000",
		"CACHE_TYPE" => "A",
		"FILTER_NAME" => "arrFilter",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"COUNT_ELEMENTS" => "N",
		"IBLOCK_ID" => Config::get("IBLOCK_ID"),
		"IBLOCK_TYPE" => Config::get("IBLOCK_TYPE"),
		"SECTION_FIELDS" => array(
			0 => "NAME",
			1 => "PICTURE",
			2 => "SECTION_PAGE_URL",
			3 => "DESCRIPTION",
		),
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "UF_SHOW_ON_MAIN_PAGE",
			2 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => $settings["fields"]["top_depth"]["value"],
		"VIEW_MODE" => "LINE",
		"LINK_TO_THE_CATALOG" => $settings["fields"]["link_catalog"]["value"],
		"BLOCK_NAME" => "Каталог", //$settings["fields"]["title"]["value"]
		"COUNT_SECTIONS" => 16, //$settings["fields"]["count_sections"]["value"]
		"COMPONENT_TEMPLATE" => "origami_popular_categories_simple",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_CODE" => "",
		"CACHE_FILTER" => "Y",
		"SHOW_SUBSECTIONS" => "Y",
		"COUNT_ELEMENTS_FILTER" => "CNT_ACTIVE"
	),
	false
);
?></div><?