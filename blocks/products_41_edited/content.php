<?php
use Sotbit\Origami\Helper\Config;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$block = new SvaBlock(__DIR__);
$settings = $block->getSettings();
$style = $block->getStyle();

$ajaxMode = $settings['fields']['ajax']['value'];
if (!in_array($ajaxMode, array('Y','N')))
    $ajaxMode = 'Y';

$productsNumber = intval($settings['fields']['products_number']['value']);
if(!is_integer($productsNumber))
    $productsNumber = 0;

	$cnt = CIBlockElement::GetList(
		array(),
		array('IBLOCK_ID' => array(11,13), "PROPERTY_DISCOUNT_VALUE" => "TRUE"),
		array(),
		false,
		array('ID', 'NAME')
	);

    ?><div class="block-wrapper-inner" style="<?= $style ?>"><?
        ?><section class="catalog_section_block catalog_section_block_tabs main-container"><?
            $container = "mp-collection";
            if ($cnt>4){
                $containers = Array(
                    "discount" => Array(
                        'container_id' =>'mp-collection__discount',
                        'name' => 'Скидки',
                        'filterName' => 'arFilterDiscount',
                        'filter' => Array(
                            "PROPERTY_DISCOUNT_VALUE" => "TRUE"
                        ),
                        'filterNameUrl' => 'discount-is-true',
						'moreLink'=>'Показать все товары со скидкой'
                    )
                );
			}
            else{
                $containers = Array(
					"hit" => Array(
						'container_id' => 'mp-collection__hit',
						'name' => 'Бестселлеры',
						'filterName' => 'arFilterHit',
						'filter' => Array(
							"!PROPERTY_KHIT" => false
						),
						'filterNameUrl' => 'khit-is-true',
						'moreLink'=>'Все бестселлеры'
					)
                );
			}

            foreach($containers as $containerId => $tab) {
                $filterName = $tab['filterName'];
                global $$filterName;
                $$filterName = $tab['filter'];
            }
            ?>
			<p class="puzzle_block__title fonts__middle_title">
				<?= $tab["name"] ?>
				<a href="/catalog/filter/<?= $tab["filterNameUrl"] ?>/apply/" class="puzzle_block__link fonts__small_text">
                    <?= $tab["moreLink"] ?>
					<i class="icon-nav_1"></i>
				</a>
			</p>

            <div class="tabs_sale_block"><?
                $i = 0;
                foreach($containers as $containerId => $tab) {
                    ?><div class="tabs_sale_block__content <?=($i == 0) ? 'active ' : '' ?>collection-id-<? echo $i ?>"><?
                    $filterName = $tab['filterName'];
                    $intSectionID = $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        "origami_section",
                        [
                            "IBLOCK_ID"                 => Config::get("IBLOCK_ID"),
                            "IBLOCK_TYPE"               => Config::get("IBLOCK_TYPE"),
                            "ELEMENT_SORT_FIELD"        => "shows",
                            "ELEMENT_SORT_ORDER"        => "desc",
                            "ELEMENT_SORT_FIELD2"       => "id",
                            "ELEMENT_SORT_ORDER2"       => "desc",
                            "PROPERTY_CODE"             => [],
                            "PROPERTY_CODE_MOBILE"      => [],
                            "META_KEYWORDS"             => "-",
                            "META_DESCRIPTION"          => "-",
                            "BROWSER_TITLE"             => "-",
                            "SET_LAST_MODIFIED"         => "N",
                            "INCLUDE_SUBSECTIONS"       => "Y",
                            "BASKET_URL"                => Config::get('BASKET_PAGE'),
                            "ACTION_VARIABLE"           => "action",
                            "PRODUCT_ID_VARIABLE"       => "id",
                            "SECTION_ID_VARIABLE"       => "SECTION_ID",
                            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                            "PRODUCT_PROPS_VARIABLE"    => "prop",
                            "FILTER_NAME"               => $filterName,
                            "CACHE_FILTER"              => "Y",
                            "CACHE_GROUPS"              => "Y",
                            "CACHE_TIME"                => "36000000",
                            "CACHE_TYPE"                => "A",
                            "SET_TITLE"                 => "N",
                            "MESSAGE_404"               => "",
                            "SET_STATUS_404"            => "Y",
                            'SHOW_ALL_WO_SECTION' => 'Y',
                            "SHOW_404"                  => "N",
                            "DISPLAY_COMPARE"           => "Y",
                            "PAGE_ELEMENT_COUNT"        => 12,
                            "LINE_ELEMENT_COUNT"        => 4,
                            "PRICE_CODE"                => \SotbitOrigami::GetComponentPrices(["OPT","SMALL_OPT","BASE"]),
                            "USE_PRICE_COUNT"           => "N",
                            "SHOW_PRICE_COUNT"          => "1",
                            "PRICE_VAT_INCLUDE"          => "Y",
                            "USE_PRODUCT_QUANTITY"       => "Y",
                            "ADD_PROPERTIES_TO_BASKET"   => "Y",
                            "PARTIAL_PRODUCT_PROPERTIES" => "N",
                            "PRODUCT_PROPERTIES"         => [],
                            "DISPLAY_TOP_PAGER"               => "N",
                            "DISPLAY_BOTTOM_PAGER"            => "N",
                            "PAGER_TITLE"                     => "������",
                            "PAGER_SHOW_ALWAYS"               => "",
                            "PAGER_TEMPLATE"                  => "",
                            "PAGER_DESC_NUMBERING"            => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
                            "PAGER_SHOW_ALL"                  => "N",
                            "PAGER_BASE_LINK_ENABLE"          => "N",
                            "PAGER_BASE_LINK"                 => "N",
                            "PAGER_PARAMS_NAME"               => "N",
                            "LAZY_LOAD"                       => "N",
                            "MESS_BTN_LAZY_LOAD"              => "N",
                            "LOAD_ON_SCROLL"                  => "N",

                            "OFFERS_CART_PROPERTIES" => [],
                            "OFFERS_FIELD_CODE"      => [],
                            "OFFERS_PROPERTY_CODE"   => [],
                            "OFFERS_SORT_FIELD"      => "sort",
                            "OFFERS_SORT_ORDER"      => "id",
                            "OFFERS_SORT_FIELD2"     => "desc",
                            "OFFERS_SORT_ORDER2"     => "desc",
                            "OFFERS_LIMIT"           => 0,
                            'OFFER_TREE_PROPS' => array(),
                            "SECTION_URL"               => SITE_DIR.'catalog/#SECTION_CODE_PATH#/',
                            "DETAIL_URL"                => SITE_DIR.'catalog/#SECTION_CODE_PATH#/#ELEMENT_CODE#/',
                            "USE_MAIN_ELEMENT_SECTION"  => "N",
                            'CONVERT_CURRENCY'          => "N",
                            'HIDE_NOT_AVAILABLE'        => "Y",
                            'HIDE_NOT_AVAILABLE_OFFERS' => "Y",

                            'LABEL_PROP'                  => [
                                0 => 'KHIT',
                                1 => 'NOVINKA'
                            ],
                            'LABEL_PROP_MOBILE'           => [],
                            'ADD_PICT_PROP'               => "MORE_PHOTO",
                            'PRODUCT_DISPLAY_MODE'        => "Y",
                            'OFFER_ADD_PICT_PROP'         => "MORE_PHOTO",
                            'PRODUCT_SUBSCRIPTION'        => "Y",
                            'SHOW_DISCOUNT_PERCENT'       => "Y",
                            'SHOW_OLD_PRICE'              => "Y",
                            'SHOW_MAX_QUANTITY'           => "M",
                            "MESS_BTN_ADD_TO_BASKET"      => "Добавить в корзину",
                            "MESS_BTN_BUY"                => "Купить",
                            "MESS_BTN_COMPARE"            => "���������",
                            "MESS_BTN_DETAIL"             => "���������",
                            "MESS_BTN_SUBSCRIBE"          => "�����������",
                            "MESS_NOT_AVAILABLE"          => "��� � �������",
                            "MESS_RELATIVE_QUANTITY_MANY" => "В наличии",
                            "MESS_RELATIVE_QUANTITY_FEW"  => "В наличии",
                            "MESS_RELATIVE_QUANTITY_NO"   => "Нет в наличии",
                            'USE_VOTE_RATING'             => "Y",
                            'TEMPLATE_THEME'              => "",
                            "ADD_SECTIONS_CHAIN"          => "N",
                            'ADD_TO_BASKET_ACTION'        => "ADD",
                            'COMPARE_PATH'                => Config::get('COMPARE_PAGE'),
                            'COMPARE_NAME'                => "CATALOG_COMPARE_LIST",
                            'USE_COMPARE_LIST'            => 'Y',
                            'ACTION_PRODUCTS' => array("ADMIN"),
                            'VARIANT_LIST_VIEW' => 'ADMIN',
                            'SHOW_SLIDER' => "Y",
                            'SECTION_NAME' => Loc::getMessage('SECT_BLOG_BLOCK_NAME'),
                            "VARIANT_LIST_VIEW" => "template_1",
                        ],
                        false
                    );
                    ?></div><?
                    $i++;
                }
            ?></div>
<?
//$APPLICATION->IncludeComponent(
//	"sotbit:crosssell.collection",
//	"origami_default",
//	array(
//		"ACTION_VARIABLE" => "action",
//		"ADD_PROPERTIES_TO_BASKET" => "Y",
//		"ADD_SECTIONS_CHAIN" => "N",
//		"AJAX_MODE" => "N",
//		"AJAX_OPTION_ADDITIONAL" => "",
//		"AJAX_OPTION_HISTORY" => "N",
//		"AJAX_OPTION_JUMP" => "N",
//		"AJAX_OPTION_STYLE" => "Y",
//		"BACKGROUND_IMAGE" => "-",
//		"BASKET_URL" => "/personal/cart/",
//		"BROWSER_TITLE" => "-",
//		"CACHE_FILTER" => "N",
//		"CACHE_GROUPS" => "Y",
//		"CACHE_TIME" => "36000000",
//		"CACHE_TYPE" => "A",
//		"COLLECTION_LIST" => array(
//			0 => "e12",
//			1 => "e11",
//			2 => "e13",
//		),
//		"COMPATIBLE_MODE" => "N",
//		"CONVERT_CURRENCY" => "N",
//		"DETAIL_URL" => "",
//		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
//		"DISPLAY_BOTTOM_PAGER" => "N",
//		"DISPLAY_COMPARE" => "Y",
//		"DISPLAY_TOP_PAGER" => "N",
//		"ELEMENT_SORT_FIELD" => "sort",
//		"ELEMENT_SORT_FIELD2" => "id",
//		"ELEMENT_SORT_ORDER" => "asc",
//		"ELEMENT_SORT_ORDER2" => "desc",
//		"FILTER_NAME" => "arrFilter",
//		"HIDE_NOT_AVAILABLE" => "N",
//		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
//		"IBLOCK_ID" => Config::get("IBLOCK_ID"),
//		"IBLOCK_TYPE" => Config::get("IBLOCK_TYPE"),
//		"INCLUDE_SUBSECTIONS" => "Y",
//		"LINE_ELEMENT_COUNT" => "5",
//		"PAGE_ELEMENT_COUNT" => $productsNumber,
//		"MESSAGE_404" => "",
//		"META_DESCRIPTION" => "-",
//		"META_KEYWORDS" => "-",
//		"OFFERS_CART_PROPERTIES" => array(
//			0 => "SIZES_SHOES",
//			1 => "SIZES_CLOTHES",
//			2 => "COLOR_REF",
//		),
//		"OFFERS_FIELD_CODE" => array(
//			0 => "NAME",
//			1 => "PREVIEW_PICTURE",
//			2 => "DETAIL_PICTURE",
//			3 => "DETAIL_PAGE_URL",
//		),
//		"OFFERS_LIMIT" => "500",
//		"OFFERS_PROPERTY_CODE" => array(
//			1 => "CML2_BAR_CODE",
//			2 => "CML2_ARTICLE",
//			5 => "CML2_BASE_UNIT",
//			7 => "MORE_PHOTO",
//			8 => "FILES",
//			9 => "CML2_MANUFACTURER",
//			10 => "PROTSESSOR",
//			11 => "CHASTOTA_PROTSESSORA",
//			12 => "KOLICHESTVO_YADER_PROTSESORA",
//			13 => "OBEM_OPERATICHNOY_PAMYATI",
//			14 => "TIP_VIDEOKARTY",
//			15 => "OBEM_VIDEOPAMYATI",
//			16 => "USTANOVLENNAYA_OS",
//			17 => "OBEM_PAMYATI",
//			18 => "RAZMER",
//			19 => "TSVET",
//			20 => "TSVET_1",
//			21 => "VIDEOKARTA",
//		),
//		"OFFERS_SORT_FIELD" => "sort",
//		"OFFERS_SORT_FIELD2" => "id",
//		"OFFERS_SORT_ORDER" => "asc",
//		"OFFERS_SORT_ORDER2" => "desc",
//		"PAGER_BASE_LINK_ENABLE" => "N",
//		"PAGER_DESC_NUMBERING" => "N",
//		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
//		"PAGER_SHOW_ALL" => "N",
//		"PAGER_SHOW_ALWAYS" => "N",
//		"PAGER_TEMPLATE" => "origami",
//		"PAGER_TITLE" => "Товары",
//		"PARTIAL_PRODUCT_PROPERTIES" => "N",
//		"PRICE_CODE" => \SotbitOrigami::GetComponentPrices(["BASE"]),
//		"PRICE_VAT_INCLUDE" => "Y",
//		"PRODUCT_ID_VARIABLE" => "id",
//		"PRODUCT_PROPERTIES" => "",
//		"PRODUCT_PROPS_VARIABLE" => "prop",
//		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
//		"SECTION_TEMPLATE" => "origami_section",
//		"COLLECTION_LIST_TEMPLATE" => "origami_default",
//		"SECTION_URL" => "",
//		"SEF_MODE" => "N",
//		"SET_BROWSER_TITLE" => "N",
//		"SET_LAST_MODIFIED" => "N",
//		"SET_META_DESCRIPTION" => "N",
//		"SET_META_KEYWORDS" => "N",
//		"SET_STATUS_404" => "N",
//		"SET_TITLE" => "N",
//		"SHOW_404" => "N",
//		"SHOW_PRICE_COUNT" => "1",
//		"SHOW_SLIDER" => "Y",
//		"USE_MAIN_ELEMENT_SECTION" => "N",
//		"USE_PRICE_COUNT" => "N",
//		"USE_PRODUCT_QUANTITY" => "N",
//		"OFFER_TREE_PROPS" => array(
//			1 => "PROTSESSOR",
//			2 => "OBEM_OPERATICHNOY_PAMYATI",
//			3 => "OBEM_PAMYATI",
//			4 => "RAZMER",
//			5 => "CHASTOTA_PROTSESSORA",
//			6 => "TIP_VIDEOKARTY",
//			7 => "TSVET",
//			8 => "KOLICHESTVO_YADER_PROTSESORA",
//			9 => "OBEM_VIDEOPAMYATI",
//			10 => "TSVET_1",
//			11 => "USTANOVLENNAYA_OS",
//			12 => "CML2_MANUFACTURER",
//		),
//		"PRODUCT_DISPLAY_MODE" => "Y",
//		"ADD_PICT_PROP" => "MORE_PHOTO",
//		"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
//		"PRODUCT_SUBSCRIPTION" => "Y",
//		"SHOW_DISCOUNT_PERCENT" => "Y",
//		"SHOW_OLD_PRICE" => "Y",
//		"SHOW_MAX_QUANTITY" => "M",
//		"MESS_RELATIVE_QUANTITY_FEW" => "В наличии",
//		"MESS_RELATIVE_QUANTITY_MANY" => "В наличии",
//		"MESS_RELATIVE_QUANTITY_NO" => "Нет в наличии",
//		"MESS_SHOW_MAX_QUANTITY" => "Наличие",
//		"USE_VOTE_RATING" => "Y",
//		"COMPARE_PATH" => Config::get("COMPARE_PAGE"),
//		"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
//		"USE_COMPARE_LIST" => "Y",
//		"PAGER_PARAMS_NAME" => "arrPager",
//		"ACTION_PRODUCTS" => array(
//			0 => "ADMIN",
//		),
//		"VARIANT_LIST_VIEW" => "template_1",
//		"COMPONENT_TEMPLATE" => "origami_default",
//		"SECTION_ID" => $_REQUEST["SECTION_ID"],
//		"LAZY_LOAD" => "Y"
//	),
//	false
//);
        ?></section><?
    ?></div>
<script>

    $( document ).ready(function() {
        var currentItem = null;
        $('ul#<?=$container?>').on('click', 'li:not(.active)', function ()
        {
            currentItem = $(this);

            var collectionId = $(this).data("collection-id");
            if (!$(this).hasClass("collection-loaded"))
            {
                if (collectionId.toString() !== 'PROMOTION_ID'){
                    getCollection($(this).data("collection-id"), currentItem);
                }
            } else {
                changeTab(currentItem);
            }
        });
    });

    function changeTab (btn) {
        $(btn)
            .addClass('active').siblings().removeClass('active')
            .closest('.catalog_section_block_tabs').find('.tabs_sale_block__content').removeClass('active').eq($(btn).index()).addClass('active');
    }


    function getCollection(collectionId, btnTab)
    {
        var collectionTab = $('.collection-id-'+collectionId);
        var currentTab = $(btnTab).closest('.catalog_section_block_tabs').find('.tabs_sale_block__content.active');

        createMainLoaderInner(currentTab);
        BX.ajax({
            url: <?= CUtil::PhpToJSObject($componentPath, false, true) ?> + '/ajax.php' + (document.location.href.indexOf('clear_cache=Y') !== -1 ? '?clear_cache=Y' : ''),
            method: 'POST',
            dataType: 'html',
            timeout: 60,
            async: true,
            data: {
                params : <?=CUtil::PhpToJSObject($arParams, false, true)?>,
                collectionId: collectionId,
                siteId: '<?=SITE_ID?>',
            },
            onsuccess: function (result)
            {

                removeMainLoaderInner(currentTab);

                changeTab(btnTab);
                $('.collection-id-'+collectionId).html(result).promise().done(function ()
                {
                    if(document.querySelector('.recommended-products__slider')) {
                        let items = document.querySelectorAll('.product_card__block_item_inner-wrapper');
                        let widthSlide = calcWidthSlide();
                        for (let i = 0; i < items.length; i++) {
                            items[i].style.width = widthSlide + 'px';
                        }
                        if(document.querySelector('.recommended-products__slider:not(.slick-slider)')) {
                            inicialSlider('.recommended-products__slider:not(.slick-slider)', settingSlider.itemSetting);
                        }
                    };

                });



                if (!collectionTab.hasClass("collection-loaded"))
                {
                    $('li.collection-' + collectionId).addClass("collection-loaded");
                }
            },
            error: function (result)
            {
                $('.collection-id-'+collectionId).html(<?= CUtil::PhpToJSObject(GetMessage("LOADING_ERROR"), false, true) ?>);
                removeMainLoaderInner(currentTab);
            }
        });
    }
</script>
