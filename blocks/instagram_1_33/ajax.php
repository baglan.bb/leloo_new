<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$block = new SvaBlock(__DIR__);
$settings = $block->getSettings();
$APPLICATION->IncludeComponent(
	"sotbit:instagram",
	"origami_insta",
	array(
		"LOGIN" => $settings["fields"]["login"]["value"],
		"IMG_COUNT" => $settings["fields"]["count"]["value"],
		"TITLE" => $settings["fields"]["title"]["value"],
		"TITLE_TEXT" => $settings["fields"]["title_text"]["value"],
		"TEXT" => $settings["fields"]["text"]["value"],
		"IMG_DEFAULT" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"COMPONENT_TEMPLATE" => "origami_insta",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>
