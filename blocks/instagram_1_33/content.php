<?
$block = new SvaBlock(__DIR__);
$settings = $block->getSettings();
$style = $block->getStyle();
Bitrix\Main\Page\Asset::getInstance()->addCss("/local/templates/.default/components/sotbit/instagram/origami_insta/style.css");

?><div class="block-wrapper-inner" id="mp-insta" style="<?= $style ?>"><?
?></div><script>
    $.ajax({
        url: "<?= str_replace($_SERVER["DOCUMENT_ROOT"], "", dirname(__FILE__)) ?>/ajax.php",
        success: function (data) {
            $("#mp-insta").html(data);
        }
    })

</script>