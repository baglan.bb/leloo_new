<?php
require_once ($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
if(isset($_GET['ajax'])&&(!empty($_POST['city'])||!empty($_REQUEST['cityId']))){

    $US=new CUser();

    if(!empty($_POST['city'])){
        $City=htmlspecialcharsEx($_POST['city']);
        $US->Update($USER->GetID(),array('PERSONAL_CITY'=>$City));
    }else{
        $CityId=(int)$_REQUEST['cityId'];
        if(!empty($GLOBALS['CITIES'][$CityId])){
            $City=$GLOBALS['CITIES'][$CityId]['UF_NAME'];
            $US->Update($USER->GetID(),array('PERSONAL_CITY'=>$City));
        }
    }
    setcookie('city',$City,time()+3600*24*30,'/');
    echo new Request(['city'=>$City],'success');
}else{
    LocalRedirect('/');
}