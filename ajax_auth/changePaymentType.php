<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Sale;

//для заказов с постоплптой
// получаем объект существующего заказа
$order = Sale\Order::load($_POST['sendData']['id']);

// задаем значение для поля STATUS_ID - N (статус: принят)
$order->setField('STATUS_ID', 'P');

// сохраняем изменения
$result = $order->save();
if (!$result->isSuccess())
{
    $result->getErrors();
} else {
    echo $_POST['sendData']['name'];
    $arEventFields = array("SUBJECT" => "Новый заказ с постоплатой", "ORDER_ID" => $_POST['sendData']['id']);
    CEvent::SendImmediate("NEW_ORDER_POSTPAY", "s1", $arEventFields);
    updDeal($_POST['sendData']['id'], 'Оплата при получении', 'NEW');
}
require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_after.php");