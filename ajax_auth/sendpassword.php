<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (isset($_POST['phone']) && strlen($_POST['phone'])>9)
session_start();
$psw = rand(100000, 999999);
$txt = 'Ваш пароль: '.$psw.' используйте для входа leloo.kz. Логин: '.$_POST['phone'];
$_SESSION['SMS_CODE'] = $psw;
if(CModule::IncludeModule("smsc.sms")) {
    $sms = new SMSC_Send;
    $phone = $_POST['phone'];
    $sms->Send_SMS($phone,$txt);
    echo json_encode(array('status'=>'success','message'=>$sms->return_mess));
}
echo json_encode(array('status'=>'success'));