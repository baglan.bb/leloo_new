<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $USER;
session_start();
$pass = str_replace('-','',$_REQUEST['USER_PASSWORD']);

$errors = array();
if  (strlen($_POST['USER_NAME']) < 2)
    array_push($errors, 'Имя не должно быть короче 3-х символов');
//if  (strlen($_POST['USER_LAST_NAME']) < 1)
//    array_push($errors, 'Фамилия не должна быть короче 2-х символов');
if  ($pass != $_SESSION['SMS_CODE'])
    array_push($errors, 'Не правильный пароль');

if(count($errors) > 0)
    echo json_encode(array('status'=>'error','message'=>$errors));
else{
    $phone = NormalizePhone($_REQUEST['USER_PHONE']);
    $arResult = $USER->Register($phone, $_POST['USER_NAME'], $_POST['USER_LAST_NAME'], $pass, $pass, $phone."@bx.bx");
    echo json_encode(array('status'=>'success'));
}

