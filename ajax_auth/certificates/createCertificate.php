<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
foreach ($_POST as $i){
    if (strlen($i) < 1){
        echo json_encode(array('status'=>'notFilled','url'=>''));
        die;
    }
}
//header('Content-Type: image/png');
    $im = ImageCreateFromjpeg("../../upload/certificates/tempate.jpg");
    $r=rand(100, 999)."-".rand(100, 999)."-".preg_replace("/[^0-9]/", '', $_POST["offer"]);
    $save = $r.".jpg";

    $white = imagecolorallocate($im, 255, 255, 255);
    $grey = imagecolorallocate($im, 128, 128, 128);
    $black = imagecolorallocate($im, 0, 0, 0);

    $font = 'C:\OpenServer\domains\leloo.local\ajax_auth\certificates\Ubuntu-Medium.ttf';
    //$font = '/var/www/u89706/data/www/leloo.kz/ajax_auth/certificates/Ubuntu-Medium.ttf';

    // Add some shadow to the text
    imagettftext($im, 20, 0, 60+1, 60+1, $grey, $font, $text);

    // Add the text
    imagettftext($im, 20, 0, 60, 120, $black, $font, $_POST["name"]);
    imagettftext($im, 20, 0, 60, 170, $black, $font, 'Купон №'.$r);
    imagettftext($im, 20, 0, 60, 240, $black, $font, $_POST["offer"]);
    imagettftext($im, 20, 0, 300, 600, $black, $font, $_POST["text"]);

    // Using imagepng() results in clearer text compared with imagejpeg()
    imagepng($im, $save);
    imagedestroy($im);


    $arCouponFields = array(
        "DISCOUNT_ID" => "55",
        "ACTIVE" => "N",
        "ONE_TIME" => "O",
        "COUPON" => $r,
        "DESCRIPTION" => 'Кому:'.$_POST["name"].' Телефон:'.$_POST["phone"].' Доставить:'.$_POST["date"].' адрес: '.$_POST["address"].' от: '.$_POST["phoneSender"]
    );

    $CID = new CCatalogDiscountCoupon();
    $CID->Add($arCouponFields);
    echo json_encode(array('status'=>'success','url'=>'/ajax_auth/certificates/'.$save));
