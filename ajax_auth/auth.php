<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

global $USER;
$msg = array('Не правильный пароль');
$pass = str_replace('-','',$_REQUEST['USER_PASSWORD']);

if ($_REQUEST['FORGOT'] > 1){
    session_start();
    $user = new CUser;
    if  ($_SESSION['SMS_CODE'] == $pass){
        $fields = Array(
            "PASSWORD"          => $pass,
            "CONFIRM_PASSWORD"  => $pass,
        );
        $user->Update($_REQUEST['FORGOT'], $fields);
        $strError .= $user->LAST_ERROR;
    }
    else{
        echo json_encode(array('status'=>'error', 'message'=>$msg));
        die();
    }
}

if (!is_object($USER)) $USER = new CUser;
$arAuthResult = $USER->Login(NormalizePhone($_REQUEST['USER_PHONE']), $pass, "Y");
$APPLICATION->arAuthResult = $arAuthResult;
if ($arAuthResult['TYPE'] == "ERROR")
    echo json_encode(array('status'=>'error', 'message'=>$msg));
else
    echo json_encode(array('status'=>'success'));



