<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $USER;

$rsUser = CUser::GetByLogin(NormalizePhone($_REQUEST['USER_PHONE']));
$arUser = $rsUser->Fetch();

if (isset($_REQUEST['CHECK']) && $_REQUEST['CHECK'] == 'Y' && $arUser == false){
    echo json_encode(array('status'=>'userNotFound','message'=>'Пользователь не найден'));
    die();
}
$flag = true;
if (isset($_REQUEST['FORGOT']) && $_REQUEST['FORGOT'] == 'Y')
    $flag=false;
if ($arUser != false && $flag)
    echo json_encode(array('status'=>'userExist','message'=>$arUser['NAME']));
else{
    if (isset($_REQUEST['USER_PHONE'])){
        if ($_REQUEST['FORGOT'] == 'Y')
            $id = $arUser["ID"];
        else
            $id = null;
        session_start();
        $psw = rand(1000, 9999);
        $textPsw = $psw; //substr($psw, 0, 3)." ".substr($psw, 3, 3);
        $txt = 'Ваш пароль: '.$textPsw.' используйте для входа leloo.kz. Логин: '.NormalizePhone($_REQUEST['USER_PHONE']);
        $_SESSION['SMS_CODE'] = $psw;
        if(CModule::IncludeModule("smsc.sms")) {
            $sms = new SMSC_Send;
            $phone = $_REQUEST['USER_PHONE'];
            $sms->Send_SMS($phone,$txt);
            echo json_encode(array('status'=>'sendPsw', 'id'=>$id));
        }
    }
    else
        echo json_encode(array('status'=>'error'));
}