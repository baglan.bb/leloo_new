<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Официальный представитель брендов корейской косметики");
$APPLICATION->SetPageProperty("keywords", "корейская косметика");
$APPLICATION->SetPageProperty("title", "Интернет-магазин \"Leloo\"");
$APPLICATION->SetTitle("Интернет-магазин \"Leloo\"");

//тут добавил Поиск
$APPLICATION->IncludeComponent(
    "bitrix:search.title",
    "bootstrap_v4_custom",
    array(
        "INPUT_ID" => "title-search-input-mobile",
        "CONTAINER_ID" => "title-search-mobile",
        "NUM_CATEGORIES" => "1",
        "CHECK_DATES" => "N",
        "ORDER" => "date",
        "PAGE" => SITE_DIR."catalog/",
        "SHOW_INPUT" => "Y",
        "SHOW_OTHERS" => "N",
        "TOP_COUNT" => "5",
        "USE_LANGUAGE_GUESS" => "Y",
        "CATEGORY_0" => array(
            0 => "iblock_sotbit_origami_catalog",
        ),
        "CATEGORY_0_iblock_catalog" => array(
            0 => NULL,
        ),
        "CATEGORY_0_TITLE" => "РўРѕРІР°СЂС‹",
        "PRICE_CODE" => array("BASE"),
        "SHOW_PREVIEW" => "Y",
        "PREVIEW_WIDTH" => "80",
        "PREVIEW_HEIGHT" => "80"
    )
);

$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => "/blocks/banner_side_right_36/content.php",
    )
);

$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => "/blocks/popular_categories_simple_14/content.php",
    )
);

//$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
//        "AREA_FILE_SHOW" => "file",
//        "PATH" => "/blocks/promotions_vertical_42/content.php",
//    )
//);

//$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
//        "AREA_FILE_SHOW" => "file",
//        "PATH" => "/blocks/banner_mini_23/content.php",
//    )
//);

$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => "/blocks/products_41_edited/content.php",
    )
);

$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => "/blocks/blog_41/content.php",
    )
);

//$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
//        "AREA_FILE_SHOW" => "file",
//        "PATH" => "/blocks/instagram_1_33/content.php",
//    )
//);

$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => "/blocks/about_19/content.php",
    )
);

$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => "/blocks/brands_slider_20/content.php",
    )
);

//$APPLICATION->IncludeComponent('sotbit:block.include','',['PART' => 'main_' . SITE_ID],null,['HIDE_ICONS' => 'Y']);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>