<?php

namespace Sva\TinyPng;

class Source
{
    private $image;

    private function __construct($buffer)
    {
        $this->image = $buffer;
        /* ... @return Source */
    }  // Защищаем от создания через new Source

    /**
     * @param $id
     * @return Result
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function fromDb($id, $force = false)
    {
        $res = FilesTable::getList(array(
            "filter" => array(
                "FILE_ID" => $id
            )
        ));

        if($res->getSelectedRowsCount() <= 0 || $force == true)
        {
            $src = $_SERVER["DOCUMENT_ROOT"] . \CFile::GetPath($id);
            return self::fromFile($src);
        }
        else
        {
            throw new \Exception("File #" . $id . " already compressed");
        }
    }

    public static function converPathToAbs(&$src)
    {
        if(substr($src, 0, strlen($_SERVER["DOCUMENT_ROOT"])) != $_SERVER["DOCUMENT_ROOT"])
        {
            $src = $_SERVER["DOCUMENT_ROOT"] . $src;
        }
    }

    /**
     * @param $path
     * @return Result
     * @throws \Exception
     */
    public static function fromFile($path)
    {
        self::converPathToAbs($path);

        if(file_exists($path) && filesize($path) > 0)
        {
            $buffer = self::fromBuffer(file_get_contents($path));
            return $buffer->result();
        }
        else
        {
            throw new \Exception("File: " . $path . " is not exists or empty", 404);
        }
    }

    public static function fromBuffer($buffer)
    {
        return new self($buffer);
    }

//    public static function fromUrl($url)
//    {
//        $body = array("source" => array("url" => $url));
//        $response = Tinify::getInstance()->getClient()->request("/shrink", "post", $body);
//        return new self($response->headers["location"]);
//    }

    public function result()
    {
        /**
         * var Sva\TinyPng\Result
         */
        return Tinify::getInstance()->getClient()->request("/shrink", $this->image);
    }

    public function toFile($path)
    {
        $result = $this->result();
        if($result->isSuccess())
        {
            $this->result()->toFile($path);
        }
    }

    public function toBuffer()
    {
        return $this->result()->toBuffer();
    }

    public function toDb(int $intFileID)
    {
        return $this->result()->toDb($intFileID);
    }
}