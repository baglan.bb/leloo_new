<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 09.12.2018
 * Time: 18:12
 */

namespace Sva\TinyPng;


class Events
{
    function OnFileSave(&$arFile, $strFileName, $strSavePath, $bForceMD5, $bSkipExt)
    {

        $boolCompress = \COption::GetOptionString(self::$module_id, "tiny_png_compress_all_files");

        if($boolCompress == 'Y')
        {
            if((!isset($arFile["MODULE_ID"]) || $arFile["MODULE_ID"] != "iblock"))
            {
                $key = \COption::GetOptionString(self::$module_id, "tiny_png_api_key");
                if(strlen($key) > 0)
                {
                    if($arFile["type"] == "image/jpeg" || $arFile["type"] == "image/png")
                    {
                        Tinify\Tinify::setKey($key);
                        $source = Tinify\Source::fromFile($arFile["tmp_name"]);
                        $source->toFile($arFile["tmp_name"]);
                        $arFile["size"] = filesize($arFile["tmp_name"]);
                    }
                }
            }
        }
    }

    function OnAfterIBlockSectionUpdate(&$arFields)
    {

        $boolCompress = \COption::GetOptionString(self::$module_id, "tiny_png_compress_iblock_section");
        if($boolCompress != 'Y')
            return;

        if($arFields["RESULT"])
        {

            if((is_array($arFields["PICTURE"]) && isset($arFields["PICTURE"]['type'])) && (($arFields["PICTURE"]['type'] == 'image/png' || $arFields["PICTURE"]['type'] == 'image/jpeg')))
            {

                $rsSection = \CIBlockSection::GetByID($arFields["ID"]);
                $arSection = $rsSection->GetNext();
                self::CompressImageByID($arSection['PICTURE']);

            }

            if((is_array($arFields["PICTURE"]) && isset($arFields["PICTURE"]['type'])) && (($arFields["PICTURE"]['type'] == 'image/png' || $arFields["PICTURE"]['type'] == 'image/jpeg')))
            {

                $rsSection = \CIBlockSection::GetByID($arFields["ID"]);
                $arSection = $rsSection->GetNext();
                self::CompressImageByID($arSection['DETAIL_PICTURE']);

            }

        }

    }

    function OnAfterIBlockElementUpdate(&$arFields)
    {

        $boolCompress = \COption::GetOptionString(self::$module_id, "tiny_png_compress_iblock_element");
        if($boolCompress != 'Y')
            return;

        if(isset($arFields["PREVIEW_PICTURE_ID"]) && intval($arFields["PREVIEW_PICTURE_ID"]) > 0)
        {
            self::CompressImageByID($arFields["PREVIEW_PICTURE_ID"]);
        }

        if(isset($arFields["DETAIL_PICTURE_ID"]) && intval($arFields["DETAIL_PICTURE_ID"]) > 0)
        {
            self::CompressImageByID($arFields["DETAIL_PICTURE_ID"]);
        }

        $arEl = false;

        foreach ($arFields["PROPERTY_VALUES"] as $key => $values)
        {

            if(!is_array(!$values))
                continue;

            foreach ($values as $k => $v)
            {

                if(is_array(!$v))
                    continue;

                if($v['VALUE']['type'] == 'image/png' || $v['VALUE']['type'] == 'image/jpeg')
                {

                    if(!$arEl)
                    {
                        $rsEl = \CIBlockElement::GetByID($arFields["ID"]);
                        if($rsEl)
                        {
                            $obEl = $rsEl->GetNextElement();
                            $arEl = $obEl->GetFields();
                            $arEl["PROPERTIES"] = $obEl->GetProperties();
                        }
                    }

                    foreach ($arEl["PROPERTIES"] as $strPropCode => $arProp)
                    {
                        if($arProp["ID"] == $key)
                        {
                            if($arProp["MULTIPLE"])
                            {
                                foreach ($arProp["VALUE"] as $intFileID)
                                {
                                    self::CompressImageByID($intFileID);
                                }
                            }
                            else
                            {
                                self::CompressImageByID($arProp["VALUE"]);
                            }
                        }
                    }

                }
            }
        }
    }
}