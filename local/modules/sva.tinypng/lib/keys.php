<?php
/**
 * Created by PhpStorm.
 * User: WebDev
 * Date: 01.10.2016
 * Time: 13:31
 */

namespace Sva\TinyPng;

use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class KeysTable extends Entity\DataManager{

    static $module_id = "sva.tinypng";

    public static function getFilePath()
    {
        return __FILE__;
    }

    public static function getTableName()
    {
        return 'b_sva_tinypng_keys';
    }

    public static function getTableTitle()
    {
        return Loc::getMessage('SVA_TINYPNG_REDIRECTS_TITLE');
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', Array(
                "primary" => true,
                "autocomplete" => true,
            )),
            new Entity\StringField('KEY'),
            new Entity\IntegerField('COMPRESSION_COUNT'),
        );
    }
}