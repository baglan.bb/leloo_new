<?php

namespace Sva\TinyPng;

use Bitrix\Main\FileTable;

class Result
{
    protected $headers;
    protected $response;
    private $error = null;
    protected $status = 0;

    public function __construct(string $response, \Bitrix\Main\Web\HttpClient $httpClient)
    {
        $this->responseText = $response;
        $this->response = (array)json_decode($response);
        $this->headers = $httpClient->getHeaders();
        $this->status = $httpClient->getStatus();
    }

    public function toBuffer()
    {
        if($this->isSuccess() == true)
        {
            return file_get_contents($this->response["output"]->url);
        }
        else
        {
            throw new \Error($this->getError());
        }
    }

    public function toFile($path)
    {
        return file_put_contents($path, $this->toBuffer());
    }

    public function toDb(int $id)
    {
        if(!$this->isSuccess())
        {
            throw new \Error("Request is not success. Status is " . $this->status);
        }

        if(!$arFile = \CFile::GetFileArray($id))
        {
            throw new \Error("File with id " . $id . " not found");
        }

        $size = $this->toFile($_SERVER["DOCUMENT_ROOT"] . $arFile["SRC"]);

        if($size !== FALSE)
        {
            $arFields = Array(
                "FILE_ID" => $id,
                "SIZE_BEFORE" => \Bitrix\Main\DB\OracleSqlHelper::forSql($this->response['input']->size),
                "SIZE_AFTER" => $size,
            );

            $sql = "UPDATE b_file 
        SET FILE_SIZE='" . \Bitrix\Main\DB\OracleSqlHelper::forSql($size) . "'
        WHERE ID='" . \Bitrix\Main\DB\OracleSqlHelper::forSql($id) . "'";
            $connection = \Bitrix\Main\Application::getConnection();
            $connection->query($sql);

            if(FilesTable::getByPrimary($id)->getSelectedRowsCount())
            {
                return FilesTable::update($id, $arFields);
            }
            else
            {
                return FilesTable::add($arFields);
            }
        }
        else
        {
            throw new \Exception('File is not whritten');
        }
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        if(($this->status == 200 || $this->status == 201) && $this->responseText <> "")
        {
            return true;
        }
        else
        {
            if(!isset($this->response["error"]))
            {
                return $this->error = "Unknown error. Response status: " . $this->status;
            }
            else
            {
                $this->error = $this->response["error"];
                return false;
            }
        }
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getError()
    {
        return $this->error;
    }
}
