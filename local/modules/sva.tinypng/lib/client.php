<?php

namespace Sva\TinyPng;

use Sva\TinyPng\Exception;
use Sva\TinyPng\Source;
use Bitrix\Main\Web\HttpClient;

class Client
{
    const API_ENDPOINT = "https://api.tinify.com";

    const RETRY_COUNT = 10;
    const RETRY_DELAY = 500;

    private $compressionCount = NULL;
    private $key = "";
    private $id = "";

    /**
     * Client constructor.
     * @param $key
     * @throws \Exception
     */
    function __construct($key)
    {
        if(is_string($key))
        {
            if(strlen($key) >= 32)
            {
                $this->key = $key;
                $this->requestCompressionCount();
            }
            else
            {
                throw new \Exception("Key length must be 32 chars");
            }
        }
        elseif(is_array($key))
        {
            if(strlen($key["KEY"]) >= 32)
            {
                $this->key = $key["KEY"];
            }
            else
            {
                throw new \Exception("Key length must be 32 chars");
            }

            if(isset($key["COMPRESSION_COUNT"]))
            {
                $this->compressionCount = intval($key["COMPRESSION_COUNT"]);
            }
            else
            {
                $this->requestCompressionCount();
            }

            if(isset($key["ID"]))
            {
                $this->id = $key["ID"];
            }
            else
            {
                $this->save();
            }
        }
    }

    /**
     * Запрашивает количество сжатых изображений
     */
    public function requestCompressionCount()
    {
        $url = "/shrink";
        $this->request($url, '', 'post');
    }

    /**
     * @return int|null
     */
    public function getCompressionCount()
    {
        return $this->compressionCount;
    }

    /**
     * @param $url
     * @param null $body
     * @param string $method
     * @return Result
     * @throws \Exception
     */
    public function request($url, $body = NULL, $method = 'post')
    {
        $headers = array();

        if(is_array($body))
        {
            if(!empty($body))
            {
                $body = json_encode($body);
                $headers["Content-Type"] = "application/json";
            }
            else
            {
                $body = NULL;
            }
        }

        for ($retries = self::RETRY_COUNT; $retries >= 0; $retries--)
        {
            if($retries < self::RETRY_COUNT)
            {
                usleep(self::RETRY_DELAY * 1000);
            }

            $httpClient = new HttpClient();
            $httpClient->setRedirect(false);
            $httpClient->setAuthorization('api', $this->key);

            $url = strtolower(substr($url, 0, 6)) == "https:" ? $url : self::API_ENDPOINT . $url;

            if(count($headers) > 0)
            {
                foreach ($headers as $headerName => $headerValue)
                {
                    $httpClient->setHeader('Content-Type', 'application/json', true);
                }
            }

            if($body)
            {
                if(strtolower($method == "post"))
                {
                    $response = $httpClient->post($url, $body);
                }
                else
                {
                    $response = $httpClient->get($url);
                }
            }
            else
            {
                if(strtolower($method == "post"))
                {
                    $response = $httpClient->post($url);
                }
                else
                {
                    $response = $httpClient->get($url);
                }
            }

            $headers = $httpClient->getHeaders();

            if($headers->get('compression-count') <> '')
            {
                $this->compressionCount = intval($headers->get('compression-count'));
                $this->save();
            }

            $result = new Result($response, $httpClient);

            if($result->isSuccess() !== true)
            {
                throw new \Exception($result->getError());
            }
            else
            {
                return $result;
            }
        }
    }

    /**
     * @throws \Exception
     */
    public function save()
    {
        $arFields = Array(
            "KEY" => $this->key,
            "COMPRESSION_COUNT" => $this->compressionCount,
        );

        if($this->id > 0)
        {
            KeysTable::update($this->id, $arFields);
        }
        else
        {
            KeysTable::add($arFields);
        }
    }
}
