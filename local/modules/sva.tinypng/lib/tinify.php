<?php

namespace Sva\TinyPng;

use Bitrix\Main\FileTable;
const VERSION = "1.5.2";

/**
 * Class Tinify
 * Класс служит для хранения информации о клиентах для сервиса TinyPNG.
 * @package Sva\TinyPng
 */
class Tinify
{
    static private $_instanse;
    private $compressionCount = 0;
    private $clients = array();

    private function __construct()
    { /* ... @return Singleton */
    }  // Защищаем от создания через new Tinify

    private function __clone()
    { /* ... @return Singleton */
    }  // Защищаем от создания через клонирование

    private function __wakeup()
    { /* ... @return Singleton */
    }  // Защищаем от создания через unserialize

    /**
     * @return Tinify
     */
    public static function getInstance()
    {
        if(!self::$_instanse)
        {
            self::$_instanse = new static();
        }

        return self::$_instanse;
    }

    public function getCompressionCount()
    {
        return $this->compressionCount;
    }

    /**
     * addClient - добавляет клиента и пересчитывает количество сжатых картинок сумируя у всех клиентов
     * @param $client
     */
    public function addClient($client)
    {
        $this->clients[] = $client;
        $this->calcCompressionCount();
    }

    /**
     * calcCompressionCount - пересчитывает количество сжатых картинок сумируя у всех клиентов
     */
    private function calcCompressionCount()
    {
        $summ = 0;
        foreach ($this->clients as $client)
        {
            $summ += $client->getCompressionCount();
        }
        $this->compressionCount = $summ;
    }

    /**
     * fillClients - Заполняет клиентов в класс
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function fillClients()
    {
        $rsKeys = KeysTable::getList();
        while ($arKey = $rsKeys->fetch())
        {
            $this->addClient(new Client($arKey));
        }
    }

    /**
     * @return Client
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getClient()
    {
        if(empty($this->clients))
        {
            $this->fillClients();
        }

        foreach ($this->clients as $client)
        {
            if($client->getCompressionCount() < 500)
            {
                return $client;
            }
        }
    }
}
