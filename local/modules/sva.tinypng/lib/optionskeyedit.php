<?php

namespace Sva\TinyPng;

use Bitrix\Main\Localization\Loc;
use Rover\Fadmin\Inputs\File;
use Rover\Fadmin\Inputs\Checkbox;
use Rover\Fadmin\Inputs\Header;
use Rover\Fadmin\Inputs\SubTab;
use Rover\Fadmin\Inputs\SubTabControl;
use Rover\Fadmin\Inputs\Text;
use Rover\Fadmin\Inputs\Banner;
use Rover\Fadmin\Options as Options;

Loc::loadMessages(__FILE__);

/**
 * Class TestOptions
 *
 * @package Sva\Settings
 */
class OptionsKeyEdit extends Options
{
    const MODULE_ID = 'sva.tinypng';

    /**
     * @return static
     */
    public static function getInstance()
    {
        return parent::getInstance(self::MODULE_ID);
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return array(
            'tabs' => self::getTabs()
        );
    }

    /**
     * @return array
     */
    protected static function getTabs()
    {
        $ar = array(
            array(
                'name' => Loc::getMessage('SVA_TINY_PNG_API_KEY_EDIT'),
                'label' => Loc::getMessage('SVA_TINY_PNG_API_KEY_EDIT_LABEL'),
                'description' => Loc::getMessage('SVA_TINY_PNG_API_KEY_EDIT_DESC'),
                'siteId' => 's1',
                'inputs' => array(
                    array(
                        'type' => Text::getType(),
                        'name' => 'tiny_png_api_key',
                        'label' => Loc::getMessage('SVA_TINY_PNG_API_KEY'),
                        'sort' => '200',
                        'multiple' => "Y"
                    ),
                )
            )
        );
        return $ar;
    }
}