<?
$MESS["SVA_TINYPNG_KEY_EDIT_TAB_NAME_COMMON"] = "API-ключ";
$MESS['SVA_TINY_PNG_API_KEY'] = 'Ключ API от TinyPNG';
$MESS["SVA_TINYPNG_KEY_EDIT"] = "TinyPNG API-ключ";
$MESS["SVA_TINYPNG_KEY_EDIT_ADD"] = "Добавить";
$MESS["SVA_TINYPNG_KEY_EDIT_UPDATE"] = "Обновить ключ";
$MESS["SVA_TINYPNG_KEY_EDIT_DELETE"] = "Удалить ключ";
$MESS["SVA_TINYPNG_KEY_EDIT_DELETE_CONFIRM"] = "Вы действительно хотите удалить ключ";
$MESS["SVA_TINYPNG_KEY_EDIT_ERR_SAVE"] = "Ошибка сохранения ключа";
$MESS["SVA_TINYPNG_KEY_EDIT_COPY"] = "Копировать ключ";
$MESS["SVA_TINYPNG_KEY_EDIT_LIST"] = "Список ключей";
$MESS["SVA_TINYPNG_KEY_EDIT_FIELD_ID"] = "ID";
$MESS["SVA_TINYPNG_KEY_EDIT_FIELD_COMPRESSION_COUNT"] = "Количество сжатых изображений";
