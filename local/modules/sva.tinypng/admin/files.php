<?php
$module = 'sva.tinypng';

use Bitrix\Main\Localization\Loc;

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $module . '/include.php');
IncludeModuleLangFile(__FILE__);

$arSites = array();
$rsSites = CSite::GetList($by1 = 'sort', $order1 = 'desc');
while ($arSite = $rsSites->GetNext())
{
    $arSites[$arSite['ID']] = '[' . $arSite['ID'] . '] ' . $arSite['NAME'];
}

CModule::IncludeModule($module);
function getNiceFileSize($fileSize, $digits = 2)
{

    $sizes = array("TB", "GB", "MB", "KB", "B");
    $total = count($sizes);
    while ($total-- && $fileSize > 1024)
    {
        $fileSize /= 1024;
    }
    return round($fileSize, $digits) . " " . $sizes[$total];
}

$key = \COption::GetOptionString($module, "tiny_png_api_key");

$list = new CSVAAdminList($module);
$list->generalKey = 'SVA_TINYPNG_FILES_FILE_ID';
$list->SetRights();
$list->SetTitle(GetMessage('SVA_TINYPNG_TITLE'));
$list->SetGroupAction(array(
    'compress' => function ($hash)
    {
    },
));
$list->SetContextMenu(false);
$list->SetHeaders(array(
    'SVA_TINYPNG_FILES_FILE_ID' => "ID",
    'SVA_TINYPNG_FILES_FILE_MODULE_ID' => GetMessage('SVA_TINYPNG_MODULE_ID'),
    'SVA_TINYPNG_FILES_FILE_CONTENT_TYPE' => GetMessage("SVA_TINYPNG_CONTENT_TYPE"),
    'SVA_TINYPNG_FILES_FILE_FILE_NAME' => GetMessage("SVA_TINYPNG_FILE_NAME"),
    'SVA_TINYPNG_FILES_FILE_ORIGINAL_NAME' => GetMessage("SVA_TINYPNG_ORIGINAL_NAME"),
    'SVA_TINYPNG_FILES_FILE_DESCRIPTION' => GetMessage("SVA_TINYPNG_DESCRIPTION"),
    'SVA_TINYPNG_FILES_FILE_FILE_SIZE' => GetMessage("SVA_TINYPNG_FILE_SIZE"),
    'SVA_TINYPNG_FILES_FILE_IMAGE' => GetMessage("SVA_TINYPNG_IMAGE"),
    'COMPRESS' => GetMessage("SVA_TINYPNG_COMPRESS"),
    'SIZE_BEFORE' => GetMessage("SVA_TINYPNG_SIZE_BEFORE"),
    'SIZE_AFTER' => GetMessage("SVA_TINYPNG_SIZE_AFTER"),
));

$arMessages = array();
if($_REQUEST["compress"])
{

    $arUpdateMessages = array();

    if(intval($_REQUEST["compress"]))
    {
        set_time_limit(0);

        $intFileID = intval($_REQUEST["compress"]);

        $src = \CFile::GetPath($intFileID);

        try
        {
            $image = \Sva\TinyPng\Source::fromDb($intFileID)->toDb($intFileID);
        }
        catch (\Exception $e)
        {
            $arUpdateMessages[] = $e->getMessage();
        }
    }

    $list->addUpdateError($arUpdateMessages);
}
elseif($_REQUEST["action"] == "compress")
{

    $arIDs = $_REQUEST["ID"];
    $arGroupMessages = array();

    if(is_array($arIDs) && count($arIDs))
    {

        set_time_limit(0);

        foreach ($arIDs as $intFileID)
        {
            $intFileID = intval($intFileID);

            if($intFileID > 0)
            {
                try
                {
                    $image = \Sva\TinyPng\Source::fromDb($intFileID)->toDb($intFileID);
                }
                catch (\Exception $e)
                {
                    $arGroupMessages[] = Loc::getMessage("SVA_TINYPNG_ERROR_COMPRESS", array(
                        "#ID#" => $intFileID,
                        "#ERROR_TEXT#" => $e->getMessage()
                    ));
                }

            }
        }
    }

    $list->addGroupError($arGroupMessages);

}

/********************************FILTER****************************************/
$list->SetFilter(array(
    'file_id' => array('TITLE' => GetMessage('SVA_TINYPNG_FILTER_ID'), 'OPER' => ''),
    'file_size' => array(
        'TITLE' => GetMessage('SVA_TINYPNG_FILTER_FILE_SIZE'),
    ),
    'file.comressed' => array(
        'TITLE' => GetMessage('SVA_TINYPNG_FILTER_COMRESSED'),
        'TYPE' => 'select',
        'VARIANTS' => Array(
            "Y" => GetMessage('SVA_TINYPNG_FILTER_COMRESSED_Y'),
            "N" => GetMessage('SVA_TINYPNG_FILTER_COMRESSED_N'),
        )
    ),
    'module_id' => array('TITLE' => GetMessage('SVA_TINYPNG_FILTER_MODULE_ID')),
    'original_name' => array('TITLE' => GetMessage('SVA_TINYPNG_FILTER_ORIGINAL_NAME'), 'OPER' => '?'),
    'file.file_name' => array('TITLE' => GetMessage('SVA_TINYPNG_FILTER_FILE_NAME'), 'OPER' => '?'),
    'SVA_TINYPNG_FILES_FILE_CONTENT_TYPE' => array(
        'TITLE' => GetMessage('SVA_TINYPNG_FILTER_FILE_TYPE'),
        'TYPE' => 'select',
        'OPER' => '@',
        'VARIANTS' => array(
            'image/png' => GetMessage('SVA_TINYPNG_MIME_PNG'),
            'image/jpeg' => GetMessage('SVA_TINYPNG_MIME_JPG')
        )
    ),
));
if(!isset($by))
    $by = 'FILE_ID';

if($by == "COMPRESS")
    $by = "FILE_ID";

if($by == "ID")
    $by = "FILE_ID";

if(!isset($order))
    $order = 'ASC';

$rsFiles = \Sva\TinyPng\FilesTable::getList(
    array(
        'select' => array(
            'FILE.ID',
            'FILE.FILE_SIZE',
            'FILE.MODULE_ID',
            'FILE.HEIGHT',
            'FILE.WIDTH',
            'FILE.SUBDIR',
            'FILE.FILE_NAME',
            'FILE.ORIGINAL_NAME',
            'FILE.CONTENT_TYPE',
            'FILE.DESCRIPTION',
            'FILE.HANDLER_ID',
            'FILE.EXTERNAL_ID',
            'FILE.SUBDIR',
            'FILE_ID',
            'SIZE_BEFORE',
            'SIZE_AFTER'
        ),
        'order' => array(
            $by => $order
        ),
        'filter' => $list->MakeFilter()
    )
);
$list->SetList(
    $rsFiles,
    array(
        'SVA_TINYPNG_FILES_FILE_IMAGE' => function ($val, $arRec)
        {
            $upload_dir = COption::GetOptionString("main", "upload_dir", "upload");
            $src = "/" . $upload_dir . "/" . $arRec["SVA_TINYPNG_FILES_FILE_SUBDIR"] . "/" . $arRec["SVA_TINYPNG_FILES_FILE_FILE_NAME"];

            $strFilePath = CFile::GetPath($arRec["SVA_TINYPNG_FILES_FILE_ID"]);
            return "<img style='max-width: 200px; height: auto;' src='" . $strFilePath . "'>";
        },
        'COMPRESS' => function ($val, $arRec)
        {

            if(intval($arRec['FILE_ID']) <= 0)
            {
                return "<button value='" . $arRec["SVA_TINYPNG_FILES_FILE_ID"] . "' name='compress' data-image-id='" . $arRec["SVA_TINYPNG_FILES_FILE_ID"] . "'>" . GetMessage("SVA_TINYPNG_COMPRESS") . "</button>";
            }
            else
            {
                return GetMessage('SVA_TINYPNG_COMRESSED');
            }

        },
        'SIZE_BEFORE' => function ($val, $arRec)
        {
            return getNiceFileSize($arRec["SIZE_BEFORE"]);
        },
        'SVA_TINYPNG_FILES_FILE_FILE_SIZE' => function ($val, $arRec)
        {
            return getNiceFileSize($arRec["SVA_TINYPNG_FILES_FILE_FILE_SIZE"]);
        },
        'SIZE_AFTER' => function ($val, $arRec)
        {
            return getNiceFileSize($arRec["SIZE_AFTER"]);
        },
        'FILE_SIZE' => function ($val, $arRec)
        {
            return getNiceFileSize($arRec["FILE_SIZE"]);
        }
    ),
    false
);
$list->SetFooter(array(
    'compress' => GetMessage('SVA_TINYPNG_COMPRESS'),
));
$list->Output();