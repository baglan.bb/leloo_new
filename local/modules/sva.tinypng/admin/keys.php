<?
/** @global CUser $USER */
/** @global CMain $APPLICATION */
/** @global array $FIELDS */
use Bitrix\Main,
    Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc,
    Sva\TinyPng\KeysTable;

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');
//require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/catalog/prolog.php');

$moduleId = 'sva.tinypng';

Loc::loadMessages(__FILE__);

$publicMode = $adminPage->publicMode;
$selfFolderUrl = $adminPage->getSelfFolderUrl();

if (!($USER->CanDoOperation('tinypng_keys_read') || $USER->CanDoOperation('tinypng_keys')))
    $APPLICATION->AuthForm('');

Loader::includeModule($moduleId);

$readOnly = !$USER->CanDoOperation('catalog_group');

$canViewUserList = (
    $USER->CanDoOperation('view_subordinate_users')
    || $USER->CanDoOperation('view_all_users')
    || $USER->CanDoOperation('edit_all_users')
    || $USER->CanDoOperation('edit_subordinate_users')
);

if ($publicMode) $canViewUserList = false;

$adminListTableID = 'tbl_tinypng_keys';

$adminSort = new CAdminSorting($adminListTableID, 'ID', 'ASC');
$adminList = new CAdminUiList($adminListTableID, $adminSort);

$filterFields = array(
    array(
        "id" => "ACTIVE",
        "name" => GetMessage("SVA_TINYPNG_KEYS_ADMIN_ACTIVE"),
        "type" => "list",
        "items" => array(
            "Y" => GetMessage("IBLOCK_YES"),
            "N" => GetMessage("IBLOCK_NO")
        ),
        "sort" => "100"
    ),
    array(
        "id" => "ID",
        "name" => "ID",
        "quickSearch" => "=",
        "default" => true,
        "sort" => "200"
    ),
    array(
        "id" => "KEY",
        "name" => Loc::getMessage('SVA_TINYPNG_KEYS_ADMIN_KEY'),
        "default" => true,
        "sort" => "300"
    ),
    array(
        "id" => "COMPRESSION_COUNT",
        "name" => Loc::getMessage('SVA_TINYPNG_KEYS_ADMIN_COMPRESSION_COUNT'),
        "default" => 10,
        "type" => "number",
        "sort" => "400"
    )
);

$filter = array();
$adminList->AddFilter($filterFields, $filter);

//$roundValues = Catalog\Helpers\Admin\RoundEdit::getPresetRoundValues(true);

if (!$readOnly && $adminList->EditAction())
{
    if (!empty($FIELDS) && is_array($FIELDS))
    {
//        $listIds = array_filter(array_keys($FIELDS));
//        if (!empty($listIds))
//        {
//            $priceTypeList = array();
//            $iterator = Catalog\RoundingTable::getList(array(
//                'select' => array('ID', 'CATALOG_GROUP_ID'),
//                'filter' => array('@ID' => $listIds)
//            ));
//            while ($row = $iterator->fetch())
//                $priceTypeList[$row['CATALOG_GROUP_ID']] = $row['CATALOG_GROUP_ID'];
//            unset($row, $iterator);
//            Catalog\RoundingTable::clearPriceTypeIds();
//            Catalog\RoundingTable::setPriceTypeIds($priceTypeList);
//            Catalog\RoundingTable::disallowClearCache();
//            $conn = Main\Application::getConnection();
//            foreach ($FIELDS as $ruleId => $fields)
//            {
//                $ruleId = (int)$ruleId;
//                if ($ruleId <= 0 || !$adminList->IsUpdated($ruleId))
//                    continue;
//
//                Catalog\Helpers\Admin\RoundEdit::prepareFields($fields);
//
//                $conn->startTransaction();
//                $result = Catalog\RoundingTable::update($ruleId, $fields);
//                if ($result->isSuccess())
//                {
//                    $conn->commitTransaction();
//                }
//                else
//                {
//                    $conn->rollbackTransaction();
//                    $adminList->AddUpdateError(implode('<br>', $result->getErrorMessages()), $ruleId);
//                }
//                unset($result);
//            }
//            Catalog\RoundingTable::allowClearCache();
//            Catalog\RoundingTable::clearCache();
//            unset($fields, $ruleId);
//            unset($priceTypeList);
//        }
    }
}

if (!$readOnly && ($listIds = $adminList->GroupAction()))
{
    $listIds = array_filter($listIds);
    if (!empty($listIds))
    {
        $action = $_REQUEST['action'];
        switch ($action)
        {
            case 'delete':
                foreach ($listIds as $listId)
                {
                    KeysTable::delete($listId);
                }
                break;
        }
        unset($action);
    }
    unset($listIds);

    if ($adminList->hasGroupErrors())
    {
        $adminSidePanelHelper->sendJsonErrorResponse($adminList->getGroupErrors());
    }
    else
    {
        $adminSidePanelHelper->sendSuccessResponse();
    }
}

$headerList = array();
$headerList['ID'] = array(
    'id' => 'ID',
    'content' => 'ID',
    'sort' => 'ID',
    'default' => true
);
$headerList['KEY'] = array(
    'id' => 'KEY',
    'content' => Loc::getMessage('SVA_TINYPNG_KEYS_ADMIN_KEY'),
    'title' => Loc::getMessage('SVA_TINYPNG_KEYS_ADMIN_KEY'),
    'sort' => 'KEY',
    'default' => true
);
$headerList['COMPRESSION_COUNT'] = array(
    'id' => 'COMPRESSION_COUNT',
    'content' => Loc::getMessage('SVA_TINYPNG_KEYS_ADMIN_COMPRESSION_COUNT'),
    'title' => Loc::getMessage('SVA_TINYPNG_KEYS_ADMIN_COMPRESSION_COUNT'),
    'sort' => 'COMPRESSION_COUNT',
    'default' => true
);
$adminList->AddHeaders($headerList);

$selectFields = array_fill_keys($adminList->GetVisibleHeaderColumns(), true);
$selectFields['ID'] = true;
$selectFieldsMap = array_fill_keys(array_keys($headerList), false);
$selectFieldsMap = array_merge($selectFieldsMap, $selectFields);

global $by, $order;
if (!isset($by))
    $by = 'ID';
if (!isset($order))
    $order = 'ASC';

$userList = array();
$userIds = array();
$nameFormat = CSite::GetNameFormat(true);

$rowList = array();

$getListParams = array(
    'select' => array_keys($selectFields),
    'filter' => $filter,
    'order' => array($by => $order)
);
$ruleIterator = new CAdminUiResult(KeysTable::getList($getListParams), $adminListTableID);
$ruleIterator->NavStart();

CTimeZone::Disable();
$adminList->SetNavigationParams($ruleIterator, array("BASE_LINK" => $selfFolderUrl."cat_round_list.php"));
while ($rule = $ruleIterator->Fetch())
{
    $rule['ID'] = (int)$rule['ID'];
    if ($selectFieldsMap['CREATED_BY'])
    {
        $rule['CREATED_BY'] = (int)$rule['CREATED_BY'];
        if ($rule['CREATED_BY'] > 0)
            $userIds[$rule['CREATED_BY']] = true;
    }
    if ($selectFieldsMap['MODIFIED_BY'])
    {
        $rule['MODIFIED_BY'] = (int)$rule['MODIFIED_BY'];
        if ($rule['MODIFIED_BY'] > 0)
            $userIds[$rule['MODIFIED_BY']] = true;
    }

    $urlEdit = $selfFolderUrl.'sva_tinypng_keys_edit.php?ID='.$rule['ID'].'&lang='.LANGUAGE_ID;
    $urlEdit = $adminSidePanelHelper->editUrlToPublicPage($urlEdit);
    $row = &$adminList->AddRow(
        $rule['ID'],
        $rule,
        $urlEdit,
        (!$readOnly ? Loc::getMessage('PRICE_ROUND_LIST_MESS_EDIT_RULE') : Loc::getMessage('PRICE_ROUND_LIST_MESS_VIEW_RULE'))
    );
    $row->AddViewField('ID', '<a href="'.$urlEdit.'">'.$rule['ID'].'</a>');

    if ($selectFieldsMap['DATE_CREATE'])
        $row->AddViewField('DATE_CREATE', $rule['DATE_CREATE']);
    if ($selectFieldsMap['TIMESTAMP_X'])
        $row->AddViewField('TIMESTAMP_X', $rule['TIMESTAMP_X']);

    $row->AddViewField(
        'CATALOG_GROUP_ID',
        (isset($priceTypeList[$rule['CATALOG_GROUP_ID']]) ? $priceTypeList[$rule['CATALOG_GROUP_ID']] : (int)$rule['CATALOG_GROUP_ID'])
    );

    if ($selectFieldsMap['PRICE'])
    {
        $row->AddViewField(
            'PRICE',
            Loc::getMessage(
                'PRICE_ROUND_LIST_PRICE_TEMPLATE',
                array('#PRICE#' => $rule['PRICE'])
            )
        );
    }

    if (!$readOnly)
    {
        if ($selectFieldsMap['PRICE'])
            $row->AddInputField('PRICE');
        if ($selectFieldsMap['ROUND_TYPE'])
            $row->AddSelectField('ROUND_TYPE', $roundTypeList);
        if ($selectFieldsMap['ROUND_PRECISION'])
            $row->AddSelectField('ROUND_PRECISION', $roundValues);
    }
    else
    {
        if ($selectFieldsMap['ROUND_TYPE'])
            $row->AddSelectField('ROUND_TYPE', $roundTypeList, false);
        if ($selectFieldsMap['ROUND_PRECISION'])
            $row->AddSelectField('ROUND_PRECISION', $roundValues, false);
    }
    $actions = array();
    $actions[] = array(
        'ICON' => 'edit',
        'TEXT' => (!$readOnly ? Loc::getMessage('SVA_TINYPNG_KEYS_ADMIN_KEY_EDIT') : Loc::getMessage('SVA_TINYPNG_KEYS_ADMIN_KEY_VIEW')),
        'LINK' => $urlEdit,
        'DEFAULT' => true
    );

    if (!$readOnly)
    {
        $actions[] = array(
            'ICON' => 'copy',
            'TEXT' => Loc::getMessage('PRICE_ROUND_LIST_CONTEXT_COPY'),
            'LINK' => $urlEdit.'&action=copy',
            'DEFAULT' => false,
        );
        $actions[] = array(
            'ICON' =>'delete',
            'TEXT' => Loc::getMessage('PRICE_ROUND_LIST_CONTEXT_DELETE'),
            'ACTION' => "if (confirm('".Loc::getMessage('PRICE_ROUND_LIST_CONTEXT_DELETE_CONFIRM')."')) ".$adminList->ActionDoGroup($rule['ID'], 'delete')
        );
    }

    $row->AddActions($actions);
    unset($actions);

    $rowList[$rule['ID']] = $row;
    unset($row);
    unset($urlEdit);
}
unset($rule);
CTimeZone::Enable();

unset($roundValues);

if (!empty($rowList) && ($selectFieldsMap['CREATED_BY'] || $selectFieldsMap['MODIFIED_BY']))
{
    if (!empty($userIds))
    {
        $userIterator = Main\UserTable::getList(array(
            'select' => array('ID', 'LOGIN', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'EMAIL'),
            'filter' => array('@ID' => array_keys($userIds)),
        ));
        while ($oneUser = $userIterator->fetch())
        {
            $oneUser['ID'] = (int)$oneUser['ID'];
            if ($canViewUserList)
                $userList[$oneUser['ID']] = '<a href="/bitrix/admin/user_edit.php?lang='.LANGUAGE_ID.'&ID='.$oneUser['ID'].'">'.CUser::FormatName($nameFormat, $oneUser).'</a>';
            else
                $userList[$oneUser['ID']] = CUser::FormatName($nameFormat, $oneUser);
        }
        unset($oneUser, $userIterator);
    }

    /** @var CAdminListRow $row */
    foreach ($rowList as &$row)
    {
        if ($selectFieldsMap['CREATED_BY'])
        {
            $userName = '';
            if ($row->arRes['CREATED_BY'] > 0 && isset($userList[$row->arRes['CREATED_BY']]))
                $userName = $userList[$row->arRes['CREATED_BY']];
            $row->AddViewField('CREATED_BY', $userName);
        }
        if ($selectFieldsMap['MODIFIED_BY'])
        {
            $userName = '';
            if ($row->arRes['MODIFIED_BY'] > 0 && isset($userList[$row->arRes['MODIFIED_BY']]))
                $userName = $userList[$row->arRes['MODIFIED_BY']];
            $row->AddViewField('MODIFIED_BY', $userName);
        }
        unset($userName);
    }
    unset($row);
}

$adminList->AddFooter(
    array(
        array(
            'title' => Loc::getMessage('MAIN_ADMIN_LIST_SELECTED'),
            'value' => $ruleIterator->SelectedRowsCount()
        ),
        array(
            'counter' => true,
            'title' => Loc::getMessage('MAIN_ADMIN_LIST_CHECKED'),
            'value' => 0
        ),
    )
);

$adminList->AddGroupActionTable(array("delete" => true));

$contextMenu = array();
if (!$readOnly)
{
    $addUrl = $selfFolderUrl."sva_tinypng_keys_edit.php?lang=".LANGUAGE_ID;
    $addUrl = $adminSidePanelHelper->editUrlToPublicPage($addUrl);
    $contextMenu[] = array(
        'ICON' => 'btn_new',
        'TEXT' => Loc::getMessage('SVA_TINYPNG_KEYS_ADMIN_KEY_ADD'),
        'TITLE' => Loc::getMessage('SVA_TINYPNG_KEYS_ADMIN_KEY_ADD'),
        'LINK' => $addUrl
    );
}
if (!empty($contextMenu))
{
    $adminList->setContextSettings(array("pagePath" => $selfFolderUrl."cat_round_list.php"));
    $adminList->AddAdminContextMenu($contextMenu);
}

unset($ruleEditUrl);

$adminList->CheckListMode();

$APPLICATION->SetTitle(Loc::getMessage('SVA_TINYPNG_KEYS_TITLE'));
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');

$adminList->DisplayFilter($filterFields);
$adminList->DisplayList();
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php');



//$module = 'sva.tinypng';
//
//use Bitrix\Main\Localization\Loc;
//
//require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');
//require_once($_SERVER['DOCUMENT_ROOT'].'/local/modules/'.$module.'/include.php');
//IncludeModuleLangFile(__FILE__);
//
//$arSites = array();
//$rsSites = CSite::GetList($by1='sort', $order1='desc');
//while ($arSite = $rsSites->GetNext()) {
//	$arSites[$arSite['ID']] = '['.$arSite['ID'].'] '.$arSite['NAME'];
//}
//
//CModule::IncludeModule($module);
//
//if($_REQUEST["compress"]){
//
//	if(intval($_REQUEST["compress"])){
//
//		$intFileID = intval($_REQUEST["compress"]);
//
//		\Sva\TinyPng\TinyPngClient::CompressImageByID($intFileID);
//	}
//
//} elseif($_REQUEST["action"] == "compress"){
//
//	$arIDs = $_REQUEST["ID"];
//
//	if(is_array($arIDs) && count($arIDs)){
//
//		set_time_limit(0);
//
//		\TinyPng\Tinify\Tinify::setKey($key);
//
//		foreach($arIDs as $intFileID){
//
//			\Sva\TinyPng\TinyPngClient::CompressImageByID($intFileID);
//		}
//	}
//}
//
///********************************FILTER****************************************/
//
///******************************** LIST ****************************************/
//$sTableID = "tbl_tinypng_keys";
//
//$oSort = new CAdminSorting($sTableID, "ID", "asc");
//$lAdmin = new CAdminList($sTableID, $oSort);
//
//
//$dbResultList = CSmileGallery::getList(Array(
//    'SELECT' => Array('ID', 'STRING_ID', 'NAME', 'SORT', 'SMILE_COUNT'),
//    'FILTER' => $arFilter,
//    'ORDER' => array($by => $order),
//    'NAV_PARAMS' => array("nPageSize"=>CAdminResult::GetNavSize($sTableID)),
//    'RETURN_RES' => 'Y'
//));
//
//$dbResultList = new CAdminResult($dbResultList, $sTableID);
//$dbResultList->NavStart();
//
//$lAdmin->NavText($dbResultList->GetNavPrint(GetMessage("SMILE_NAV")));
//
//$lAdmin->AddHeaders(array(
//    array("id"=>"ID", "content"=>GetMessage("SMILE_ID"), "sort"=>"ID", "default"=>false),
//    array("id"=>"NAME", "content"=>GetMessage("SMILE_NAME"), "default"=>true),
//    array("id"=>"STRING_ID", "content"=>GetMessage("SMILE_STRING_ID"), "default"=>false),
//    array("id"=>"SORT","content"=>GetMessage("SMILE_SORT"), "sort"=>"SORT", "default"=>true, "align"=>"right"),
//    array("id"=>"SMILE_COUNT","content"=>GetMessage("SMILE_SMILE_COUNT"), "sort"=>"SMILE_COUNT", "default"=>true),
//));
//
//$arVisibleColumns = $lAdmin->GetVisibleHeaderColumns();
//
//while ($arForum = $dbResultList->NavNext(true, "f_"))
//{
//    $row =& $lAdmin->AddRow($f_ID, $arForum);
//
//    $row->AddField("ID", $f_ID);
//    $row->AddField("SORT", $f_SORT);
//    $row->AddViewField("NAME", '<a href="'."smile_set.php?GALLERY_ID=".$f_ID."&lang=".LANG."&".GetFilterParams("filter_").'">'.(strlen($f_NAME)>0?$f_NAME: GetMessage('SMILE_GALLERY_NAME', Array('#ID#' => $f_ID))).'</a>');
//    $row->AddField("SMILE_COUNT", $f_SMILE_COUNT);
//
//
//    $row->AddInputField("NAME", array("size"=>20));
//    $row->AddInputField("SORT", array("size"=>5));
//
//    if ($f_STRING_ID == 'bitrix')
//    {
//        $row->AddField("STRING_ID", $f_STRING_ID);
//        $arActions = Array(
//            array("ICON"=>"edit", "TEXT"=>GetMessage("SMILE_EDIT_DESCR"), "ACTION"=>$lAdmin->ActionRedirect("smile_gallery_edit.php?ID=".$f_ID."&lang=".LANG."&".GetFilterParams("filter_").""), "DEFAULT"=>true),
//        );
//    }
//    else
//    {
//        $row->AddInputField("STRING_ID", array("size"=>20));
//        $arActions = Array(
//            array("ICON"=>"edit", "TEXT"=>GetMessage("SMILE_EDIT_DESCR"), "ACTION"=>$lAdmin->ActionRedirect("smile_gallery_edit.php?ID=".$f_ID."&lang=".LANG."&".GetFilterParams("filter_").""), "DEFAULT"=>true),
//            array("SEPARATOR" => true),
//            array("ICON"=>"delete", "TEXT"=>GetMessage("SMILE_DELETE_DESCR"), "ACTION"=>"if(confirm('".GetMessage('SMILE_DEL_CONF')."')) ".$lAdmin->ActionDoGroup($f_ID, "delete"))
//        );
//    }
//    $row->AddActions($arActions);
//
//}
//
//$aContext = array(
//    array(
//        "TEXT" => GetMessage("SMILE_BTN_ADD_NEW"),
//        "LINK" => "smile_gallery_edit.php?lang=".LANG,
//        "TITLE" => GetMessage("SMILE_BTN_ADD_NEW_ALT"),
//        "ICON" => "btn_new",
//    ),
//);
//$lAdmin->AddAdminContextMenu($aContext);
//$lAdmin->CheckListMode();
//
//$APPLICATION->SetTitle(GetMessage("SMILE_TITLE"));
//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
//
//$lAdmin->DisplayList();
//
//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
//$lAdmin = new CAdminList($module);
//
//$arFilterFields = array();
//$lAdmin->InitFilter($arFilterFields);
//
//$aContext = array(
//    array(
//        "TEXT" => GetMessage("SMILE_BTN_ADD_NEW"),
//        "LINK" => "smile_gallery_edit.php?lang=".LANG,
//        "TITLE" => GetMessage("SMILE_BTN_ADD_NEW_ALT"),
//        "ICON" => "btn_new",
//    ),
//);
//$lAdmin->AddAdminContextMenu($aContext);
//
//$lAdmin->AddHeaders(array(
//	'ID' => "ID",
//	'KEY' => Loc::getMessage('SVA_TINYPNG_KEYS_ADMIN_KEY'),
//	'COMPRESSION_COUNT' => Loc::getMessage("SVA_TINYPNG_KEYS_ADMIN_COMPRESSION_COUNT"),
//));
//
//$arVisibleColumns = $lAdmin->GetVisibleHeaderColumns();
//
//if (!isset($by))
//	$by = 'ID';
//if (!isset($order))
//	$order = 'ASC';
//
//$sTableID = "tbl_tinypng_keys";
//
//$rsFiles = Sva\TinyPng\TinyPngKeyTable::getList();
//$dbResultList = new CAdminResult($rsFiles, $sTableID);
//$dbResultList->NavStart();
//$lAdmin->NavText($dbResultList->GetNavPrint(GetMessage("SMILE_NAV")));
////$lAdmin->SetList(
////	$rsFiles,
////	array(
////		'IMAGE' => function($val, $arRec){
////			$strFilePath = CFile::GetPath($arRec["ID"]);
////			return "<img style='max-width: 200px; height: auto;' src='" . $strFilePath . "'>";
////		},
////		'COMPRESS' => function($val, $arRec){
////
////			if(intval($arRec['FILE_ID']) <= 0){
////				return "<button value='".$arRec["ID"]."' name='compress' data-image-id='"  . $arRec["ID"] . "' href='#'>" . Loc::getMessage("SVA_TINYPNG_COMPRESS") . "</button>";
////			} else {
////				return Loc::getMessage('SVA_TINYPNG_COMRESSED');
////			}
////
////		},
////	),
////	false
////);
//$aContext = array(
//    array(
//        "TEXT" => GetMessage("SMILE_BTN_ADD_NEW"),
//        "LINK" => "smile_gallery_edit.php?lang=".LANG,
//        "TITLE" => GetMessage("SMILE_BTN_ADD_NEW_ALT"),
//        "ICON" => "btn_new",
//    ),
//);
//$lAdmin->AddAdminContextMenu($aContext);
//$lAdmin->CheckListMode();
//
//$APPLICATION->SetTitle(GetMessage("SMILE_TITLE"));
//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
//
//$lAdmin->DisplayList();
//
//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");