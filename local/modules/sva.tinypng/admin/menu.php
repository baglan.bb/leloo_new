<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php');
IncludeModuleLangFile(__FILE__);

use Bitrix\Main\Localization\Loc;

$aMenu = array(
    'parent_menu' => 'global_menu_services',
    'section' => 'sva.tinypng',
    'sort' => 85,
    'text' => Loc::getMessage('SVA_TINYPNG_MAIN_TITLE'),
    'title' => Loc::getMessage('SVA_TINYPNG_MAIN_TITLE'),
    'url' => 'sva_tinypng_files.php?lang=' . LANGUAGE_ID,
    'icon' => 'sva_tinypng_menu_icon',
    'page_icon' => 'sva_tinypng_files_icon',
    'items_id' => 'menu_sva_tinypng',
    'items' => Array(
        array(
            "module_id" => "sva.tinypng_files",
            "text" => Loc::getMessage("SVA_TINYPNG_FILES"),
            "title" => Loc::getMessage("SVA_TINYPNG_FILES"),
            "items_id" => "menu_sva_tinypng_files",
            'url' => 'sva_tinypng_files.php?lang=' . LANGUAGE_ID,
        ),
        array(
            "module_id" => "sva.tinypng_keys",
            "text" => Loc::getMessage("SVA_TINYPNG_KEYS"),
            "title" => Loc::getMessage("SVA_TINYPNG_KEYS"),
            "items_id" => "menu_sva_tinypng_keys",
            'url' => 'sva_tinypng_keys.php?lang=' . LANGUAGE_ID,
            "more_url" => Array(
                "menu_sva_tinypng_keys_admin.php",
                "menu_sva_tinypng_keys_edit.php"
            )
        )
    )
);

if(!empty($aMenu))
{
    return $aMenu;
}
else
{
    return false;
}