<?
/** @global CMain $APPLICATION */

use Bitrix\Main,
    Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc,
    Bitrix\Catalog,
    Sva\TinyPng\TinyPngClient;

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php');

Loc::loadMessages(__FILE__);

$selfFolderUrl = $adminPage->getSelfFolderUrl();
$listUrl = $selfFolderUrl . "sva_tinypng_keys.php?lang=" . LANGUAGE_ID;
$listUrl = $adminSidePanelHelper->editUrlToPublicPage($listUrl);

if(!($USER->CanDoOperation('tinypng_keys_read') || $USER->CanDoOperation('tinypng_keys')))
    $APPLICATION->AuthForm('');
Loader::includeModule('sva.tinypng');
$readOnly = !$USER->CanDoOperation('tinypng_keys');

$request = Main\Context::getCurrent()->getRequest();

$returnUrl = '';
$rawReturnUrl = (string)$request->get('return_url');
if($rawReturnUrl != '')
{
    $currentUrl = $APPLICATION->GetCurPage();
    if(strtolower(substr($rawReturnUrl, strlen($currentUrl))) != strtolower($currentUrl))
        $returnUrl = $rawReturnUrl;
}
unset($rawReturnUrl);

$formID = 'tinyPngKeyEdit';
$control = new CAdminForm($ruleFormID, array(
    array(
        'ICON' => 'tinypng',
        'DIV' => 'keyEdit',
        'TAB' => Loc::getMessage('SVA_TINYPNG_KEY_EDIT_TAB_NAME_COMMON'),
        'TITLE' => Loc::getMessage('SVA_TINYPNG_KEY_EDIT_TAB_NAME_COMMON')
    )
));
$control->SetShowSettings(false);
$formID .= '_form';

$errors = array();
$fields = array();
$copy = false;
$keyID = (int)$request->get('ID');
if($keyID < 0)
    $keyID = 0;

if($keyID > 0)
    $copy = ($request->get('action') == 'copy');

if(
    check_bitrix_sessid()
    && !$readOnly
    && $request->isPost()
    && (string)$request->getPost('Update') == 'Y'
)
{
    $adminSidePanelHelper->decodeUriComponent($request);

    $rawData = $request->getPostList();

    if(!empty($rawData['KEY']))
        $fields['KEY'] = $rawData['KEY'];

    $fields["COMPRESSION_COUNT"] = 0;

    //Get compressed count
    //    $obTinyPng = new Sva\TinyPng\Tinify();
    //    $obTinyPng->addClient();
    //    try
    //    {
    //        $client = new Sva\TinyPng\Client($fields['KEY']);
    //    }
    //    catch (\Exception $e)
    //    {
    //        if($e->getMessage() != "InputMissing"){
    //            throw new \Error($e->getMessage());
    //        }
    //    }
    //    var_dump($client);
    //    $fields["COMPRESSION_COUNT"] = $client->getCompressedCount();

    if($keyID == 0 || $copy)
        $result = Sva\TinyPng\KeysTable::add($fields);
    else
        $result = Sva\TinyPng\KeysTable::update($keyID, $fields);

    $fields["ID"] = $keyID;
//    $client = new Sva\TinyPng\Client($fields);
//    try
//    {
//        $client->requestCompressionCount();
//    }
//    catch (\Exception $e)
//    {
//        if($e->getMessage() != "InputMissing")
//        {
//            throw new \Error($e->getMessage());
//        }
//    }
//    $client->save();

    if(!$result->isSuccess())
    {
        $errors = $result->getErrorMessages();
    }
    else
    {
        if($keyID == 0 || $copy)
            $keyID = $result->getId();
    }
    unset($result);
    unset($rawData);

    if(empty($errors))
    {
        if($adminSidePanelHelper->isAjaxRequest())
        {
            $adminSidePanelHelper->sendSuccessResponse("base", array("ID" => $keyID));
        }
        else
        {
            if((string)$request->getPost('apply') != '')
            {
                $applyUrl = $selfFolderUrl . "cat_round_edit.php?lang=" . $lang . "&ID=" . $keyID . '&' . $control->ActiveTabParam();
                $applyUrl = $adminSidePanelHelper->setDefaultQueryParams($applyUrl);
                LocalRedirect($applyUrl);
            }
            else
            {
                $adminSidePanelHelper->localRedirect($listUrl);
                LocalRedirect($listUrl);
            }
        }
    }
    else
    {
        $adminSidePanelHelper->sendJsonErrorResponse($errors);
    }
}

$APPLICATION->SetTitle(
    $keyID == 0
        ? Loc::getMessage('SVA_TINYPNG_KEY_EDIT_ADD')
        : (
    !$copy
        ? Loc::getMessage('SVA_TINYPNG_KEY_EDIT_UPDATE', array('#ID#' => $keyID))
        : Loc::getMessage('SVA_TINYPNG_KEY_EDIT_COPY', array('#ID#' => $keyID))
    )
);

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php');

$contextMenuItems = array(
    array(
        'ICON' => 'btn_list',
        'TEXT' => Loc::getMessage('SVA_TINYPNG_KEY_EDIT_LIST'),
        'LINK' => $listUrl
    )
);
if(!$readOnly && $keyID > 0)
{
    if(!$copy)
    {
        $addUrl = $selfFolderUrl . "sva_tinypng_keys_edit.php?lang=" . LANGUAGE_ID;
        $addUrl = $adminSidePanelHelper->editUrlToPublicPage($addUrl);
        $contextMenuItems[] = array('SEPARATOR' => 'Y');
        $contextMenuItems[] = array(
            'ICON' => 'btn_new',
            'TEXT' => Loc::getMessage('SVA_TINYPNG_KEY_EDIT_ADD'),
            'LINK' => $addUrl
        );
        $contextMenuItems[] = array(
            'ICON' => 'btn_copy',
            'TEXT' => Loc::getMessage('SVA_TINYPNG_KEY_EDIT_COPY'),
            'LINK' => $addUrl . '&ID=' . $keyID . '&action=copy'
        );
        $deleteUrl = $selfFolderUrl . "sva_tinypng_keys.php?lang=" . LANGUAGE_ID . "&ID=" . $keyID . "&action=delete&" . bitrix_sessid_get() . "";
        $buttonAction = "LINK";
        if($adminSidePanelHelper->isPublicFrame())
        {
            $deleteUrl = $adminSidePanelHelper->editUrlToPublicPage($deleteUrl);
            $buttonAction = "ONCLICK";
        }
        $contextMenuItems[] = array(
            'ICON' => 'btn_delete',
            'TEXT' => Loc::getMessage('SVA_TINYPNG_KEY_EDIT_DELETE'),
            $buttonAction => "javascript:if (confirm('" . CUtil::JSEscape(Loc::getMessage('SVA_TINYPNG_KEY_EDIT_DELETE_CONFIRM')) . "')) top.window.location.href='" . $deleteUrl . "';",
            'WARNING' => 'Y',
        );
    }
}

$contextMenu = new CAdminContextMenu($contextMenuItems);
$contextMenu->Show();
unset($contextMenu, $contextMenuItems);

if(!empty($errors))
{
    $errorMessage = new CAdminMessage(
        array(
            'DETAILS' => implode('<br>', $errors),
            'TYPE' => 'ERROR',
            'MESSAGE' => Loc::getMessage('PRICE_ROUND_EDIT_ERR_SAVE'),
            'HTML' => true
        )
    );
    echo $errorMessage->Show();
    unset($errorMessage);
}

$defaultValues = array(
    'ID' => 0,
    'KEY' => '',
    'COMPRESSION_COUNT' => 0,
);
$selectFields = array_keys($defaultValues);
$selectFields[] = 'ID';

$key = array();
if($keyID > 0)
{
    $key = \Sva\TinyPng\KeysTable::getList(array(
        'select' => $selectFields,
        'filter' => array('=ID' => $keyID)
    ))->fetch();

    if(!$key)
        $keyID = 0;
}
if($keyID == 0)
    $key = $defaultValues;

$key['ID'] = (int)$key['ID'];
$key['KEY'] = (string)$key['KEY'];
$key['COMPRESSION_COUNT'] = (int)$key['COMPRESSION_COUNT'];

if(!empty($errors))
    $key = array_merge($key, $fields);

$control->BeginPrologContent();
$control->EndPrologContent();
$control->BeginEpilogContent();
echo GetFilterHiddens("filter_"); ?>
    <input type="hidden" name="Update" value="Y">
    <input type="hidden" name="lang" value="<? echo LANGUAGE_ID; ?>">
    <input type="hidden" name="ID" value="<? echo $keyID; ?>">
<?
if($copy)
{
    ?><input type="hidden" name="action" value="copy"><?
}
if(!empty($returnUrl))
{
    ?><input type="hidden" name="return_url" value="<? echo htmlspecialcharsbx($returnUrl); ?>"><?
}
echo bitrix_sessid_post();
$control->EndEpilogContent();
$formActionUrl = $selfFolderUrl . 'sva_tinypng_keys_edit.php?lang=' . LANGUAGE_ID;
$formActionUrl = $adminSidePanelHelper->setDefaultQueryParams($formActionUrl);
$control->Begin(array('FORM_ACTION' => $formActionUrl));
$control->BeginNextFormTab();

if($keyID > 0 && !$copy)
    $control->AddViewField('ID', Loc::getMessage('SVA_TINYPNG_KEY_EDIT_FIELD_ID'), $keyID, false);

$control->AddViewField('COMPRESSION_COUNT', Loc::getMessage('SVA_TINYPNG_KEY_EDIT_FIELD_COMPRESSION_COUNT'), $key["COMPRESSION_COUNT"], false);

$control->AddEditField('KEY', Loc::getMessage('SVA_TINYPNG_KEY_EDIT'), true, array(), $key['KEY']);

$control->Buttons(array('disabled' => $readOnly, 'back_url' => $listUrl));
$control->Show();
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');