<?

use Bitrix\Main\Localization\Loc;
use Sotbit\Origami\Helper\Config;
use Bitrix\Main\Loader;
use Bitrix\Main\Page\Asset;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$APPLICATION->ShowAjaxHead();
Asset::getInstance()->addCss(SITE_DIR . "local/templates/.default/components/bitrix/form.result.new/origami_wantgift/style.css");

Loader::includeModule('sotbit.origami');
?>

<div class="sotbit_want_gift_wrapper">
    <div class="sotbit_order_phone__title_narrowly">

        <? if ($arResult["isFormTitle"]) { ?>
            <div class="sotbit_order_phone__title"><?= $arResult["FORM_TITLE"] ?>
            </div>
        <? } ?>

    </div>
    <div class="want_gift-resizeable_content">
        <? if ($arResult["IMG_PRODUCT"]["SRC"]) : ?>
            <div class="sotbit_want_gift_image">
                <img src="<?= $arResult["IMG_PRODUCT"]["SRC"] ?>" alt="<?= $arResult["IMG_PRODUCT"]["NAME"] ?>">
                <div class="sotbit_want_gift_name"><?= $arResult["IMG_PRODUCT"]["NAME"] ?></div>
                <div class="sotbit_want_gift_price">
                    <?= $arResult["IMG_PRODUCT"]["PRICE"] ?>
                </div>
                <? if ($arResult["IMG_PRODUCT"]["OLD_PRICE"]): ?>
                    <div class="sotbit_want_gift_oldprice">
                        <?= $arResult["IMG_PRODUCT"]["OLD_PRICE"] ?>
                    </div>
                <? endif; ?>
            </div>
        <? endif; ?>

        <div class="sotbit_order_phone">
            <div class="sotbit_order_phone__title_wide">
                <? if ($arResult["isFormTitle"]) { ?>
                    <div class="sotbit_order_phone__title"><?= $arResult["FORM_TITLE"] ?>
                    </div>
                <? } ?>
            </div>
            <div class="want_gift-resizeable_content_wide">
                <div class="popup-error-message">
                    <? if ($arResult["isFormErrors"] == "Y"): ?><?= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>
                </div>
                <div class="sotbit_order_success_show">

                    <? if ($arResult["FORM_NOTE"]) : ?>
                        <div class="popup-window-message-content">

                            <svg class="popup-window-icon-check">
                                <use xlink:href="/local/templates/sotbit_origami/assets/img/sprite.svg#icon_check_form"></use>
                            </svg>

                            <div>
                                <div class="popup-window-message-title"><?= GetMessage('OK_THANKS'); ?></div>
                                <div style="font-size: 16px;"><?= $arResult["FORM_NOTE"] ?></div>
                            </div>

                        </div>
                    <? endif; ?>

                </div>
                <? if (empty($arResult["FORM_NOTE"]))
                {
                ?>
                <?= $arResult["FORM_HEADER"] ?>
                <?
                foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) {
                    ?>

                    <div class="sotbit_order_phone__block">
                        <?
                        if ($arQuestion['CAPTION'] !== Loc::getMessage('OK_LINK_PRODUCT')) { ?>
                            <p class="sotbit_order_phone__block_title">
                                <?= $arQuestion['CAPTION'] ?>
                                <?= ($arQuestion['REQUIRED'] == 'Y') ? '*' : '' ?>
                            </p>
                            <?
                        } ?>

                        <?
                        if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] != 'textarea'):?>
                            <input
                                    type="<?= ($arQuestion['CAPTION'] !== Loc::getMessage('OK_LINK_PRODUCT')) ? $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] : 'hidden' ?>"
                                    name="form_<?= ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'tel') ? 'text' : $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>_<?= $arQuestion['STRUCTURE'][0]['ID'] ?>"
                                    value="<?= ($arQuestion['CAPTION'] !== Loc::getMessage('OK_LINK_PRODUCT')) ? $arQuestion['STRUCTURE'][0]['VALUE'] :
                                        (!empty($_REQUEST['url']) ? $_REQUEST['url'] : $_REQUEST["form_" . $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] . "_" . $arQuestion['STRUCTURE'][0]["ID"]]) ?>"
                                <?= ($arQuestion['REQUIRED'] == 'Y') ? 'required' : '' ?>
                                <?= ($arQuestion['STRUCTURE'][0]['MASK']) ? 'placeholder="' . $arQuestion['STRUCTURE'][0]['MASK'] . '"' : '' ?>
                                <?= ($arQuestion['STRUCTURE'][0]['PATTERN']) ? 'pattern="' . $arQuestion['STRUCTURE'][0]['PATTERN'] . '"' : '' ?>
                            >
                        <? else: ?>
                            <?= $arQuestion["HTML_CODE"] ?>
                        <? endif ?>
                    </div>
                    <?
                }
                ?>

                <input type="hidden" name="img" value="<?= $arResult["IMG_PRODUCT"]["SRC"] ?>"/>
                <input type="hidden" name="name" value="<?= $arResult["IMG_PRODUCT"]["NAME"] ?>"/>
                <input type="hidden" name="price" value="<?= $arResult["IMG_PRODUCT"]["PRICE"] ?>"/>

                <? if ($arResult["isUseCaptcha"] == "Y") {
                    ?>
                    <div class="sotbit_order_phone__block">
                        <div class="feedback_block__captcha">
                            <p class="popup-window-field_description"><?= Loc::getMessage('CAPTCHA_TITLE') ?>
                            </p>
                            <div class="feedback_block__captcha_input">
                                <input type="text" name="captcha_word" size="30" maxlength="50" value="" required/>
                            </div>
                            <div class="feedback_block__captcha_img">
                                <input type="hidden" name="captcha_sid"
                                       value="<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>"/>
                                <img

                                    src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>"/>
                                <div class="captcha-refresh"  onclick="reloadCaptcha(this,'<?= SITE_DIR ?>');return false;">
                                    <svg class="icon_refresh" width="16" height="14">
                                        <use
                                            xlink:href="/local/templates/sotbit_origami/assets/img/sprite.svg#icon_refresh"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?
                } ?>

                <div class="confidential-field">
                    <div class="feedback_block__compliance">
                        <input type="checkbox"
                               id="personal_phone_personal"
                               name="personal"
                               class="checkbox__input"
                               checked="checked">
                        <label for="personal_phone_personal" class="checkbox__label fonts__middle_comment">
                            <?= Loc::getMessage('OK_CONFIDENTIAL') ?>
                            <a href="<?= Config::get('CONFIDENTIAL_PAGE', $arParams['SITE_ID']) ?>" target="_blank">
                                <?= Loc::getMessage('OK_CONFIDENTIAL2') ?>  </a>
                        </label>
                    </div>
                </div>
                <div class="popup-window-submit_button">
                    <input type="button" name="web_form_submit"
                           value="<?= htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?>"
                           onclick="sendForm('<?= $arResult['arForm']['SID'] ?>', '<?= Config::get('COLOR_BASE', $arParams['SITE_ID']) ?>')">
                    <input type="submit" name="web_form_submit" style="display:none;"
                           value="<?= htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?>">
                    <?= $arResult["FORM_FOOTER"] ?>
                    <?
                    } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function sendForm(sid, color) {
        if ($("form[name='" + sid + "'] input[name='personal']").is(':checked')) {
            $("form[name='" + sid + "'] input[type='submit']").trigger('click');
        } else {
            $('.feedback_block__compliance svg path').css({'stroke': color, 'stroke-dashoffset': 0});
        }
    }
</script>

<script>
    ;(function resize() {
        let wrapper,
            wrappers = document.querySelectorAll(".wrap-popup-window");

        for (let i = 0; i < wrappers.length; i++) {
            if (wrappers[i].querySelector(".want_gift-resizeable_content_wide")) {
                wrapper = wrappers[i];
                break;
            }
        }

        let popupContent = wrapper.querySelector(".popup-content");

        $(popupContent).ready(
            resizeWantGiftPopup()
        );

        function resizeWantGiftPopup() {
            let popupResizeableContentWide = wrapper.querySelector(".want_gift-resizeable_content_wide"),
                popupResizeableContentNarrowly = wrapper.querySelector(".want_gift-resizeable_content"),
                titleWide = wrapper.querySelector(".sotbit_order_phone__title_wide"),
                titleNarrowly = wrapper.querySelector(".sotbit_order_phone__title_narrowly"),
                popupContent = wrapper.querySelector(".popup-content"),
                popupWindow = wrapper.querySelector(".popup-window");

            setUpListeners();
            resize();

            function setUpListeners() {
                popupResizeableContentWide.addEventListener("scroll", putTitleShadow);

                popupResizeableContentNarrowly.addEventListener("scroll", putTitleShadow);

                window.addEventListener("resize", () => {
                    resize();
                    putTitleShadow();
                });

                popupContent.addEventListener("load", () => {
                    resize();
                    putTitleShadow();
                });
            }

            function resize() {
                let wantGiftImage = wrapper.querySelector(".sotbit_want_gift_image");

                if (titleWide.clientHeight > titleNarrowly.clientHeight) {
                    resizePopupContent(popupResizeableContentWide, titleWide, popupResizeableContentNarrowly);

                    if (wantGiftImage.clientHeight > popupWindow.clientHeight) {
                        wantGiftImage.style.height = popupWindow.clientHeight + "px";
                    } else {
                        wantGiftImage.style.height = "auto";
                    }

                } else {
                    resizePopupContent(popupResizeableContentNarrowly, titleNarrowly, popupResizeableContentWide);
                }
            }

            function resizePopupContent(resizeableContent, title, unresizeable) {
                let clientHeight = document.documentElement.clientHeight * 0.97,
                    newHeight;

                resizeableContent.style.overflowY = "hidden";
                resizeableContent.style.height = "auto";

                unresizeable.style.overflowY = "hidden";
                unresizeable.style.height = "auto";

                if (popupContent.clientHeight > popupWindow.clientHeight || popupContent.clientHeight > clientHeight) {

                    newHeight = (clientHeight < popupWindow.clientHeight) ?
                        (clientHeight - title.clientHeight) :
                        (popupWindow.clientHeight - title.clientHeight);

                    resizeableContent.style.overflowY = "auto";
                    resizeableContent.style.height = newHeight + "px";
                }
            }

            function putTitleShadow() {
                let scrolledWide = popupResizeableContentWide.scrollTop,
                    scrolledNarrowly = popupResizeableContentNarrowly.scrollTop;

                if (scrolledWide === 0) {
                    titleWide.style.boxShadow = "none";
                } else {
                    titleWide.style.boxShadow = "0 2px 5px 3px rgba(0,0,0,.1)";
                }

                if (scrolledNarrowly === 0) {
                    titleNarrowly.style.boxShadow = "none";
                } else {
                    titleNarrowly.style.boxShadow = "0 2px 5px 3px rgba(0,0,0,.1)";
                }
            }
        };
    })();
</script>
