<?
use Sotbit\Origami\Helper\Config;
\Bitrix\Main\Loader::includeModule('sotbit.origami');
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/**
 * @global array $arParams
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global string $cartId
 */
if (!$arResult["DISABLE_USE_BASKET"])
{
	?>
    <? if ($APPLICATION->GetCurPage(false) === '/'): ?>
	<a class="ml-2 mr-3 header-two__basket-buy active" target="_blank" href="https://www.instagram.com/leloo.kz/">
		<svg width="18" height="18" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
<g>
	<g>
		<path d="M352,0H160C71.648,0,0,71.648,0,160v192c0,88.352,71.648,160,160,160h192c88.352,0,160-71.648,160-160V160
			C512,71.648,440.352,0,352,0z M464,352c0,61.76-50.24,112-112,112H160c-61.76,0-112-50.24-112-112V160C48,98.24,98.24,48,160,48
			h192c61.76,0,112,50.24,112,112V352z"/>
	</g>
</g>
			<g>
				<g>
					<path d="M256,128c-70.688,0-128,57.312-128,128s57.312,128,128,128s128-57.312,128-128S326.688,128,256,128z M256,336
			c-44.096,0-80-35.904-80-80c0-44.128,35.904-80,80-80s80,35.872,80,80C336,300.096,300.096,336,256,336z"/>
				</g>
			</g>
			<g>
				<g>
					<circle cx="393.6" cy="118.4" r="17.056"/>
				</g>
			</g>
</svg>
	</a>
	<? endif; ?>

    <?if($arResult["SHOW_COMPARE"]):
	    ?>
		<a class="header-two__basket-compare <?=($arResult["NUM_PRODUCTS_COMPARE"] > 0)?'active':''?>" <?if($arResult["NUM_PRODUCTS_COMPARE"] != 0):?> href="<?=Config::get('COMPARE_PAGE')?>" <?endif;?>>
			<svg width="18" height="18">
				<use xlink:href="/local/templates/sotbit_origami/assets/img/sprite.svg#icon_compare"></use>
			</svg>
			<span><?=$arResult["NUM_PRODUCTS_COMPARE"]?></span>
		</a>
	<?endif;
    if($arResult["SHOW_DELAY"]):
	?>
	<a class="header-two__basket-favorites <?=($arResult["NUM_PRODUCTS_DELAY"] > 0)?'active':''?>" href="<?=$arParams['PATH_TO_BASKET'].'#favorit'?>" <?if($arResult["NUM_PRODUCTS_DELAY"]>0):?>onmouseenter="<?=$cartId?>.toggleOpenCloseCart('open')"<?endif?>>
		<svg width="18" height="18">
			<use xlink:href="/local/templates/sotbit_origami/assets/img/sprite.svg#icon_favourite"></use>
		</svg>
		<span><?=$arResult["NUM_PRODUCTS_DELAY"]?></span>
	</a>
    <?endif;?>
    <?php
    $notifications =array();
    global $USER;
    if(is_object($USER) && $USER->IsAuthorized())
    {
        $rsUser = CUser::GetList($by, $order,
            array(
                "ID" => $USER->GetID(),
            ),
            array(
                "SELECT" => array(
                    "UF_NOTIFY",
                ),
            )
        );
        if($arUser = $rsUser->Fetch())
        {
        	if ($arUser["UF_NOTIFY"] !==false)
            	$notifications=$arUser["UF_NOTIFY"];
        }
        $notifications = array_merge($notifications, getHighload('Notifycations'));

        // Выведем даты всех заказов текущего пользователя за текущий месяц, отсортированные по дате заказа
        $hasOrder=false;
        $arFilter=[
            "USER_ID"=>$USER->GetID(),
            "PAYED"=>"N",
            "CANCELED"=>"N"
        ];
        $db_sales=CSaleOrder::GetList(["DATE_INSERT"=>"ASC"],$arFilter);
        while ($ar_sales=$db_sales->Fetch()) {
            $hasOrder=true;
            break;
        }
    }else
        $notifications = getHighload('Notifycations');
    $firstTime = (!isset($_COOKIE["BITRIX_SM_LOGIN"]) && !$USER->IsAuthorized()) ? true:false;
    if ($hasOrder)
        array_splice($notifications, 0, 0, "alert&У вас есть неоплаченный заказ$ Завершите оплату в <i>личном кабинете</i>-Y");
    if ($firstTime)
        array_splice($notifications, 0, 0, "discount&Получи скидку 10% на первый заказ$ Похоже ты у нас впервые, <i>зарегистрируйся</i> и получи промокод на скидку. Нажми тут что бы перейти к регистрации-Y");
    $show = 'none';
//    foreach ($notifications as $n){
//        if (((getNotifyText($n,'show') || (isset($n["UF_SHOW"]) && $n["UF_SHOW"] == "1") || $firstTime) && !$_SESSION['showed'])){
//            $show = 'block';
//            $_SESSION['showed'] = true;
//            break;
//        }
//    }
    sort($notifications);
    ?>
	<div class="header-two__basket-buy <?=count($notifications)>0&&$notifications!==false?'active':''?> notif">
		<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
<g>
	<g>
		<path d="M467,61H45C20.218,61,0,81.196,0,106v300c0,24.72,20.128,45,45,45h422c24.72,0,45-20.128,45-45V106
			C512,81.28,491.872,61,467,61z M460.786,91L256.954,294.833L51.359,91H460.786z M30,399.788V112.069l144.479,143.24L30,399.788z
			 M51.213,421l144.57-144.57l50.657,50.222c5.864,5.814,15.327,5.795,21.167-0.046L317,277.213L460.787,421H51.213z M482,399.787
			L338.213,256L482,112.212V399.787z"/>
	</g>
</g>
</svg>
		<g><path d="m411 262.862v-47.862c0-69.822-46.411-129.001-110-148.33v-21.67c0-24.813-20.187-45-45-45s-45 20.187-45 45v21.67c-63.59 19.329-110 78.507-110 148.33v47.862c0 61.332-23.378 119.488-65.827 163.756-4.16 4.338-5.329 10.739-2.971 16.267s7.788 9.115 13.798 9.115h136.509c6.968 34.192 37.272 60 73.491 60 36.22 0 66.522-25.808 73.491-60h136.509c6.01 0 11.439-3.587 13.797-9.115s1.189-11.929-2.97-16.267c-42.449-44.268-65.827-102.425-65.827-163.756zm-170-217.862c0-8.271 6.729-15 15-15s15 6.729 15 15v15.728c-4.937-.476-9.94-.728-15-.728s-10.063.252-15 .728zm15 437c-19.555 0-36.228-12.541-42.42-30h84.84c-6.192 17.459-22.865 30-42.42 30zm-177.67-60c34.161-45.792 52.67-101.208 52.67-159.138v-47.862c0-68.925 56.075-125 125-125s125 56.075 125 125v47.862c0 57.93 18.509 113.346 52.671 159.138z"/><path d="m451 215c0 8.284 6.716 15 15 15s15-6.716 15-15c0-60.1-23.404-116.603-65.901-159.1-5.857-5.857-15.355-5.858-21.213 0s-5.858 15.355 0 21.213c36.831 36.831 57.114 85.8 57.114 137.887z"/><path d="m46 230c8.284 0 15-6.716 15-15 0-52.086 20.284-101.055 57.114-137.886 5.858-5.858 5.858-15.355 0-21.213-5.857-5.858-15.355-5.858-21.213 0-42.497 42.497-65.901 98.999-65.901 159.099 0 8.284 6.716 15 15 15z"/></g></svg>
		<span><?=$notifications!==false?count($notifications):0?></span>
		<div class="notifCentre" style="display: <?=$show?>">
			<div class="notifHead">
				<p>Уведомления</p>
				<div class="open-basket-origami__close">
					Закрыть
				</div>
			</div>
			<div class="notifBody">
                <? foreach ($notifications as $i=>$notify): ?>
					<?php
						if ($i==0 && $firstTime) $link = '/personal/?register=yes';
						elseif ($i==0 && $hasOrder) $link = '/personal/orders/';
						elseif (stripos($notify['UF_TITLE'], 'kaspi')!==false) $link = '/help/kaspi/';
					?>
					<a href="<?=$link?>" class="notifCard <?=$i>2?'collapsed':''?> <?=$i==4?'second':''?>">
						<div class="cardIcon">
                            <?php if (isset($notify["UF_ICON"])): ?>
								<img src="<?=CFile::GetPath($notify["UF_ICON"])?>" alt="">
                            <?php else: ?>
								<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/svg/notifyIcons/<?=getNotifyText($notify)?>.svg" alt="">
                            <?php endif; ?>
						</div>
						<div class="cardBody">
							<div class="cardTitle"><?=isset($notify["UF_TITLE"])? $notify["UF_TITLE"]:getNotifyText($notify,'title')?></div>
							<div class="cardText"><?=isset($notify["UF_TEXT"])? $notify["UF_TEXT"]:getNotifyText($notify,'text')?></div>
<!--							<div class="cardClose">-->
<!--								<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"-->
<!--									 viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">-->
<!--<ellipse style="fill:#E04F5F;" cx="256" cy="256" rx="256" ry="255.832"/>-->
<!--									<g transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 77.26 32)">-->
<!--										<rect x="3.98" y="-427.615" style="fill:#FFFFFF;" width="55.992" height="285.672"/>-->
<!--										<rect x="-110.828" y="-312.815" style="fill:#FFFFFF;" width="285.672" height="55.992"/>-->
<!--									</g>-->
<!---->
<!--</svg>-->
<!--							</div>-->
						</div>
					</a>
                <? endforeach;?>
			</div>
		</div>
	</div>
    <?if($arResult["SHOW_BASKET"]):?>
	<a class="header-two__basket-buy <?=($arResult['NUM_PRODUCTS'] > 0)?'active':''?>" href="<?=$arParams['PATH_TO_BASKET']?>" <?if($arResult["NUM_PRODUCTS"]>0):?>onmouseenter="<?=$cartId?>.toggleOpenCloseCart('open')"<?endif;?>>
		<svg width="18" height="18">
			<use xlink:href="/local/templates/sotbit_origami/assets/img/sprite.svg#icon_cart"></use>
		</svg>
        <?
        if (!$compositeStub)
        {
            if ($arParams['SHOW_NUM_PRODUCTS'] == 'Y' && ($arResult['NUM_PRODUCTS'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y'))
            {
                ?>
				<span><?=$arResult['NUM_PRODUCTS']?></span>
                <?
            }
        }
        ?>
	</a>
    <?endif;?>
	<?
}
?>


