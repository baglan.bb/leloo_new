<?php

use Sotbit\Origami\Helper\Config;
use \Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
\Bitrix\Main\Loader::includeModule('currency');
CJSCore::Init(array('currency'));
$this->setFrameMode(true);
?>

<div class="promotion_detail">
    <?
    $ElementID = $APPLICATION->IncludeComponent(
        "bitrix:news.detail",
        "",
        [
            "DISPLAY_DATE"              => $arParams["DISPLAY_DATE"],
            "DISPLAY_NAME"              => $arParams["DISPLAY_NAME"],
            "DISPLAY_PICTURE"           => $arParams["DISPLAY_PICTURE"],
            "DISPLAY_PREVIEW_TEXT"      => $arParams["DISPLAY_PREVIEW_TEXT"],
            "IBLOCK_TYPE"               => $arParams["IBLOCK_TYPE"],
            "IBLOCK_ID"                 => $arParams["IBLOCK_ID"],
            "FIELD_CODE"                => $arParams["DETAIL_FIELD_CODE"],
            "PROPERTY_CODE"             => $arParams["DETAIL_PROPERTY_CODE"],
            "DETAIL_URL"                => $arResult["FOLDER"]
                .$arResult["URL_TEMPLATES"]["detail"],
            "SECTION_URL"               => $arResult["FOLDER"]
                .$arResult["URL_TEMPLATES"]["section"],
            "META_KEYWORDS"             => $arParams["META_KEYWORDS"],
            "META_DESCRIPTION"          => $arParams["META_DESCRIPTION"],
            "BROWSER_TITLE"             => $arParams["BROWSER_TITLE"],
            "SET_CANONICAL_URL"         => $arParams["DETAIL_SET_CANONICAL_URL"],
            "DISPLAY_PANEL"             => $arParams["DISPLAY_PANEL"],
            "SET_LAST_MODIFIED"         => $arParams["SET_LAST_MODIFIED"],
            "SET_TITLE"                 => $arParams["SET_TITLE"],
            "MESSAGE_404"               => $arParams["MESSAGE_404"],
            "SET_STATUS_404"            => $arParams["SET_STATUS_404"],
            "SHOW_404"                  => $arParams["SHOW_404"],
            "FILE_404"                  => $arParams["FILE_404"],
            "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
            "ADD_SECTIONS_CHAIN"        => $arParams["ADD_SECTIONS_CHAIN"],
            "ACTIVE_DATE_FORMAT"        => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
            "CACHE_TYPE"                => $arParams["CACHE_TYPE"],
            "CACHE_TIME"                => $arParams["CACHE_TIME"],
            "CACHE_GROUPS"              => $arParams["CACHE_GROUPS"],
            "USE_PERMISSIONS"           => $arParams["USE_PERMISSIONS"],
            "GROUP_PERMISSIONS"         => $arParams["GROUP_PERMISSIONS"],
            "DISPLAY_TOP_PAGER"         => $arParams["DETAIL_DISPLAY_TOP_PAGER"],
            "DISPLAY_BOTTOM_PAGER"      => $arParams["DETAIL_DISPLAY_BOTTOM_PAGER"],
            "PAGER_TITLE"               => $arParams["DETAIL_PAGER_TITLE"],
            "PAGER_SHOW_ALWAYS"         => "N",
            "PAGER_TEMPLATE"            => $arParams["DETAIL_PAGER_TEMPLATE"],
            "PAGER_SHOW_ALL"            => $arParams["DETAIL_PAGER_SHOW_ALL"],
            "CHECK_DATES"               => $arParams["CHECK_DATES"],
            "ELEMENT_ID"                => $arResult["VARIABLES"]["ELEMENT_ID"],
            "ELEMENT_CODE"              => $arResult["VARIABLES"]["ELEMENT_CODE"],
            "SECTION_ID"                => $arResult["VARIABLES"]["SECTION_ID"],
            "SECTION_CODE"              => $arResult["VARIABLES"]["SECTION_CODE"],
            "IBLOCK_URL"                => $arResult["FOLDER"]
                .$arResult["URL_TEMPLATES"]["news"],
            "USE_SHARE"                 => $arParams["USE_SHARE"],
            "SHARE_HIDE"                => $arParams["SHARE_HIDE"],
            "SHARE_TEMPLATE"            => $arParams["SHARE_TEMPLATE"],
            "SHARE_HANDLERS"            => $arParams["SHARE_HANDLERS"],
            "SHARE_SHORTEN_URL_LOGIN"   => $arParams["SHARE_SHORTEN_URL_LOGIN"],
            "SHARE_SHORTEN_URL_KEY"     => $arParams["SHARE_SHORTEN_URL_KEY"],
            "ADD_ELEMENT_CHAIN"         => (isset($arParams["ADD_ELEMENT_CHAIN"])
                ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
            'STRICT_SECTION_CHECK'      => (isset($arParams['STRICT_SECTION_CHECK'])
                ? $arParams['STRICT_SECTION_CHECK'] : ''),
        ],
        $component
    );
    global $detailPromotion;
    if($detailPromotion) {
        $intSectionID = $APPLICATION->IncludeComponent(
            "bitrix:catalog.section",
            "origami_section",
            [
                "IBLOCK_ID"                 => Config::get("IBLOCK_ID"),
                "IBLOCK_TYPE"               => Config::get("IBLOCK_TYPE"),
                "ELEMENT_SORT_FIELD"        => "rand",
                "ELEMENT_SORT_ORDER"        => "id",
                "ELEMENT_SORT_FIELD2"       => "desc",
                "ELEMENT_SORT_ORDER2"       => "desc",
                "PROPERTY_CODE"             => [
                    17 => "CML2_BAR_CODE",
                    18 => "CML2_ARTICLE",
                    19 => "CML2_ATTRIBUTES",
                    20 => "CML2_TRAITS",
                    21 => "CML2_BASE_UNIT",
                    22 => "CML2_TAXES",
                    23 => "MORE_PHOTO",
                    24 => "FILES",
                    25 => "CML2_MANUFACTURER",
                    30 => "BLOG_POST_ID",
                    31 => "BLOG_COMMENTS_CNT",
                    155 => "KHIT",
                ],
                "PROPERTY_CODE_MOBILE"      => [],
                "META_KEYWORDS"             => "-",
                "META_DESCRIPTION"          => "-",
                "BROWSER_TITLE"             => "-",
                "SET_LAST_MODIFIED"         => "N",
                "INCLUDE_SUBSECTIONS"       => "Y",
                "BASKET_URL"                => Config::get('BASKET_PAGE'),
                "ACTION_VARIABLE"           => "action",
                "PRODUCT_ID_VARIABLE"       => "id",
                "SECTION_ID_VARIABLE"       => "SECTION_ID",
                "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                "PRODUCT_PROPS_VARIABLE"    => "prop",
                "FILTER_NAME"               => "detailPromotion",
                "CACHE_FILTER"              => "Y",
                "CACHE_GROUPS"              => "Y",
                "CACHE_TIME"                => "36000000",
                "CACHE_TYPE"                => "A",
                "SET_TITLE"                 => "N",
                "MESSAGE_404"               => "",
                "SET_STATUS_404"            => "Y",
                'SHOW_ALL_WO_SECTION'       => 'Y',
                "SHOW_404"                  => "N",
                "DISPLAY_COMPARE"           => "Y",
                "PAGE_ELEMENT_COUNT"        => 100,
                "LINE_ELEMENT_COUNT"        => 5,
                "PRICE_CODE"                => \SotbitOrigami::GetComponentPrices(["BASE","OPT","SMALL_OPT"]),
                "USE_PRICE_COUNT"           => "N",
                "SHOW_PRICE_COUNT"          => "1",
                
                "PRICE_VAT_INCLUDE"          => "Y",
                "USE_PRODUCT_QUANTITY"       => "Y",
                "ADD_PROPERTIES_TO_BASKET"   => "Y",
                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                "PRODUCT_PROPERTIES"         => [
                ],
                
                "DISPLAY_TOP_PAGER"               => "N",
                "DISPLAY_BOTTOM_PAGER"            => "N",
                "PAGER_TITLE"                     => Loc::getMessage('PAGER_TITLE'),
                "PAGER_SHOW_ALWAYS"               => "",
                "PAGER_TEMPLATE"                  => "",
                "PAGER_DESC_NUMBERING"            => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
                "PAGER_SHOW_ALL"                  => "N",
                "PAGER_BASE_LINK_ENABLE"          => "N",
                "PAGER_BASE_LINK"                 => "N",
                "PAGER_PARAMS_NAME"               => "N",
                "LAZY_LOAD"                       => "N",
                "MESS_BTN_LAZY_LOAD"              => "N",
                "LOAD_ON_SCROLL"                  => "N",

                "OFFERS_CART_PROPERTIES" => [
                    0 => "SIZES_SHOES",
                    1 => "SIZES_CLOTHES",
                    2 => "COLOR_REF",
                ],
                "OFFERS_FIELD_CODE"      => [
                    0 => "NAME",
                    1 => "PREVIEW_PICTURE",
                    2 => "DETAIL_PICTURE",
                    3 => "DETAIL_PAGE_URL",
                ],
                "OFFERS_PROPERTY_CODE"   => [
                    1  => "CML2_BAR_CODE",
                    2  => "CML2_ARTICLE",
                    5  => "CML2_BASE_UNIT",
                    7  => "MORE_PHOTO",
                    8  => "FILES",
                ],
                "OFFERS_SORT_FIELD"      => "sort",
                "OFFERS_SORT_ORDER"      => "id",
                "OFFERS_SORT_FIELD2"     => "desc",
                "OFFERS_SORT_ORDER2"     => "desc",
                "OFFERS_LIMIT"           => 0,

                "SECTION_URL"               => SITE_DIR
                    .'catalog/#SECTION_CODE_PATH#/',
                "DETAIL_URL"                => SITE_DIR
                    .'catalog/#SECTION_CODE_PATH#/#ELEMENT_CODE#/',
                "USE_MAIN_ELEMENT_SECTION"  => "N",
                'CONVERT_CURRENCY'          => "N",
                'HIDE_NOT_AVAILABLE'        => "N",
                'HIDE_NOT_AVAILABLE_OFFERS' => "N",
                
                'LABEL_PROP'                  => [
                    0 => "RASPRODAZHA",
                ],
                'LABEL_PROP_MOBILE'           => [],
                'ADD_PICT_PROP'               => "MORE_PHOTO",
                'PRODUCT_DISPLAY_MODE'        => "Y",
                'OFFER_ADD_PICT_PROP'         => "MORE_PHOTO",
                'OFFER_TREE_PROPS'            => [
                    "PROTSESSOR",
                    "OBEM_OPERATICHNOY_PAMYATI",
                    "OBEM_PAMYATI",
                    "RAZMER",
                    "CHASTOTA_PROTSESSORA",
                    "TIP_VIDEOKARTY",
                    "TSVET",
                    "KOLICHESTVO_YADER_PROTSESORA",
                    "OBEM_VIDEOPAMYATI",
                    "TSVET_1",
                    "USTANOVLENNAYA_OS",
                    "CML2_MANUFACTURER",
                ],
                'PRODUCT_SUBSCRIPTION'        => "Y",
                'SHOW_DISCOUNT_PERCENT'       => "Y",
                'SHOW_OLD_PRICE'              => "Y",
                'SHOW_MAX_QUANTITY'           => "M",
                "MESS_BTN_ADD_TO_BASKET"      => Loc::getMessage('MESS_BTN_ADD_TO_BASKET'),
                "MESS_BTN_BUY"                => Loc::getMessage('MESS_BTN_BUY'),
                "MESS_BTN_COMPARE"            => Loc::getMessage('MESS_BTN_COMPARE'),
                "MESS_BTN_DETAIL"             => Loc::getMessage('MESS_BTN_DETAIL'),
                "MESS_BTN_SUBSCRIBE"          => Loc::getMessage('MESS_BTN_SUBSCRIBE'),
                "MESS_NOT_AVAILABLE"          => 'Нет в наличии',
                "MESS_RELATIVE_QUANTITY_MANY" => 'В наличии',
                "MESS_RELATIVE_QUANTITY_FEW"  => 'В наличии',
                "MESS_RELATIVE_QUANTITY_NO"   => "Нет в наличии",
                'USE_VOTE_RATING'             => "Y",
                'TEMPLATE_THEME'              => "",
                "ADD_SECTIONS_CHAIN"          => "N",
                'ADD_TO_BASKET_ACTION'        => "ADD",
                'COMPARE_PATH'                => Config::get('COMPARE_PAGE'),
                'COMPARE_NAME'                => "CATALOG_COMPARE_LIST",
                'USE_COMPARE_LIST'            => 'Y',
                'ACTION_PRODUCTS' => array("ADMIN"),
                "ACTION_PRODUCTS" => array(
                    0 => "ADMIN",
                ),
                "VARIANT_LIST_VIEW" => "ADMIN",
                "SHOW_SLIDER" => "N",
                'SECTION_NAME' => Loc::getMessage('SECT_PROMO_BLOCK_NAME')
            ],
            false
        );
    }
    ?>
    <div class="puzzle_block puzzle_block-form-promotion-wrapper">
        <?
        $APPLICATION->IncludeComponent(
            "bitrix:form.result.new",
            "origami_webform_2",
            Array(
                "CACHE_TIME" => "3600",
                "CACHE_TYPE" => "A",
                "CHAIN_ITEM_LINK" => "",
                "CHAIN_ITEM_TEXT" => "",
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO",
                "EDIT_URL" => "",
                "IGNORE_CUSTOM_TEMPLATE" => "N",
                "LIST_URL" => "",
                "SEF_MODE" => "N",
                "SUCCESS_URL" => "",
                "USE_EXTENDED_ERRORS" => "N",
                "VARIABLE_ALIASES" => Array(
                    "RESULT_ID" => "RESULT_ID",
                    "WEB_FORM_ID" => "WEB_FORM_ID"
                ),
                "WEB_FORM_ID" => 2
            )
        );
        ?>
    </div>
    <div class="puzzle_block puzzle_block_promotion_return">
        <a class="return" href="<?=$arResult['FOLDER']?>">
			<span class="return_inner">
				<?=GetMessage('RETURN')?>
			</span>
        </a>
    </div>
</div>