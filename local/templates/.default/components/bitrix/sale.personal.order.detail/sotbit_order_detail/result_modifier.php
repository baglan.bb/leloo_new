<?
use Bitrix\Sale;

Bitrix\Main\Loader::includeModule("sale");

$order = Sale\Order::load($arResult["ID"]);
$propertyCollection = $order->getPropertyCollection();
$arProps = $propertyCollection->getArray();
foreach($arProps['properties'] as $arProp) {
    if((intval($arProp["ID"]) == 20 || intval($arProp["ID"]) == 21) && strlen(implode("", $arProp["VALUE"])) > 0) {
        $arProp["VALUE"] = implode(", ", $arProp["VALUE"]);
        $arResult["ORDER_PROPS"][] = $arProp;
    }
}
