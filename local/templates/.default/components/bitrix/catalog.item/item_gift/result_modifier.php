<?

use Sotbit\Origami\Helper\Config;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
try {
    \Bitrix\Main\Loader::includeModule('sotbit.origami');
} catch (\Bitrix\Main\LoaderException $e) {
    print_r($e->getMessage());
}

if ($arResult['ITEM']['PROPERTIES']['MORE_PHOTO']['VALUE']) {
    $arResult['ITEM']['MORE_PHOTO'] = [];
    $rs = CFile::GetList(
        [],
        [
            '@ID' => implode(',',
                $arResult['ITEM']['PROPERTIES']['MORE_PHOTO']['VALUE']),
        ]
    );
    while ($file = $rs->Fetch()) {
        $file['SRC'] = CFile::GetFileSRC($file);
        $arResult['ITEM']['MORE_PHOTO'][] = $file;
    }
}

$Item = new \Sotbit\Origami\Image\Item();
$arResult['ITEM'] = $Item->prepareImages($arResult['ITEM']);

$arResult['ITEM'] = \SotbitOrigami::changeColorImages($arResult['ITEM']);
$color = \Sotbit\Origami\Helper\Color::getInstance(SITE_ID);
$arParams = $color::changePropColorView($arResult, $arParams)['PARAMS'];
?>