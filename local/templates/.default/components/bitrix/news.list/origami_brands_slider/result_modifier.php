<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule('dev2fun.imagecompress');

foreach($arResult["ITEMS"] as &$arItem)
{
    if(is_array($arItem["PREVIEW_PICTURE"]))
    {
        $img = \CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], Array("width" => 200, "height" => 100),BX_RESIZE_IMAGE_PROPORTIONAL, true);
        $fullSrc = \Bitrix\Main\Application::getDocumentRoot() . $img['src'];

        $mime = mime_content_type($fullSrc);
        switch ($mime){
            case "image/jpeg":
                Dev2fun\ImageCompress\Compress::getInstance()->compressJPG($fullSrc);
                break;
            case "image/png":
                Dev2fun\ImageCompress\Compress::getInstance()->compressPNG($fullSrc);
                break;
        }

        $arItem["PREVIEW_PICTURE"]["MINI"] = Array(
            "SRC" => $img['src'],
            "WIDTH" => $img['width'],
            "HEIGHT" => $img['height'],
        );
    }
}