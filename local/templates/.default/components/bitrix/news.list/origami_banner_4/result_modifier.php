<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
{
    die();
}
CModule::IncludeModule('dev2fun.imagecompress');

$arResult['MAIN'] = [];

foreach($arResult['ITEMS'] as &$item)
{
    if(is_array($item["PREVIEW_PICTURE"]))
    {
        $img = \CFile::ResizeImageGet($item["PREVIEW_PICTURE"], Array("width" => 500, "height" => 500),BX_RESIZE_IMAGE_PROPORTIONAL, true);
        $fullSrc = \Bitrix\Main\Application::getDocumentRoot() . $img['src'];

        $mime = mime_content_type($fullSrc);
        switch ($mime){
            case "image/jpeg":
                Dev2fun\ImageCompress\Compress::getInstance()->compressJPG($fullSrc);
                break;
        }

        $item["PREVIEW_PICTURE"]["MINI"] = Array(
            "SRC" => $img['src'],
            "WIDTH" => $img['width'],
            "HEIGHT" => $img['height'],
        );
    }

    if(is_array($item["DETAIL_PICTURE"]))
    {
        $img = \CFile::ResizeImageGet($item["DETAIL_PICTURE"], Array("width" => 500, "height" => 500),BX_RESIZE_IMAGE_PROPORTIONAL, true);
        $fullSrc = \Bitrix\Main\Application::getDocumentRoot() . $img['src'];

        $mime = mime_content_type($fullSrc);
        switch ($mime){
            case "image/jpeg":
                Dev2fun\ImageCompress\Compress::getInstance()->compressJPG($fullSrc);
                break;
        }

        $item["DETAIL_PICTURE"]["MINI"] = Array(
            "SRC" => $img['src'],
            "WIDTH" => $img['width'],
            "HEIGHT" => $img['height'],
        );
    }

    if(in_array('MAIN',$item['PROPERTIES']['BANNER_TYPE']['VALUE_XML_ID']))
    {
        $arResult['MAIN'][] = $item;
    }

    if(in_array('SIDE',$item['PROPERTIES']['BANNER_TYPE']['VALUE_XML_ID']))
    {
        $arResult['MAIN'][] = $item;
    }
}

unset($arResult['ITEMS']);

