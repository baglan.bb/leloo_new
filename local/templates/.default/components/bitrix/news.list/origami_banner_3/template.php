<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->createFrame()->begin();
use Sotbit\Origami\Helper\Config;
//$hoverClass = implode(" ", Config::getArray("HOVER_EFFECT"));
$lazyLoad = (Config::get('LAZY_LOAD') == "Y");
?>
<div class="puzzle_block about__puzzle_block main-container">
    <div class="about_block">
        <div class="main_page-catalog_banner">
            <div class="slider_block">
                <div class="block_main_canvas__big_canvas slider-canvas owl-carousel">
                    <?if ($arResult['MAIN']):?>
                    <?foreach ($arResult['MAIN'] as $item):
                        $img = $item['DESKTOP_BANNER'];
                        $mobileImage = $item["MOBILE_BANNER"];
                    ?><div class="block_main_canvas__big_canvas__content<?=$hoverClass?><?= $mobileImage ? " responsive" : "" ?>"><?
                        if(strlen($item['PROPERTIES']['BUTTON_TEXT']['VALUE']) <= 0 && $item['PROPERTIES']['URL']['VALUE']){
                            ?><a href="<?=$item['PROPERTIES']['URL']['VALUE'] ?>" <?=($item['PROPERTIES']['NEW_TAB']['VALUE'] == 'Y') ? 'target="_blank"' : '' ?>><?
                        }?>
						<picture>
							<source class="owl-lazy" data-srcset="<?=webp($item['MOBILE_BANNER_S']['src'])?>" type="image/webp" media="(max-width: 360px)">
							<source class="owl-lazy" data-srcset="<?=$item['MOBILE_BANNER_S']['src']?>" media="(max-width: 360px)">
							<?php if ($item["MOBILE_BANNER_WEBP"]): ?>
								<source class="owl-lazy" data-srcset="<?=$item["MOBILE_BANNER_WEBP"]?>" type="image/webp" media="(max-width: 768px)">
							<?php else: ?>
								<source class="owl-lazy" data-srcset="<?=webp($item['MOBILE_BANNER']['SRC'])?>" type="image/webp" media="(max-width: 768px)">
							<?php endif; ?>
							<source class="owl-lazy" data-srcset="<?=$item['MOBILE_BANNER']['SRC']?>" media="(max-width: 768px)">
							<source class="owl-lazy" data-srcset="<?=webp($item['DESKTOP_BANNER']['SRC'])?>" type="image/webp">
							<img class="owl-lazy" data-src="<?=$item['DESKTOP_BANNER']['SRC']?>" title="<?=$img['TITLE']?>" alt="<?=$img['ALT']?>">
						</picture><?
                        if($lazyLoad):
                            ?><span class="lazyOnMain" style="background-image: url('<?=$item['MOBILE_BANNER_XXS']['src']?>')"></span><?
                        endif;
                        ?><div class="block_main_canvas__big_canvas__info">
                            <?if($item['~PREVIEW_TEXT']):?>
                            <div class="block_main_canvas__big_canvas__title fonts__middle_title">
                                <?=$item['~PREVIEW_TEXT']?>
                            </div>
                            <?endif;?>
                            <?if($item['~DETAIL_TEXT']):?>
                            <div class="block_main_canvas__big_canvas__comment fonts__small_text">
                                <?=$item['~DETAIL_TEXT']?>
                            </div>
                            <?endif;?>
                            <?if($item['PROPERTIES']['BUTTON_TEXT']['VALUE'] && $item['PROPERTIES']['URL']['VALUE']):?>
                                <div class="slider_button-wrapper">
                                    <a href="<?=$item['PROPERTIES']['URL']['VALUE'] ?>" <?=($item['PROPERTIES']['NEW_TAB']['VALUE'] == 'Y') ? 'target="_blank"' : '' ?> class="main_btn button_another sweep-to-right"
                                       title="<?=$item['~PREVIEW_TEXT']?>">
                                        <?=$item['PROPERTIES']['BUTTON_TEXT']['VALUE']?>
                                    </a>
                                </div>
                            <?endif;?>
                        </div>
                        <?if(strlen($item['PROPERTIES']['BUTTON_TEXT']['VALUE']) <= 0 && $item['PROPERTIES']['URL']['VALUE']){ ?>
                            </a>
                        <? } ?>
                    </div>
                    <?endforeach;
                    endif;?>
                </div>
            </div>
            <?
            if ($arResult['SIDE']):?>
            <div class="catalog_links-block">
                <div class="links-section_wrapper">
                    <div class="links-section_left">
                        <?if(isset($arResult['SIDE'][0])):
                        $item = $arResult['SIDE'][0];
                        $img = $item['PREVIEW_PICTURE']['SRC'] ? $item['PREVIEW_PICTURE'] : $item['DETAIL_PICTURE'];
                        $name = $item['~PREVIEW_TEXT'] ? $item['~PREVIEW_TEXT'] : $item['NAME'];
                        if($lazyLoad)
                        {
                            $strLazyLoad = 'src="'.SITE_TEMPLATE_PATH.'/assets/img/loader_lazy.svg" data-src="'.$img['SRC'].'" class="lazy"';
                        }else{
                            $strLazyLoad = 'src="'.$img['SRC'].'"';
                        }

                        ?>
                        <a class="links-section__link <?=$hoverClass?>" href="<?=$item['PROPERTIES']['URL']['VALUE']?>" title="<?=$name?>">
                            <img <?=$strLazyLoad?>
                                    width="<?=$img['WIDTH']?>"
                                    height="<?=$img['HEIGHT']?>"
                                    title="<?=$img['TITLE']?>"
                                    alt="<?=$img['ALT']?>"
                            >
                            <?if($lazyLoad):?>
                                <span class="loader-lazy"></span>
                            <?endif;?>
                            <div class="link-section_text"><?=$name?></div>
                        </a>
                        <?endif;?>
                        <?if(isset($arResult['SIDE'][1])):
                        $item = $arResult['SIDE'][1];
                        $img = $item['PREVIEW_PICTURE']['SRC'] ? $item['PREVIEW_PICTURE'] : $item['DETAIL_PICTURE'];
                        $name = $item['~PREVIEW_TEXT'] ? $item['~PREVIEW_TEXT'] : $item['NAME'];
                        if($lazyLoad)
                        {
                            $strLazyLoad = 'src="'.SITE_TEMPLATE_PATH.'/assets/img/loader_lazy.svg" data-src="'.$img['SRC'].'" class="lazy"';
                        }else{
                            $strLazyLoad = 'src="'.$img['SRC'].'"';
                        }
                        ?>
                            <a class="links-section__link <?=$hoverClass?>" href="<?=$item['PROPERTIES']['URL']['VALUE']?>" title="<?=$name?>">
                                <img <?=$strLazyLoad?>
                                        width="<?=$img['WIDTH']?>"
                                        height="<?=$img['HEIGHT']?>"
                                        title="<?=$img['TITLE']?>"
                                        alt="<?=$img['ALT']?>"
                                >
                                <?if($lazyLoad):?>
                                    <span class="loader-lazy"></span>
                                <?endif;?>
                                <div class="link-section_text"><?=$name?></div>
                            </a>
                        <?endif;?>
                    </div>
                </div>
                <div class="links-section_wrapper">
                    <div class="links-section_right">
                        <?if(isset($arResult['SIDE'][2])):
                        $item = $arResult['SIDE'][2];
                        $img = $item['PREVIEW_PICTURE']['SRC'] ? $item['PREVIEW_PICTURE'] : $item['DETAIL_PICTURE'];
                        $name = $item['~PREVIEW_TEXT'] ? $item['~PREVIEW_TEXT'] : $item['NAME'];

                        if($lazyLoad)
                        {
                            $strLazyLoad = 'src="'.SITE_TEMPLATE_PATH.'/assets/img/loader_lazy.svg" data-src="'.$img['SRC'].'" class="lazy"';
                        }else{
                            $strLazyLoad = 'src="'.$img['SRC'].'"';
                        }
                        ?>
                            <a class="links-section__link <?=$hoverClass?>" href="<?=$item['PROPERTIES']['URL']['VALUE']?>" title="<?=$name?>">
                                <img <?=$strLazyLoad?>
                                        width="<?=$img['WIDTH']?>"
                                        height="<?=$img['HEIGHT']?>"
                                        title="<?=$img['TITLE']?>"
                                        alt="<?=$img['ALT']?>"
                                >
                                <?if($lazyLoad):?>
                                    <span class="loader-lazy"></span>
                                <?endif;?>
                                <div class="link-section_text"><?=$name?></div>
                            </a>
                        <?endif;?>
                        <?if(isset($arResult['SIDE'][3])):
                        $item = $arResult['SIDE'][3];
                        $img = $item['PREVIEW_PICTURE']['SRC'] ? $item['PREVIEW_PICTURE'] : $item['DETAIL_PICTURE'];
                        $name = $item['~PREVIEW_TEXT'] ? $item['~PREVIEW_TEXT'] : $item['NAME'];

                        if($lazyLoad)
                        {
                            $strLazyLoad = 'src="'.SITE_TEMPLATE_PATH.'/assets/img/loader_lazy.svg" data-src="'.$img['SRC'].'" class="lazy"';
                        }else{
                            $strLazyLoad = 'src="'.$img['SRC'].'"';
                        }
                        ?>
                            <a class="links-section__link <?=$hoverClass?>" href="<?=$item['PROPERTIES']['URL']['VALUE']?>" title="<?=$name?>">
                                <img <?=$strLazyLoad?>
                                        width="<?=$img['WIDTH']?>"
                                        height="<?=$img['HEIGHT']?>"
                                        title="<?=$img['TITLE']?>"
                                        alt="<?=$img['ALT']?>"
                                >
                                <?if($lazyLoad):?>
                                    <span class="loader-lazy"></span>
                                <?endif;?>
                                <div class="link-section_text"><?=$name?></div>
                            </a>
                        <?endif;?>
                    </div>
                </div>
            </div>
            <?endif;?>
        </div>
    </div>
</div>

<script>
    window.sliderInitWindow();
    document.addEventListener('DOMContentLoaded', function () {
        let container = document.querySelectorAll('.main_page-catalog_banner');
        let items = document.querySelectorAll('.main_page-catalog_banner .links-section__link');
        let itemsList = Array.prototype.slice.call(items);
        function setHeight (item) {
            let elemWidth = Math.round(item.offsetWidth);
            item.style.height = elemWidth + 'px';
        }
        itemsList.forEach(function (elem) {
            setHeight(elem);
        });

        window.addEventListener('resize', function () {
            itemsList.forEach(function (elem) {
                setHeight(elem);
            });
        });
    });


</script>
