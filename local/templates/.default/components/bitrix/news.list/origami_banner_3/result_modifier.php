<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
{
    die();
}
$arResult['SIDE'] = [];
$arResult['MAIN'] = [];

foreach($arResult['ITEMS'] as $item)
{
    $item["DESKTOP_BANNER"] = $item['PREVIEW_PICTURE']['SRC'] ? $item['PREVIEW_PICTURE'] : $item['DETAIL_PICTURE'];

    if(intval($item["PROPERTIES"]["MOBILE_BANNER"]["VALUE"]) > 0)
    {
        $item["MOBILE_BANNER"] = CFile::GetFileArray($item["PROPERTIES"]["MOBILE_BANNER"]["VALUE"]);
        $item["MOBILE_BANNER_S"] = CFile::ResizeImageGet($item["MOBILE_BANNER"]["ID"], array('width'=>360, 'height'=>360), BX_RESIZE_IMAGE_EXACT, true);
        $item["MOBILE_BANNER_XXS"] = CFile::ResizeImageGet($item["MOBILE_BANNER"]["ID"], array('width'=>40, 'height'=>40), BX_RESIZE_IMAGE_EXACT, true);
    }

    if(intval($item["PROPERTIES"]["MOBILE_BANNER_WEBP"]["VALUE"]) > 0)
    {
        $item["MOBILE_BANNER_WEBP"] = CFile::GetPath($item["PROPERTIES"]["MOBILE_BANNER_WEBP"]["VALUE"]);
    }


    if(in_array('SIDE',$item['PROPERTIES']['BANNER_TYPE']['VALUE_XML_ID']))
    {
        $arResult['SIDE'][] = $item;
    }

    if(in_array('MAIN',$item['PROPERTIES']['BANNER_TYPE']['VALUE_XML_ID']))
    {
        $arResult['MAIN'][] = $item;
    }
}

unset($arResult['ITEMS']);

