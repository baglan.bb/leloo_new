<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $sotbitSeoMetaBottomDesc;
global $sotbitSeoMetaTopDesc;
global $sotbitSeoMetaAddDesc;
global $sotbitSeoMetaFile;
global $issetCondition;
global ${$arParams["FILTER_NAME"]};

$this->setFrameMode(true);
use Sotbit\Origami\Helper\Config;
$hoverClass = implode(" ", Config::getArray("HOVER_EFFECT"));
$lazyLoad = (Config::get('LAZY_LOAD') == "Y");


if(isset($sotbitSeoMetaFile))
{
    ?><div class="catalog_content__canvas"><?=
        $sotbitSeoMetaFile
    ?></div><?
} elseif($arResult["SECTION"]["DETAIL_PICTURE"]) {
    ?><div class="catalog_content__canvas"><?
        ?><img class="catalog_content__canvas_img" src="<?=$arResult["SECTION"]["DETAIL_PICTURE"]['SRC']?>" width="<?=$arResult["SECTION"]["DETAIL_PICTURE"]['WIDTH']?>" height="<?=$arResult["SECTION"]["DETAIL_PICTURE"]['HEIGHT']?>" alt="<?=$arResult["SECTION"]["DETAIL_PICTURE"]['ALT']?>" title="<?=$arResult["SECTION"]["DETAIL_PICTURE"]['TITLE']?>"><?
    ?></div><?
}

if (count($arResult['SECTIONS']) > 0):
?><div class="catalog_content__category_block"><?
	?><div class="catalog_content__category"><?
        foreach ($arResult['SECTIONS'] as $section)
        {
            $this->AddEditAction($section['ID'], $section['EDIT_LINK'], $strSectionEdit);
            $this->AddDeleteAction($section['ID'], $section['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
            ?><a href="<?=$section['SECTION_PAGE_URL'] ?>" title="<?=$section['NAME']?>" class="catalog_content__category_item <?=$hoverClass?>"><?
				?><p class="catalog_content__category_img_title fonts__middle_text"><?=$section['NAME']?></p><?
			?></a><?
        }
    ?></div><?
?></div><?
endif;

//tags
$curPageTags = array();
foreach (getHighload('Tags') as $key=>$tag){
	$string = substr($tag["UF_FILTER"], strpos($tag["UF_FILTER"], 'catalog'));
	$fp = strpos($string, '/filter');

	$url = substr_replace($string, '', $fp);
	$fUrl = substr($string, $fp);
	$depthLevel = substr_count($url, '/');

	if (strpos($_SERVER['REQUEST_URI'], $url) > 0)
		if ($arResult['SECTION']['DEPTH_LEVEL'] == $depthLevel){
            $curPageTags[$key]['NAME'] = $tag["UF_NAME"];
            $curPageTags[$key]['URL'] = '/'.$url.$fUrl;
		}
}
?>
<?php if (count($curPageTags)>0): ?>
	<div class="tags">
		<b>Популярные тэги:</b>
		<? foreach ($curPageTags as $t): ?>
		<a href="<?=$t['URL']?>">#<?=$t['NAME']?></a>
		<? endforeach;?>
	</div>
<?php endif; ?>

