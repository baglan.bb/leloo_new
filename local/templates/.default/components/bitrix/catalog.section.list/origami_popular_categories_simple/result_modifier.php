<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$maxCategories = $arParams["COUNT_SECTIONS"];

CModule::IncludeModule('dev2fun.imagecompress');

$arResult["SECTIONS"] = array_slice($arResult["SECTIONS"], 0, $maxCategories);

foreach($arResult["SECTIONS"] as &$arSection)
{
    if(is_array($arSection["PICTURE"]))
    {
        $img = \CFile::ResizeImageGet($arSection["PICTURE"], Array("width" => 400, "height" => 400),BX_RESIZE_IMAGE_PROPORTIONAL, true);
        $fullSrc = \Bitrix\Main\Application::getDocumentRoot() . $img['src'];

        $mime = mime_content_type($fullSrc);
        switch ($mime){
            case "image/jpeg":
                Dev2fun\ImageCompress\Compress::getInstance()->compressJPG($fullSrc);
                break;
        }

        $arSection["PICTURE"]["MINI"] = Array(
            "SRC" => $img['src'],
            "WEBP" => webp($img['src']),
            "WIDTH" => $img['width'],
            "HEIGHT" => $img['height'],
        );
    }
}