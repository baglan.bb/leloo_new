// "use strict";
(function () {
    document.addEventListener("DOMContentLoaded", function () {

        var widthHeaderNav = document.querySelector('.header-two__main-nav').offsetWidth;
        var menuWrapper = document.querySelector('.header-two__menu-catalog.menu-catalog-one');
        var menuBtn = document.querySelector('.header-two__main-nav-catalog.header-two__main-nav-catalog--one');
        var widthBorder = 1;
        var widthBtn = document.querySelector('.header-two__main-nav-catalog > a').offsetWidth + widthBorder * 2;
        var menu = document.querySelector('.main-menu-wrapper');
        var mainMenu = menu.querySelector('.main-menu-wrapper__submenu-main');
        var containerTwo = menu.querySelector('.main-menu-wrapper__submenu-two-wrapper');
        var containerThree = menu.querySelector('.main-menu-wrapper__submenu-three-wrapper');
        var containerBanner = menu.querySelector('.main-menu-wrapper__submenu-banner-wrapper');

        var menuScrollbar = new PerfectScrollbar(mainMenu,{
            wheelSpeed: 0.5,
            wheelPropagation: true,
            minScrollbarLength: 20,
            typeContainer: 'li'
        });

        mainMenu.style.width = widthBtn + 'px';
        mainMenu.style.minWidth = widthBtn + 'px';
        menu.style.minWidth = widthBtn + 'px';
        menu.style.width = widthBtn +'px';
        widthSubmenu = (widthHeaderNav - widthBtn) / 3;

        window.addEventListener('resize', function () {
            widthBtn = document.querySelector('.header-two__main-nav-catalog > a').offsetWidth + widthBorder * 2;
            mainMenu.style.width = widthBtn + 'px';
            mainMenu.style.minWidth = widthBtn + 'px';
            menu.style.minWidth = widthBtn + 'px';
            menu.style.width = widthBtn + 'px';
            widthHeaderNav = document.querySelector('.header-two__main-nav').offsetWidth;
            widthSubmenu = (widthHeaderNav - widthBtn) / 3;
            if(containerTwo.firstElementChild) {
                containerTwo.firstElementChild.style.width = widthSubmenu + 'px';
            }
            if (containerThree.firstElementChild) {
                containerThree.firstElementChild.style.width = widthSubmenu + 'px';
            }
            if (containerBanner.firstElementChild) {
                containerBanner.firstElementChild.style.width = widthSubmenu + 'px';
            }
        });

        var tempItem = null;
        menuWrapper.addEventListener('mouseover', function (evt) {
            var btnEvt = evt.target;
            while (btnEvt !== this) {
                if(btnEvt.getAttribute('data-role') == 'item-menu' || btnEvt.getAttribute('data-role') == 'item-submenu' || btnEvt.getAttribute('data-role') == 'item-submenu-two') {
                    switch (btnEvt.getAttribute('data-role')) {
                        case 'item-menu':
                            if(btnEvt !== tempItem) {
                                hover(btnEvt, mainMenu);
                                delSubmenu(containerThree);
                                copySubmenu(btnEvt, containerTwo, containerThree);
                                delBanner(containerBanner);
                                copyBanner(btnEvt, containerBanner);
                                tempItem = btnEvt;
                                calcWidth();
                            }
                            break;
                        case 'item-submenu':
                            if(btnEvt !== tempItem) {
                                hover(btnEvt, btnEvt.parentElement);
                                copySubmenu(btnEvt, containerThree);
                                tempItem = btnEvt;
                                calcWidth();
                            }
                            break;
                        case 'item-submenu-two':
                            if(btnEvt !== tempItem) {
                                hover(btnEvt, btnEvt.parentElement);
                                tempItem = btnEvt;
                                calcWidth();
                            }
                            break;

                        default:
                            break;
                    }
                    return;
                }
                btnEvt = btnEvt.parentNode;
            }
        });

        menuWrapper.addEventListener('mouseleave', function (evt) {
            delSubmenu(containerTwo);
            delSubmenu(containerThree);
            delHover(this);
            delBanner(containerBanner);
            tempItem = null;
            menu.style.width = widthBtn + 'px';
        });

        menuBtn.addEventListener('mouseover', function (evt) {
            menuScrollbar.update();
        });

        function copySubmenu(item, container, containerNext) {
            if(item.querySelector('.js-submenu')) {
                var submenu = item.querySelector('.js-submenu');
                if(container.firstElementChild) {
                    container.removeChild(container.firstElementChild);
                }
                container.appendChild(submenu.cloneNode(true));
                container.firstElementChild.style.width = widthSubmenu + 'px';
                // calcWidth();

                new PerfectScrollbar(container.firstElementChild,{
                    wheelSpeed: 0.5,
                    wheelPropagation: true,
                    minScrollbarLength: 20
                });
                if(containerNext) {
                    if (submenu.firstElementChild.querySelector('ul')) {
                        containerNext.appendChild(submenu.firstElementChild.querySelector('ul').cloneNode(true));
                        containerNext.firstElementChild.style.width = widthSubmenu + 'px';
                        // calcWidth();
                    }
                }
            } else {
                if(container.firstElementChild) {
                    container.removeChild(container.firstElementChild);
                }
            }
        }

        function delSubmenu (container) {
            if(container.firstElementChild) {
                container.removeChild(container.firstElementChild);
            }
        }

        function copyBanner (item, container) {
            if (item.querySelector('.main-menu-wrapper__submenu-banner')){
                container.appendChild(item.querySelector('.main-menu-wrapper__submenu-banner').cloneNode(true));
                container.firstElementChild.style.width = widthSubmenu + 'px';
                // calcWidth();
            };
        }

        function delBanner (container) {
            if(container.firstElementChild) {
                container.removeChild(container.firstElementChild);
            }
        }

        function hover (item, container) {
            var items = container.children;
            for(var i = 0; items.length > i; i++) {
                items[i].classList.remove('hover');
            }
            item.classList.add('hover');
        }

        function delHover (container) {
            var allItemsHover = container.querySelectorAll('.hover');
            for (var i = 0; i < allItemsHover.length; i++) {
                allItemsHover[i].classList.remove('hover');

            }
        }

        function calcWidth() {
            var summWidth = widthBtn + containerTwo.offsetWidth + containerThree.offsetWidth + containerBanner.offsetWidth - widthBorder;
            menu.style.width = summWidth + 'px';
        }

        menuWrapper.addEventListener('wheel', function (evt) {
            evt.preventDefault();
        });
    });
})();

$(document).ready(function () {


    // ---scroll-----
    var cont = document.querySelector('.header-two__menu-catalog');
    if(cont) {
        new PerfectScrollbar(cont,{
            wheelSpeed: 0.5,
            wheelPropagation: true,
            minScrollbarLength: 20,
            // typeContainer: 'li'
        });
    }


    var navSubmenu = document.querySelectorAll('.header-two__nav-submenu');
    if(navSubmenu) {
        for (var i = 0; navSubmenu.length > i; i++) {
            new PerfectScrollbar(navSubmenu[i],{
                wheelSpeed: 0.5,
                wheelPropagation: true,
                minScrollbarLength: 20,
                typeContainer: 'li'
            });
        }
    }
});
