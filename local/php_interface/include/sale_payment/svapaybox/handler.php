<?php

namespace Sale\Handlers\PaySystem;

use Bitrix\Main\Config,
    Bitrix\Main\Error,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Request,
    Bitrix\Main\Result,
    Bitrix\Main\Text\Encoding,
    Bitrix\Main\Type\DateTime,
    Bitrix\Main\Web\HttpClient,
    Bitrix\Sale\Order,
    Bitrix\Sale\PaySystem,
    Bitrix\Sale\Payment,
    Bitrix\Sale\PriceMaths;

Loc::loadMessages(__FILE__);

class SvaPayBoxHandler extends PaySystem\ServiceHandler// implements PaySystem\IRefundExtended, PaySystem\IHold implements, IReturn
{
    /**
     * @param Payment $payment
     * @param Request|null $request
     *
     * @return PaySystem\ServiceResult
     */
    public function initiatePay(Payment $payment, Request $request = null)
    {
        $params = array('URL' => $this->getUrl($payment, 'pay'));

        $arRequestFields = $this->getRequestFields($payment);
        $arRequestFields['pg_sig'] = $this->getSignature($payment, $arRequestFields);

        $params['REQUEST_FIELDS'] = $arRequestFields;

        $this->setExtraParams($params);

        return $this->showTemplate($payment, "template");
    }

    private function getRequestFields(Payment $payment)
    {
        $arRequest = Array(
            "pg_merchant_id" => $this->getBusinessValue($payment, 'SHOP_MERCHANT_ID'),
            "pg_amount" => intval($this->getBusinessValue($payment, 'SHOULD_PAY')),
            "pg_salt" => uniqid(),
            "pg_order_id" => $this->getBusinessValue($payment, 'PAYMENT_ID'),
            "pg_description" => 'Order: ' . $this->getBusinessValue($payment, 'ORDER_ID') . '; Payment ID: ' . $this->getBusinessValue($payment, 'PAYMENT_ID'),
            "pg_success_url" => "https://" . $_SERVER['SERVER_NAME'] . ($this->getBusinessValue($payment, 'SUCCESS_URL') ? $this->getBusinessValue($payment, 'SUCCESS_URL') : "/bitrix/tools/sale_ps_result.php"),
            "pg_success_url_method" => $this->getBusinessValue($payment, 'SUCCESS_URL_METHOD'),
            "pg_result_url" => "https://" . $_SERVER['SERVER_NAME'] . ($this->getBusinessValue($payment, 'RESULT_URL') ? $this->getBusinessValue($payment, 'RESULT_URL') : "/bitrix/tools/sale_ps_result.php"),
            "pg_result_url_method" => $this->getBusinessValue($payment, 'RESULT_URL_METHOD'),
            "pg_payment_system" => $this->getBusinessValue($payment, 'PAYMENT_SYSTEM'),
            "pg_testing_mode" => $this->getBusinessValue($payment, 'SHOP_TESTING_MODE'),
        );

        $arRequest = array_merge($this->getIndicativeFields(), $arRequest);

        ksort($arRequest);
        return $arRequest;
    }

    private function getSignature(Payment $payment, Array $arRequest)
    {
        $strScriptName = "payment.php";
        $strSecretKey = $this->getBusinessValue($payment, 'SHOP_SECRET_KEY');

        array_unshift($arRequest, $strScriptName);
        array_push($arRequest, $strSecretKey);

        $strParams = join(';', $arRequest);

        return md5($strParams);
    }

    /**
     * @return mixed
     */
    protected function getUrlList()
    {
        return array(
            'pay' => array(
                self::ACTIVE_URL => 'https://paybox.kz/payment.php'
            ),
        );
    }

    /**
     * @param Payment $payment
     * @return bool
     */
    protected function isTestMode(Payment $payment = null)
    {
        return ($this->getBusinessValue($payment, 'PS_IS_TEST') == 'Y');
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function getPaymentIdFromRequest(Request $request)
    {
        $paymentId = $request->get('ORDER_ID');
        $paymentId = preg_replace("/^[0]+/", "", $paymentId);

        return intval($paymentId);
    }

    /**
     * @return array
     */
    public function getCurrencyList()
    {
        return array('KZT');
    }

    /**
     * @param PaySystem\ServiceResult $result
     * @param Request $request
     * @return mixed
     */
    public function sendResponse(PaySystem\ServiceResult $result, Request $request)
    {
        global $APPLICATION;
        $APPLICATION->RestartBuffer();

        $data = $result->getData();
        echo $data['response'];

        $APPLICATION->FinalActions();
        die();
    }

    /**
     * @param Payment $payment
     * @param Request $request
     *
     * @return PaySystem\ServiceResult
     */
    public function processRequest(Payment $payment, Request $request)
    {
        $result = new PaySystem\ServiceResult();

        /****************/
        $arResult = Array(
            "pg_status" => "ok"
        );

        $strResult = "<?xml version=\"1.0\" encoding=\"utf-8\"?><response>";
        foreach($arResult as $strResultKey => $strResultValue)
        {
            $strResult .= "<" . $strResultKey . ">" . $strResultValue . "</" . $strResultKey . ">";
        }

        $strResult .= "</response>";
        /****************/

        $data['response'] = $strResult;

        $fields = array(
            "PS_STATUS_CODE" => "Y",
            "PS_STATUS_MESSAGE" => '',
            "PS_CURRENCY" => 'KZT',
            "PS_RESPONSE_DATE" => new DateTime(),
            "PS_INVOICE_ID" => '',
        );

        $result->setData($data);

        return $result;
    }

    public static function getIndicativeFields()
    {
        return array('BX_HANDLER' => 'SVA_PAYBOX');
    }


    /**
     * @param Request $request
     * @param $paySystemId
     * @return bool
     */

    static protected function isMyResponseExtended(Request $request, $paySystemId)
    {
        return true;
    }
}