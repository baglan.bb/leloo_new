<?
use Bitrix\Main\Localization\Loc;

\Bitrix\Main\Page\Asset::getInstance()->addCss("/bitrix/themes/.default/sale.css");
Loc::loadMessages(__FILE__);
?>
<div class="mb-4">
    <?= Loc::getMessage('SALE_HANDLERS_PAY_SYSTEM_YANDEX_DESCRIPTION') . " " . SaleFormatCurrency($params['PAYMENT_SHOULD_PAY'], $payment->getField('CURRENCY')); ?>
    <form name="ShopForm" action="<?= $params['URL']; ?>" method="post">

        <?
        foreach($params["REQUEST_FIELDS"] as $strFieldName => $strFieldValue)
        {
            ?><input type="hidden" name="<?= $strFieldName ?>" value="<?= htmlspecialcharsbx($strFieldValue); ?>"><?
        }
        ?>

        <div class="d-flex align-items-center justify-content-start">
            <input class="btn btn-primary pl-4 pr-4" value="<?= Loc::getMessage('SALE_HANDLERS_PAY_SYSTEM_SVA_PAYBOX_BUTTON_PAID') ?>" type="submit">
            <p><?= Loc::getMessage('SALE_HANDLERS_PAY_SYSTEM_SVA_PAYBOX_REDIRECT_MESS'); ?></p>
        </div>

        <p><?= Loc::getMessage('SALE_HANDLERS_PAY_SYSTEM_YANDEX_WARNING_RETURN'); ?></p>
    </form>
</div>