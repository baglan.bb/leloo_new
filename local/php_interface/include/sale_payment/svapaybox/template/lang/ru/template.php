<?php
$MESS["SALE_HANDLERS_PAY_SYSTEM_SVA_PAYBOX_BUTTON_PAID"] = "Перейти к оплате";
$MESS["SALE_HANDLERS_PAY_SYSTEM_SVA_PAYBOX_REDIRECT_MESS"] = "Вы будете перенаправленны на страницу оплаты";
$MESS["SALE_HANDLERS_PAY_SYSTEM_SVA_PAYBOX_WARNING_RETURN"] = "<b>Обратите внимание:</b> если вы откажетесь от покупки, для возврата денег вам придется обратиться в магазин.";
