<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
    die();
}

use Bitrix\Main\Localization\Loc;

CModule::IncludeModule("sale");
$getList = CSaleStatus::GetList(array(), array("LID" => LANGUAGE_ID));
$arrStatusName = array();
while ($arrStatus = $getList->Fetch())
{
    $arrStatusName[] = $arrStatus;
}

$arrStatusIdAndName = array();
foreach ($arrStatusName as $key => $value)
{
    $k = $arrStatusName[$key]['ID'];
    $arrStatusIdAndName[$k] = array(
        'NAME' => $arrStatusName[$key]['NAME']
    );
}

$data = array(
    'NAME' => 'PayBox',
    'SORT' => 500,
    'CODES' => array(
        "SHOP_MERCHANT_ID" => array(
            "NAME" => Loc::getMessage("SVA_PAYBOX_SHOP_MERCHANT_ID"),
            "DESCRIPTION" => Loc::getMessage("SVA_PAYBOX_SHOP_MERCHANT_ID_DESCR"),
            "VALUE" => "",
            "TYPE" => ""
        ),
        "SHOP_SECRET_KEY" => array(
            "NAME" => Loc::getMessage("SVA_PAYBOX_SHOP_SECRET_KEY"),
            "DESCRIPTION" => Loc::getMessage("SVA_PAYBOX_SHOP_SECRET_KEY_DESCR"),
            "VALUE" => "",
            "TYPE" => ""
        ),
        "SHOP_TESTING_MODE" => array(
            "NAME" => Loc::getMessage("SVA_PAYBOX_SHOP_TESTING_MODE"),
            "DESCRIPTION" => Loc::getMessage("SVA_PAYBOX_SHOP_TESTING_MODE_DESCR"),
            "VALUE" => "",
            "TYPE" => ""
        ),
        "PAYMENT_ID" => array(
            "NAME" => Loc::getMessage("SVA_PAYBOX_PAYMENT_ID"),
            "DESCRIPTION" => Loc::getMessage("SVA_PAYBOX_PAYMENT_ID_DESCR"),
            'DEFAULT' => array(
                'PROVIDER_KEY' => 'PAYMENT',
                'PROVIDER_VALUE' => 'ACCOUNT_NUMBER'
            ),
            "TYPE" => "PAYMENT"
        ),
        "ORDER_ID" => array(
            "NAME" => Loc::getMessage("SVA_PAYBOX_ORDER_ID"),
            "DESCRIPTION" => Loc::getMessage("SVA_PAYBOX_ORDER_ID_DESCR"),
            'DEFAULT' => array(
                'PROVIDER_KEY' => 'ORDER',
                'PROVIDER_VALUE' => 'ACCOUNT_NUMBER'
            ),
            "TYPE" => "ORDER"
        ),
        "ORDER_LIVETIME" => array(
            "NAME" => Loc::getMessage("SVA_PAYBOX_ORDER_LIVETIME"),
            "DESCRIPTION" => Loc::getMessage("SVA_PAYBOX_ORDER_LIVETIME_DESCR"),
            "VALUE" => "",
            "TYPE" => ""
        ),
        "SHOULD_PAY" => array(
            "NAME" => Loc::getMessage("SVA_PAYBOX_SHOULD_PAY"),
            "DESCRIPTION" => Loc::getMessage("SVA_PAYBOX_SHOULD_PAY_DESCR"),
            "VALUE" => "SHOULD_PAY",
            'DEFAULT' => array(
                'PROVIDER_KEY' => 'PAYMENT',
                'PROVIDER_VALUE' => 'SUM'
            )
        ),
        "SITE_URL" => array(
            "NAME" => Loc::getMessage("SVA_PAYBOX_SITE_URL"),
            "DESCRIPTION" => Loc::getMessage("SVA_PAYBOX_SITE_URL_DESCR"),
            "VALUE" => "http://" . $_SERVER['HTTP_HOST'],
            "TYPE" => ""
        ),
        "CHECK_URL" => array(
            "NAME" => Loc::getMessage("SVA_PAYBOX_CHECK_URL"),
            "DESCRIPTION" => Loc::getMessage("SVA_PAYBOX_CHECK_URL"),
            "VALUE" => "http://" . $_SERVER['HTTP_HOST'] . "/paybox/check.php",
            "TYPE" => ""
        ),
        "RESULT_URL" => array(
            "NAME" => Loc::getMessage("SVA_PAYBOX_RESULT_URL"),
            "DESCRIPTION" => Loc::getMessage("SVA_PAYBOX_RESULT_URL_DESCR"),
            "VALUE" => "http://" . $_SERVER['HTTP_HOST'] . "/paybox/result.php",
            "TYPE" => ""
        ),
        "REFUND_URL" => array(
            "NAME" => Loc::getMessage("SVA_PAYBOX_REFUND_URL"),
            "DESCRIPTION" => Loc::getMessage("SVA_PAYBOX_REFUND_URL_DESCR"),
            "VALUE" => "http://" . $_SERVER['HTTP_HOST'] . "/paybox/refund.php",
            "TYPE" => ""
        ),
        "REQUEST_METHOD" => array(
            "NAME" => Loc::getMessage("SVA_PAYBOX_REQUEST_METHOD"),
            "DESCRIPTION" => Loc::getMessage("SVA_PAYBOX_REQUEST_METHOD_DESCR"),
            "VALUE" => "POST",
            "TYPE" => ""
        ),
        "SUCCESS_URL" => array(
            "NAME" => Loc::getMessage("SVA_PAYBOX_SUCCESS_URL"),
            "DESCRIPTION" => Loc::getMessage("SVA_PAYBOX_SUCCESS_URL_DESCR"),
            "VALUE" => "http://" . $_SERVER['HTTP_HOST'] . "/paybox/success.php",
            "TYPE" => ""
        ),
        "SUCCESS_URL_METHOD" => array(
            "NAME" => Loc::getMessage("SVA_PAYBOX_SUCCESS_URL_METHOD"),
            "DESCRIPTION" => Loc::getMessage("SVA_PAYBOX_SUCCESS_URL_METHOD_DESCR"),
            "VALUE" => "AUTOPOST",
            "TYPE" => ""
        ),
        "FAILURE_URL" => array(
            "NAME" => Loc::getMessage("SVA_PAYBOX_FAILURE_URL"),
            "DESCRIPTION" => Loc::getMessage("SVA_PAYBOX_FAILURE_URL_DESCR"),
            "VALUE" => "http://" . $_SERVER['HTTP_HOST'] . "/paybox/failure.php",
            "TYPE" => ""
        ),
        "FAILURE_URL_METHOD" => array(
            "NAME" => Loc::getMessage("SVA_PAYBOX_FAILURE_URL_METHOD"),
            "DESCRIPTION" => Loc::getMessage("SVA_PAYBOX_FAILURE_URL_METHOD_DESCR"),
            "VALUE" => "POST",
            "TYPE" => ""
        ),
        "STATUS_FAILED" => array(
            "NAME" => Loc::getMessage("SVA_PAYBOX_STATUS_FAILED"),
            "DESCRIPTION" => Loc::getMessage("SVA_PAYBOX_STATUS_FAILED_DESCR"),
            "VALUE" => $arrStatusIdAndName,
            "TYPE" => "SELECT"
        ),
        "STATUS_REVOKED" => array(
            "NAME" => Loc::getMessage("SVA_PAYBOX_STATUS_REVOKED"),
            "DESCRIPTION" => Loc::getMessage("SVA_PAYBOX_STATUS_REVOKED_DESCR"),
            "VALUE" => $arrStatusIdAndName,
            "TYPE" => "SELECT"
        ),
        "PAYMENT_SYSTEM" => array(
            "NAME" => Loc::getMessage("SVA_PAYBOX_PS_NAME"),
            "DESCRIPTION" => Loc::getMessage("SVA_PAYBOX_PS_NAME_DESCR"),
            "VALUE" => "",
            "TYPE" => ""
        ),
    )
);