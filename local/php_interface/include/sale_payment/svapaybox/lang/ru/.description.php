<?php
$MESS["SVA_PAYBOX_SHOP_MERCHANT_ID"] = "Идентификатор магазина в PayBox ( Merchant ID )";
$MESS["SVA_PAYBOX_SHOP_MERCHANT_ID_DESCR"] = "используется для идентификации магазига при совершении платежей.";
$MESS["SVA_PAYBOX_SHOP_SECRET_KEY"] = "Кодовое слово (Secret Key) ";
$MESS["SVA_PAYBOX_SHOP_SECRET_KEY_DESCR"] = "Используется для подтверждения идентификации магазина при совершении платежей.";
$MESS["SVA_PAYBOX_SHOP_TESTING_MODE"] = "Тестовый режим ";
$MESS["SVA_PAYBOX_SHOP_SECRET_KEY_DESCR"] = "В случае если вы находитесь в боевом режиме, но вам нужно провести тестовый транзакции, ставите Y и все транзакции будут создаваться по тестовым платежным системам.";

$MESS["SVA_PAYBOX_ORDER_ID"] = "Номер заказа";
$MESS["SVA_PAYBOX_ORDER_ID_DESCR"] = "Номер заказа в системе сайта";
$MESS["SVA_PAYBOX_ORDER_LIVETIME"] = "Время жизни счета";
$MESS["SVA_PAYBOX_ORDER_LIVETIME_DESCR"] = "Измеряется в секундах, если вы указали пустую строку, время жизни по умолчанию 1 сутки.";

$MESS["SVA_PAYBOX_PAYMENT_ID"] = "Номер оплаты";
$MESS["SVA_PAYBOX_PAYMENT_ID_DESCR"] = "Номер оплаты в системе сайта";

$MESS["SVA_PAYBOX_SHOULD_PAY"] = "Сумма заказа";
$MESS["SVA_PAYBOX_SHOULD_PAY_DESCR"] = "Сумма к оплате.";

$MESS["SVA_PAYBOX_SITE_URL"] = "Site URL";
$MESS["SVA_PAYBOX_SITE_URL_DESCR"] = "";

$MESS["SVA_PAYBOX_CHECK_URL"] = "Check URL";
$MESS["SVA_PAYBOX_CHECK_URL_DESCR"] = "Адрес скрипта, отвечающего на запросы check.";
$MESS["SVA_PAYBOX_RESULT_URL"] = "Result URL";
$MESS["SVA_PAYBOX_RESULT_URL_DESCR"] = "Адрес скрипта, отвечающего на запросы result.";
$MESS["SVA_PAYBOX_REFUND_URL"] = "Refund URL";
$MESS["SVA_PAYBOX_REFUND_URL_DESCR"] = "Адрес скрипта, отвечающего на запросы refund.";

$MESS["SVA_PAYBOX_REQUEST_METHOD"] = "Метод запроса на Check URL и Result URL";
$MESS["SVA_PAYBOX_REQUEST_METHOD_DESCR"] = "Вазможные варианты: (POST и GET).";

$MESS["SVA_PAYBOX_SUCCESS_URL"] = "Success URL";
$MESS["SVA_PAYBOX_SUCCESS_URL_DESCR"] = "Адрес на который возрвращать клиента при удачном проведение транзакции.";
$MESS["SVA_PAYBOX_SUCCESS_URL_METHOD"] = "Метод запроса на Success URL";
$MESS["SVA_PAYBOX_SUCCESS_URL_METHOD_DESCR"] = "Вазможные варианты: (POST, GET, AUTOPOST и AUTOGET).";
$MESS["SVA_PAYBOX_FAILURE_URL"] = "Failure URL";
$MESS["SVA_PAYBOX_FAILURE_URL_DESCR"] = "Адрес на который возрвращать клиента при не удачном проведение транзакции.";
$MESS["SVA_PAYBOX_FAILURE_URL_METHOD"] = "Метод запроса на Failure URL";
$MESS["SVA_PAYBOX_FAILURE_URL_METHOD_DESCR"] = "Вазможные варианты: (POST, GET, AUTOPOST и AUTOGET).";
$MESS["SVA_PAYBOX_STATUS_FAILED"] = "Статус после отказа";
$MESS["SVA_PAYBOX_STATUS_FAILED_DESCR"] = "Статус после отказа платежа.";
$MESS["SVA_PAYBOX_STATUS_REVOKED"] = "Статус после отмены";
$MESS["SVA_PAYBOX_STATUS_REVOKED_DESCR"] = "Статус после отмены платежа (возврата).";

$MESS["SVA_PAYBOX_ORDER_DATE"] = "Дата создания заказа";
$MESS["SVA_PAYBOX_ORDER_DATE_DESCR"] = "";

$MESS["SVA_PAYBOX_NEW_EMAIL"] = "Неправильно введён e-mail";
$MESS["SVA_PAYBOX_USE_NEW"] = "Использовать новый ";
$MESS["SVA_PAYBOX_USE_NEW_EMAIL"] = "e-mail";

$MESS["SVA_PAYBOX_PS_NAME"] = "Название ПС";
$MESS["SVA_PAYBOX_PS_NAME_DESCR"] = "Название платежной системы (pg_payment_system)";