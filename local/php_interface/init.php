<?
require 'events.php';

use Bitrix\Sale\BusinessValue;

function print_arr($arr)
{
    echo "<pre>";
    print_r($arr);
    echo "</pre>";
}

function getPayment($accountNumber)
{
    $payment = null;
    $ar = explode("/", $accountNumber);
    $orderID = $ar[0];

    $order = \Bitrix\Sale\Order::load($orderID);
    $paymentCollection = $order->getPaymentCollection();

    foreach ($paymentCollection as $orderPayment)
    {
        if($orderPayment->getField('ACCOUNT_NUMBER') == $accountNumber)
        {
            $payment = $orderPayment;
        }
    }

    return $payment;
}

function getRequestFields(\Bitrix\Sale\Payment $payment)
{
    $paymentSystem = $payment->getPaySystem();
//    BusinessValue::getValueFromProvider($payment, "SHOP_MERCHANT_ID", $paymentSystem->getConsumerName())
    $arRequest = Array(
        "pg_merchant_id" => BusinessValue::getValueFromProvider($payment, 'SHOP_MERCHANT_ID', $paymentSystem->getConsumerName()),
        "pg_amount" => intval(BusinessValue::getValueFromProvider($payment, 'SHOULD_PAY', $paymentSystem->getConsumerName())),
        "pg_salt" => uniqid(),
        "pg_order_id" => BusinessValue::getValueFromProvider($payment, 'PAYMENT_ID', $paymentSystem->getConsumerName()),
        "pg_description" => 'Order: ' . BusinessValue::getValueFromProvider($payment, 'ORDER_ID', $paymentSystem->getConsumerName()) . '; Payment ID: ' . BusinessValue::getValueFromProvider($payment, 'PAYMENT_ID', $paymentSystem->getConsumerName()),
        "pg_success_url" => "https://" . $_SERVER['SERVER_NAME'] . (BusinessValue::getValueFromProvider($payment, 'SUCCESS_URL', $paymentSystem->getConsumerName()) ? BusinessValue::getValueFromProvider($payment, 'SUCCESS_URL', $paymentSystem->getConsumerName()) : "/bitrix/tools/sale_ps_result.php"),
        "pg_success_url_method" => BusinessValue::getValueFromProvider($payment, 'SUCCESS_URL_METHOD', $paymentSystem->getConsumerName()),
        "pg_result_url" => "https://" . $_SERVER['SERVER_NAME'] . (BusinessValue::getValueFromProvider($payment, 'RESULT_URL', $paymentSystem->getConsumerName()) ? BusinessValue::getValueFromProvider($payment, 'RESULT_URL', $paymentSystem->getConsumerName()) : "/bitrix/tools/sale_ps_result.php"),
        "pg_result_url_method" => BusinessValue::getValueFromProvider($payment, 'RESULT_URL_METHOD', $paymentSystem->getConsumerName()),
        "pg_payment_system" => BusinessValue::getValueFromProvider($payment, 'PAYMENT_SYSTEM', $paymentSystem->getConsumerName()),
        "pg_testing_mode" => BusinessValue::getValueFromProvider($payment, 'SHOP_TESTING_MODE', $paymentSystem->getConsumerName()),
    );

    $arRequest = array_merge(Array('BX_HANDLER' => 'SVA_PAYBOX'), $arRequest);

    ksort($arRequest);
    return $arRequest;
}

function get_the_browser()
{
    if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)
        return 'Internet explorer';
    elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Trident') !== false)
        return 'Internet explorer';
    elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') !== false)
        return 'Mozilla Firefox';
    elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== false)
        return 'Google Chrome';
    elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false)
        return "Opera Mini";
    elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Opera') !== false)
        return "Opera";
    elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') !== false)
        return "Safari";
    else
        return 'Other';
}


/**
 * @param $imageSrc
 *
 * Меняет jpg или png на webp сохраняет в папку /upload/webp и возвращает нвоый путь если все получилось сделать иначе возвращает оригинальный путь.
 *
 * @return string
 */
function webp(string $imageSrc):string{
    $regexp = "/\.(jpe?g|png)$/m";

    if(!preg_match_all($regexp, $imageSrc)){
        return $imageSrc;
    }

//    if(get_the_browser() != "Google Chrome"){
//        return $imageSrc;
//    }

    $fullSrc = \Bitrix\Main\Application::getDocumentRoot() . $imageSrc;

    if(!file_exists($fullSrc)){
        return false;
    }

    $fullWebPSrc = preg_replace($regexp, ".webp", \Bitrix\Main\Application::getDocumentRoot() . str_replace("/upload/", "/upload/webp/", $imageSrc));
    $webPSrc = str_replace(\Bitrix\Main\Application::getDocumentRoot(), "", $fullWebPSrc);

    $mime = mime_content_type($fullSrc);

    switch ($mime){
        case "image/jpeg":
            $rsImage = imagecreatefromjpeg($fullSrc);
            break;
        case "image/png":
            $rsImage = imagecreatefrompng($fullSrc);
            break;
    }

    $arPathParts = explode("/", dirname($webPSrc));

    for($i = 1; $i < count($arPathParts); $i++)
    {
        $tmpArPathParts = $arPathParts;
        $tmpPart = \Bitrix\Main\Application::getDocumentRoot() . join("/", array_splice($tmpArPathParts, 0, $i + 1));
        if(!is_dir($tmpPart)){
            mkdir($tmpPart);
        }
    }

    if(!is_dir(dirname($fullWebPSrc)))
    {
        mkdir(dirname($fullWebPSrc));
    }

    imagewebp($rsImage, $fullWebPSrc, 100);

    return $webPSrc;
}

if (!function_exists('custom_mail') && COption::GetOptionString("webprostor.smtp", "USE_MODULE") == "Y")
{
    function custom_mail($to, $subject, $message, $additional_headers='', $additional_parameters='')
    {
        if (stripos($to, '@bx.bx')>0)
            return false;
        if(CModule::IncludeModule("webprostor.smtp"))
        {

            // исправляю переносы строк в заголовках
            if (!empty($additional_headers)) {
                $additional_headers = str_replace("\r\n", "\n", $additional_headers); // на случай если уже нормальные переносы
                $additional_headers = str_replace("\n", "\r\n", $additional_headers);
            }

            $smtp = new CWebprostorSmtp("s1");
            $result = $smtp->SendMail($to, $subject, $message, $additional_headers, $additional_parameters);

            if($result)
                return true;
            else
                return false;
        }
    }
}

class SvaBlock {
    private $dir;
    private $settings;
    private $style;

    function __construct($dir)
    {
        $this->dir = $dir;
        $this->fetchSettings();
        $this->buildStyle();
        $this->addAssets();
    }

    public function addAssets() {
        foreach($this->settings["ext"]["css"] as $file){
            Bitrix\Main\Page\Asset::getInstance()->addCss(str_replace($_SERVER["DOCUMENT_ROOT"], "", $file));
        }

        foreach($this->settings["ext"]["js"] as $file){
            Bitrix\Main\Page\Asset::getInstance()->addJs(str_replace($_SERVER["DOCUMENT_ROOT"], "", $file));
        }
    }

    public function getStyle(){
        return $this->style;
    }

    public function getSettings(){
        return $this->settings;
    }

    private function fetchSettings()
    {
        $strSettings = file_get_contents($this->dir . "/settings.php");
        $this->settings =  unserialize($strSettings);
    }

    private function buildStyle(){
        $style = '';
        $padding = '';
        if ($this->settings['style']['padding-top']['value']) {
            $padding .= $this->settings['style']['padding-top']['value'].'px ';
        } else {
            $padding .= '0px ';
        }
        if ($this->settings['style']['padding-right']['value']) {
            $padding .= $this->settings['style']['padding-right']['value'].'px ';
        } else {
            $padding .= '0px ';
        }
        if ($this->settings['style']['padding-bottom']['value']) {
            $padding .= $this->settings['style']['padding-bottom']['value'].'px ';
        } else {
            $padding .= '0px ';
        }
        if ($this->settings['style']['padding-left']['value']) {
            $padding .= $this->settings['style']['padding-left']['value'].'px ';
        } else {
            $padding .= '0px ';
        }
        if ($padding) {
            $style .= 'padding: '.$padding.'; ';
        }

        $background = "";

        if ($this->settings['style']['background-color']['value']) {
            $background .= 'background-color: '. $this->settings['style']['background-color']['value'].';';
        }
        if ($this->settings['style']['background-image']['value'] > 0) {
            $imgPath = \CFile::GetPath($this->settings['style']['background-image']['value']);
            $background .= 'background-image: url('.$imgPath.');';
        }
        if ($this->settings['style']['background-position']['value']) {
            $background .= 'background-position: '.$this->settings['style']['background-position']['value'].';';
        }
        if ($this->settings['style']['background-repeat']['value']) {
            $background .= 'background-repeat: '.$this->settings['style']['background-repeat']['value'].';';
        }
        if ($this->settings['style']['background-size']['value']) {
            $background .= 'background-size: '.$this->settings['style']['background-size']['value'].';';
        }
        if ($this->settings['style']['background-attachment']['value']) {
            $background .= 'background-attachment: '.$this->settings['style']['background-attachment']['value'].';';
        }
        if ($this->settings['style']['background-clip']['value']) {
            $background .= 'background-clip: '.$this->settings['style']['background-clip']['value'].';';
        }
        if ($this->settings['style']['background-origin']['value']) {
            $background .= 'background-origin: '.$this->settings['style']['background-origin']['value'].';';
        }

        if ($background) {
            $style .= $background;
        }

        $this->style = $style;
    }
}

//записать цену со скидкой в свойсьва товара
function MyUpdateFilter() {
    /* Получение всех элементов */
    $arOrder = array("SORT" => "ASC");
    $arFilter = array("IBLOCK_ID" => (11));
    $arGroupBy = false;
    $arNavStartParams = false;
    $arSelectFields = array("ID","PROPERTY_FINAL_PRICE");
    $GLOBALS['TODAY_HAS_DISCOUNT'] = false;
    $GetListElement = CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
    while($GetListElementRow = $GetListElement->GetNextElement()) {
        $GetListElementItem = $GetListElementRow->GetFields();

        $discount = getPriceById($GetListElementItem["ID"]);
        /* Изменение свойства */
        if($discount > 0) {
            CIBlockElement::SetPropertyValues($GetListElementItem["ID"], 11, 1408, "DISCOUNT");
        }
        else
            CIBlockElement::SetPropertyValues($GetListElementItem["ID"], 11, 1409, "DISCOUNT");

    }

    $arOrder = array("SORT" => "ASC");
    $arFilter = array("IBLOCK_ID" => (13));
    $arGroupBy = false;
    $arNavStartParams = false;
    $arSelectFields = array("ID","PROPERTY_FINAL_PRICE");

    $GetListElement = CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
    while($GetListElementRow = $GetListElement->GetNextElement()) {
        $GetListElementItem = $GetListElementRow->GetFields();

        $discount = getPriceById($GetListElementItem["ID"]);
        /* Изменение свойства */
        if($discount > 0) {
            CIBlockElement::SetPropertyValues($GetListElementItem["ID"], 11, 1410, "DISCOUNT");
        }
        else
            CIBlockElement::SetPropertyValues($GetListElementItem["ID"], 11, 1411, "DISCOUNT");

    }
    echo 'Done!';
}

function getPriceById($id){
    global $USER;
    $arPrice = CCatalogProduct::GetOptimalPrice($id, 1, $USER->GetUserGroupArray(), 'N');
    if (!$arPrice || count($arPrice) <= 0)
    {
        if ($nearestQuantity = CCatalogProduct::GetNearestQuantityPrice($id, 1, $USER->GetUserGroupArray()))
        {
            $quantity = $nearestQuantity;
            $arPrice = CCatalogProduct::GetOptimalPrice($id, $quantity, $USER->GetUserGroupArray(), 'N');
        }
    }
    return $arPrice['RESULT_PRICE']['DISCOUNT'];
}


//get city by IP
//geo ext
require_once $_SERVER['DOCUMENT_ROOT'] . '/extensions/GeoIp2/autoload.php';
use GeoIp2\Database\Reader;

function getKZCities($OnlyAvailable=true)
{
    $KZ_Cities = array();

    $cities = getHighload('Cities', array(), array("UF_SORT" => "ASC"));

    foreach ($cities as $city) {
//        if ($city['UF_SORT']<1000) continue;
        $KZ_Cities[$city['UF_NAME']] = $city;
    }
    return $KZ_Cities;
}

function getMyCity($KZ_Cities,$ReturnKaspi=false)
{
    global $USER;
    $UserCity = urldecode(@$_COOKIE['city']);
    $MyCity = new Reader($_SERVER['DOCUMENT_ROOT'] . '/extensions/GeoIp2/GeoLite2-City.mmdb');
    $UserInfo = false;
    if ($USER->IsAuthorized()) {
        $UserInfo = CUser::GetByID($USER->GetID());
        $UserInfo = $UserInfo->arResult[0];
        $UserCity = $UserInfo['PERSONAL_CITY'];
        $GLOBALS["USER_CITY"] = $UserCity;
    }
    try {
        $MyCity = $MyCity->city($_SERVER['REMOTE_ADDR']);
        //        $MyCity = $MyCity->city('185.97.113.217');
        //        $MyCity = $MyCity->city('178.219.186.12'); //moscow ip
        $MyCity = $MyCity->city->names['ru'];
    } catch (Exception $e) {
        $MyCity = false;
    }
    if (!empty($UserCity) && isset($KZ_Cities[$UserCity])) {
        $CurrentCityName = $UserCity;
    } else {
        $CurrentCityName = !empty($MyCity) ? $MyCity : 'Алматы';
        setcookie('city', $CurrentCityName, time() + 3600 * 24 * 30, '/');
        $US = new CUser();
        $US->Update($USER->GetID(), array('PERSONAL_CITY' => $CurrentCityName));
    }
    if($ReturnKaspi) return $KZ_Cities[$CurrentCityName]['ID'];
    return $CurrentCityName;
}

function getHighload($HLiB_name, $Filter = array(), $Order = array(), $Select = array('*'))
{
    CModule::IncludeModule('highloadblock'); //модуль highload инфоблоков
    $rsData = \Bitrix\Highloadblock\HighloadBlockTable::getList(array('filter' => array('TABLE_NAME' => $HLiB_name)));
    $RecordsOut = false;
    if (!($hldata = $rsData->fetch())) {
        //          echo 'Инфоблок не найден';
    } else {
        $RecordsOut = array();
        $hlentity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
        $hlDataClass = $hldata['NAME'] . 'Table';
        $res = $hlDataClass::getList(array(
                'filter' => $Filter,
                'select' => $Select,
                'order' => $Order,
            )
        );
        while ($row = $res->fetch()) {
            $RecordsOut[$row['ID']] = $row;
        }
    }
    return $RecordsOut;
}

function getNotifyText($n,$type='icons'){
    switch ($type){
        case ('title'):
            $t = strstr($n, '&');
            return strstr(substr($t,1), '$', true);
            break;
        case('text'):
            return substr(substr(strstr($n, '$'),1),0,-2);
            break;
        case('show'):
            $t = substr($n,-1);
            return $t=='-Y'?true:false;
            break;
        default:
            return strstr($n, '&', true);
    }
}

function CustomAddMessage2Log($sText, $sModule = "", $traceDepth = 6, $bShowArgs = false)
{
    if (defined("CRM_LOG_FILENAME") && CRM_LOG_FILENAME <> '')
    {
        if(!is_string($sText))
        {
            $sText = var_export($sText, true);
        }
        if ($sText <> '')
        {
            ignore_user_abort(true);
            if ($fp = @fopen(CRM_LOG_FILENAME, "ab"))
            {
                if (flock($fp, LOCK_EX))
                {
                    @fwrite($fp, "Host: ".$_SERVER["HTTP_HOST"]."\nDate: ".date("Y-m-d H:i:s")."\nModule: ".$sModule."\n".$sText."\n");
                    $arBacktrace = Bitrix\Main\Diag\Helper::getBackTrace($traceDepth, ($bShowArgs? null : DEBUG_BACKTRACE_IGNORE_ARGS));
                    $strFunctionStack = "";
                    $strFilesStack = "";
                    $firstFrame = (count($arBacktrace) == 1? 0: 1);
                    $iterationsCount = min(count($arBacktrace), $traceDepth);
                    for ($i = $firstFrame; $i < $iterationsCount; $i++)
                    {
                        if ($strFunctionStack <> '')
                            $strFunctionStack .= " < ";

                        if (isset($arBacktrace[$i]["class"]))
                            $strFunctionStack .= $arBacktrace[$i]["class"]."::";

                        $strFunctionStack .= $arBacktrace[$i]["function"];

                        if(isset($arBacktrace[$i]["file"]))
                            $strFilesStack .= "\t".$arBacktrace[$i]["file"].":".$arBacktrace[$i]["line"]."\n";
                        if($bShowArgs && isset($arBacktrace[$i]["args"]))
                        {
                            $strFilesStack .= "\t\t";
                            if (isset($arBacktrace[$i]["class"]))
                                $strFilesStack .= $arBacktrace[$i]["class"]."::";
                            $strFilesStack .= $arBacktrace[$i]["function"];
                            $strFilesStack .= "(\n";
                            foreach($arBacktrace[$i]["args"] as $value)
                                $strFilesStack .= "\t\t\t".$value."\n";
                            $strFilesStack .= "\t\t)\n";

                        }
                    }

                    if ($strFunctionStack <> '')
                    {
                        @fwrite($fp, "    ".$strFunctionStack."\n".$strFilesStack);
                    }

                    @fwrite($fp, "----------\n");
                    @fflush($fp);
                    @flock($fp, LOCK_UN);
                    @fclose($fp);
                }
            }
            ignore_user_abort(false);
        }
    }
}


//данные из каспи в массив
function decodeKaspiData($data){
    $arr = [];
    $obj = json_decode($data);
    foreach ($obj->data as $key=>$d){
        $arr[$key]['DATE'] = date('d.m.Y H:i:s', substr($d->attributes->creationDate, 0, -3));
        $arr[$key]['CODE'] = $d->attributes->code;
        $arr[$key]['DELIVERY_DATE'] = date('d.m.Y', $d->attributes->plannedDeliveryDate/1000);
        $arr[$key]['NAME'] = $d->attributes->customer->firstName;
        $arr[$key]['LAST_NAME'] = $d->attributes->customer->lastName;
        $arr[$key]['PAYED'] = $d->attributes->signatureRequired?'На подписании':'Оплачен на Kaspi.kz';
        $arr[$key]['PRICE'] = $d->attributes->totalPrice;
        $arr[$key]['PHONE'] = '+7'.$d->attributes->customer->cellPhone;
        $arr[$key]['ADDRESS'] = $d->attributes->deliveryAddress->formattedAddress;
        $arr[$key]['COMMENT'] = $d->attributes->cancellationComment;
        $arr[$key]['DELIVERY_MODE'] = $d->attributes->deliveryMode;
        $arr[$key]['ID'] = $d->id;
        $arr[$key]['SOURCE'] = 'kaspi';
        $arr[$key]['PRODUCTS'] = getProductsFromKaspi($d->relationships->entries->links->related);
        //    $arr[$key]['PAY_TYPE'] = ($d->attributes->paymentMode=='PREPAID')?'Предоплата':'Оплата в кредит';
        //    $arr[$key]['STATUS'] = $d->attributes->status;
    }
    return $arr;
}

//get order products from kaspi
function getProductsFromKaspi($link){
    $products =[];
    $httpClientKaspi = new \Bitrix\Main\Web\HttpClient();
    $httpClientKaspi->setHeader('X-Auth-Token', 'a5TR1Wab6k6JS9ZUeR4qt4JUJ9Jy3+/JHsCwp3ItFOM=');
    $httpClientKaspi->setHeader('Content-Type', 'application/vnd.api+json');
    $result = $httpClientKaspi->get($link); // это вернет массив продкутов по пути relationships->product->links->related
    $obj = json_decode($result);
    foreach ($obj->data as $orderProduct){
        $products[getProdId($orderProduct->relationships->product->links->related, 'code')]['NAME'] = getProdId($orderProduct->relationships->product->links->related, 'name');
        $products[getProdId($orderProduct->relationships->product->links->related, 'code')]['PRICE'] = $orderProduct->attributes->basePrice;
        $products[getProdId($orderProduct->relationships->product->links->related, 'code')]['QUANTITY'] = $orderProduct->attributes->quantity;
    }
    return $products;
}

//получить id продукта по его ссылки. Каспи
function getProdId($link, $type){
    $httpClientProd = new \Bitrix\Main\Web\HttpClient();
    $httpClientProd->setHeader('X-Auth-Token', 'a5TR1Wab6k6JS9ZUeR4qt4JUJ9Jy3+/JHsCwp3ItFOM=');
    $httpClientProd->setHeader('Content-Type', 'application/vnd.api+json');
    $result = $httpClientProd->get($link); // это вернет массив продкутов по пути relationships->product->links->related
    $obj = json_decode($result);
    return $obj->data->attributes->$type;
}

//проверяем контакт, создаем сделку
function addDealToCrm($data, $stage, $products, $kaspi=false)
{
    //проверяем есть ли такой контакт
    $searchContact=CRest::call(
        "crm.contact.list",
        [
            "filter"=>["PHONE"=>$data['PHONE']],
            "select"=>["ID"]
        ]
    );
    if (intval($searchContact['result'][0]['ID'])>0) {
        $contactID=intval($searchContact['result'][0]['ID']);
    }else {
        //создаем контакт
        $contact=CRest::call(
            "crm.contact.add",
            [
                "fields"=>
                    [
                        'NAME'=>$data['NAME'],
                        'LAST_NAME'=>$data['LAST_NAME'],
                        "OPENED"=>"Y",
                        "TYPE_ID"=>"CLIENT",
                        "SOURCE_ID"=>"SELF",
                        "PHONE"=>[["VALUE"=>$data['PHONE'],"VALUE_TYPE"=>"WORK"]]
                    ],
            ]
        );
        $contactID=$contact['result'];
    }
    if ($stage=='NEW') {
        $ASSIGNED_BY_ID='1104'; //TODO тут не работает
    }elseif ($stage=='PREPARATION') {
        $ASSIGNED_BY_ID='1092';
    }elseif ($stage=='EXECUTING') {
        $ASSIGNED_BY_ID='14';
    }else {
        $ASSIGNED_BY_ID='1';
    }

    if ($data['DELIVERY_MODE']=='DELIVERY_LOCAL'&&$data['ADDRESS']) {
        $deliveryMode=$data['ADDRESS'];
    } //TODO тут подумать над регионами сайта
    elseif ($data['DELIVERY_MODE']=='DELIVERY_REGIONAL_TODOOR') {
        $deliveryMode='Kaspi доставка в регион';
    }elseif ($data['DELIVERY_MODE']=='DELIVERY_PICKUP'||!$data['ADDRESS']) {
        $deliveryMode='Самовывоз';
    }else {
        $deliveryMode=$data['ADDRESS'];
    }

    //создаем сделку
    $resultProduct=CRest::call(
        'crm.product.list',
        [
            'filter'=>[
                '>PRICE'=>0,
            ]
        ]
    );

    $productsInCRM=addProductToDeal($products, $kaspi);

    //Deal product
    $resultDeal=CRest::call(
        'crm.deal.add',
        [
            'fields'=>[
                "TITLE"=>$data['SOURCE']=='site'?'site #'.$data['CODE']:'kaspi #'.$data['CODE'],
                "STAGE_ID"=>$stage,
                "CONTACT_ID"=>$contactID,
                'ADDRESS'=>$data['ADDRESS'],
                'COMMENTS'=>$data['COMMENT'],
                'SOURCE_ID'=>$data['SOURCE']=='site'?'WEB':'7',
                'OPPORTUNITY'=>$data['PRICE'],
                'ASSIGNED_BY_ID'=>$ASSIGNED_BY_ID,
                'UF_CRM_1607174687783'=>$deliveryMode!='Самовывоз'?$data['DELIVERY_DATE']:'',
                'UF_CRM_1606219084'=>$data['PAYED'],
                'UF_CRM_1608367956755'=>$deliveryMode,
                'UF_CRM_1606656332154'=>$data['SOURCE']=='site'?
                    'https://leloo.kz/bitrix/admin/sale_order_view.php?ID='.$data['CODE']:
                    'https://kaspi.kz/merchantcabinet/#/orders/details/'.$data['CODE'],
                'UF_CRM_1608103552942'=>$data['ID'],
                'ORIGIN_ID'=>$data['ID']
            ]
        ]
    );
    if ($ID=$resultDeal['result']) {
        $rows=[];
        foreach ($productsInCRM as $key=>$pId) {
            array_push($rows,[
                'PRODUCT_ID'=>$key,
                'PRICE_EXCLUSIVE'=>$pId['PRICE'],
                'QUANTITY'=>$pId['QUANTITY']
            ]);
        }
        $result=CRest::call(
            'crm.deal.productrows.set',
            [
                'id'=>$ID,
                'rows'=>$rows
            ]
        );
    }else {
        echo 'error create deal';
        exit;
    }
}

//update deal on crm
function updDeal($id, $type, $stage){
    $getDial = CRest::call(
        "crm.deal.list",
        [
            'order'=> [ "ID"=> "ASC" ],
            'filter'=> [ "ORIGIN_ID"=> $id ],
            'select'=> [ "ID", "TITLE", "STAGE_ID", "PROBABILITY", "OPPORTUNITY", "CURRENCY_ID" ]
        ]
    );
    if ($getDial['total']>0){
        $result = CRest::call(
            'crm.deal.update',
            [
                "id" => $getDial['result'][0]['ID'],
                "fields" =>
                    [
                        "STAGE_ID" => $stage,
                        "UF_CRM_1606219084" => $type,
                    ]
            ]
        );
    }

}

//add product to CRM and Deal
function addProductToDeal($products, $kaspi=false){
    $productIds = array_keys($products);
    $prods = CRest::call(
        'crm.product.list',
        [
            'order'=> [ "NAME"=> "ASC" ],
            'filter'=> [ "XML_ID"=> $productIds ],
            'select'=> [ "ID", "XML_ID" ]
        ]
    );
    $t=[]; //temp array
    $crmProdIds=[]; //products founded from crm
    if ($prods['total']>0){
        foreach ($prods['result'] as $key=>$prod){
            $crmProdIds[intval($prod['ID'])]['PRICE'] = $products[$prod['XML_ID']]['PRICE'];
            $crmProdIds[intval($prod['ID'])]['QUANTITY'] = $products[$prod['XML_ID']]['QUANTITY'];
            array_push($t, $prod['XML_ID']);
        }
    }

    $prodsNotCrm = array_diff($productIds, $t);
    unset($t);
    if (count($prodsNotCrm)>0){
        sort($prodsNotCrm);
        if (!$kaspi){
            $arSelect = Array("ID", "NAME", "DETAIL_TEXT", "PREVIEW_PICTURE");
            $arFilter = Array("ID"=>$prodsNotCrm);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>100), $arSelect);

            $batch=[];
            $i=1;
            while($ob = $res->GetNextElement()){
                $batch['add_prod'.$i]=[
                    'method'=>'crm.product.add',
                    'params'=>[
                        "fields"=>
                            [
                                "NAME"=>$ob->fields['NAME'],
                                "DESCRIPTION"=>$ob->fields['~DETAIL_TEXT'],
                                "CURRENCY_ID"=>"KZT",
                                "XML_ID"=>$ob->fields['ID'],
                            ]
                    ]
                ];
                $i++;
            }
        }
        else{ //для товаров пришедших с каспи. Для них решено создавать новые товары не зависимо от тех что лежат в БУС
            $batch=[];
            $i=1;
            foreach ($prodsNotCrm as $item)
            {
                $batch['add_prod'.$i]=[
                    'method'=>'crm.product.add',
                    'params'=>[
                        "fields"=>
                            [
                                "NAME"=>$products[$item]['NAME'],
                                "CURRENCY_ID"=>"KZT",
                                "XML_ID"=>$item,
                            ]
                    ]
                ];
                $i++;
            }
        }
        $Batch = CRest::callBatch($batch);
        if(count($Batch['result']['result'])>0)
            $j=0;
            foreach ($Batch['result']['result'] as $res){
                $crmProdIds[$res]['PRICE'] = $products[$prodsNotCrm[$j]]['PRICE'];
                $crmProdIds[$res]['QUANTITY'] =$products[$prodsNotCrm[$j]]['QUANTITY'];
                $j++;
            }

//TODO поработать над intval всех переменных
    }
    return $crmProdIds;
}