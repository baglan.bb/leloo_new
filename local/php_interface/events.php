<?
use Bitrix\Main\EventManager;
use Bitrix\Sale;
require_once (__DIR__.'/crest.php');


class SvaEvents
{
    function OnBeforeUserRegisterHandler(&$arFields)
    {
        $arFields["PERSONAL_PHONE"] = '+'.NormalizePhone($_REQUEST['USER_PHONE']);
        $arFields["WORK_PHONE"] = '+'.NormalizePhone($_REQUEST['USER_PHONE']);


        //если пользователь регистрируется через личный кабинет
        if (isset($_POST['REGISTER_CABINET']) && $_POST['REGISTER_CABINET'] == 'Y'){
            $arFields["LOGIN"] = NormalizePhone($_REQUEST['USER_PHONE']);
            $arFields["EMAIL"] = NormalizePhone($_REQUEST['USER_PHONE']).'@bx.bx';
            if (isset($arFields["PASSWORD"]) && $arFields["PASSWORD"] == $_SESSION['SMS_CODE']){
                $arFields["CONFIRM_PASSWORD"] = $arFields["PASSWORD"];
            }
        }
    }
}

$eventManager = EventManager::getInstance();
$eventManager->addEventHandler(
    "main",
    "OnBeforeUserRegister",
    array("\SvaEvents", "OnBeforeUserRegisterHandler")
);


//если нет собачки в логине, то ноормализируем телефонный номер
AddEventHandler("main", "OnBeforeUserLogin", Array("MyClass", "OnBeforeUserLoginHandler"));

class MyClass
{
    // создаем обработчик события "OnBeforeUserLogin"
    function OnBeforeUserLoginHandler(&$arFields)
    {
        if (stripos($arFields["LOGIN"], '@') === false)
            $arFields["LOGIN"] = NormalizePhone($arFields["LOGIN"]);

        //если был запрос на сброс пароля
        if (isset($_POST['U_ID']) && $_POST['U_ID'] > 1){
            global $USER;
            session_start();
            $pass = str_replace('-','',$_REQUEST['USER_PASSWORD']);
            $user = new CUser;
            if ($_SESSION['SMS_CODE'] == $pass){
                $fields = Array(
                    "PASSWORD"          => $pass,
                    "CONFIRM_PASSWORD"  => $pass,
                );
                $user->Update($_POST['U_ID'], $fields);
                $strError .= $user->LAST_ERROR;
            }
        }

    }
}
//что бы немного облегчить жизнь бухгалтера, тут адрес собирается одной строкой
use Bitrix\Main;
//Main\EventManager::getInstance()->addEventHandler(
//    'sale',
//    'OnSaleOrderBeforeSaved',
//    'myFunction'
//);
//// вся эта штука должна была брать заказы, в названии которых есть набор и в идеале убирать из корзины товар с
//// названием набор, вместо него ложить товары которые были как раз частью этого набора что хранятся в свойствах
//// ну или на крайний случай, берет состов из свойства товара "набор" и пишет их названия в коммент к заказу
//function myFunction(Main\Event $event)
//{
////    $productID = 2421;
////    $quantity = 1;
////
////    if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog"))
////    {
////        if (IntVal($productID)>0)
////        {
////            Add2BasketByProductID(
////                $productID,
////                $quantity,
////                array()
////            );
////            $success = 1;
////        }
////    }
//
//
////    $sostavNabora = array();
////    $arID = array();
////    $arBasketItems = array();
////    $dbBasketItems = CSaleBasket::GetList(
////        array(
////            "NAME" => "ASC",
////            "ID" => "ASC"
////        ),
////        array(
////            "FUSER_ID" => CSaleBasket::GetBasketUserID(),
////            "LID" => SITE_ID,
////            "ORDER_ID" => "NULL"
////        ),
////        false,
////        false,
////        array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "PRODUCT_PROVIDER_CLASS")
////    );
////    while ($arItems = $dbBasketItems->Fetch())
////    {
////        if ('' != $arItems['PRODUCT_PROVIDER_CLASS'] || '' != $arItems["CALLBACK_FUNC"])
////        {
////            CSaleBasket::UpdatePrice($arItems["ID"],
////                $arItems["CALLBACK_FUNC"],
////                $arItems["MODULE"],
////                $arItems["PRODUCT_ID"],
////                $arItems["QUANTITY"],
////                "N",
////                $arItems["PRODUCT_PROVIDER_CLASS"]
////            );
////            $arID[] = $arItems["ID"];
////        }
////    }
////    if (!empty($arID))
////    {
////        $dbBasketItems = CSaleBasket::GetList(
////            array(
////                "NAME" => "ASC",
////                "ID" => "ASC"
////            ),
////            array(
////                "ID" => $arID,
////                "ORDER_ID" => "NULL"
////            ),
////            false,
////            false,
////            array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "NAME")
////        );
////        while ($arItems = $dbBasketItems->Fetch())
////        {
////            $arBasketItems[] = $arItems;
////        }
////    }
////    // Печатаем массив, содержащий актуальную на текущий момент корзину
////    foreach ($arBasketItems as $item){
////        if (stripos($item["NAME"], 'набор')>0){
////            $t = [];
////            $elements = [];
////            $db_props = CIBlockElement::GetProperty(11, $item["PRODUCT_ID"], array("sort" => "asc"), Array("CODE"=>"SOSTAV_NABORA"));
////            while($ar_props = $db_props->Fetch()){
////                array_push($elements, $ar_props["VALUE"]);
////            }
////
////            $order = ['SORT' => 'ASC'];
////            $filter = ['IBLOCK_ID' => 13, 'ID'=>$elements];
////            $rows = CIBlockElement::GetList($order, $filter);
////            unset($elements);
////            while ($row = $rows->fetch()) {
////                $row['PROPERTIES'] = [];
////                $elements[$row['ID']] =& $row;
////                unset($row);
////            }
////
////            CIBlockElement::GetPropertyValuesArray($elements, $filter['IBLOCK_ID'], $filter);
////            unset($rows, $filter, $order);
////
////            foreach ($elements as $el){
////                array_push($t, 'Название: '.$el["NAME"].' Ар.: '.$el["PROPERTIES"]["CML2_ARTICLE"]["VALUE"].' ШК.: '.$el["PROPERTIES"]["CML2_BAR_CODE"]["VALUE"]);
////            }
////        }
////    }
//
//    /** @var Order $order */
//    $order = $event->getParameter("ENTITY");
//    $propertyCollection = $order->getPropertyCollection();
//    foreach ($propertyCollection as $propertyItem){
//        switch ($propertyItem->getField('CODE')){
//            case 'LAST_NAME':
//                global $USER;
//                $name = $propertyItem->getField('VALUE');
//                $user = new CUser;
//                $fields = Array(
//                    "LAST_NAME"         => $name,
//                );
//                $user->Update($USER->GetID(), $fields);
//                break;
//            case 'LOCATION':
//                $_SESSION["USER_LOCATION"] = $propertyItem->getField('VALUE');
//                $arLocs = CSaleLocation::GetByID($_SESSION["USER_LOCATION"]);
//                $address .= $arLocs["CITY_NAME"];
//                break;
//            case 'STREET':
//                $address .= ', ул/мкр.: '.$propertyItem->getField('VALUE');
//                break;
//            case 'HOME':
//                $address .= '. д.: '.$propertyItem->getField('VALUE');
//                break;
//            case 'FLAT':
//                if (mb_strlen($propertyItem->getField('VALUE'))>0)
//                    $address .= '. кв/офис: '.$propertyItem->getField('VALUE');
//                break;
//            case 'ADDRESS':
//                $propertyItem->setField('VALUE', $address);
//                break;
//            case 'SOSTAV_NABORA':
//                $propertyItem->setField('VALUE', $t);
//                break;
//        }
//    }
//}


//пример использования события OnSaleOrderSaved
Main\EventManager::getInstance()->addEventHandler(
    'sale',
    'OnSaleOrderSaved',
    'FunctionOrderSaved'
);

//в обработчике получаем сумму, с которой планируются некоторые действия в дальнейшем:

function FunctionOrderSaved(Main\Event $event)
{
    /** @var Order $order */
    $order=$event->getParameter("ENTITY");
    $oldValues=$event->getParameter("VALUES");
    $isNew=$event->getParameter("IS_NEW");
    if ($isNew) {
        $currentOrder = [];
        $currentOrder['CODE'] = $currentOrder['ID']=$order->getField('ID');
        $currentOrder['PRICE']=$order->getPrice();
        $currentOrder['PAYED']='Не оплачен';
        $currentOrder['SOURCE']='site';

        //утро буднего дня планируемое время доставки
        if (date('w')>0 && date('w')<6 && date('H:m')<date('H:m', strtotime('9:30')))
            $deliveryDate=date('d.m.Y');
        elseif (date('w')<5)
            $deliveryDate=date('d.m.Y', strtotime('+1 day'));
        elseif (date('w')==5)
            $deliveryDate=date('d.m.Y', strtotime('+3 day'));
        elseif (date('w')==6)
            $deliveryDate=date('d.m.Y', strtotime('+2 day'));
        $currentOrder['DELIVERY_DATE']=$deliveryDate;

        if (CModule::IncludeModule("sale")&&CModule::IncludeModule("catalog")) {
            $basket=Sale\Order::load($currentOrder['ID'])->getBasket();

            foreach ($basket as $item) {
                $products[$item->getProductId()]['PRICE'] = $item->getPrice();
                $products[$item->getProductId()]['QUANTITY'] = $item->getQuantity();
            }

            $propertyCollection = $order->getPropertyCollection();
            foreach ($propertyCollection as $propertyItem){
                switch ($propertyItem->getField('CODE')){
                    case 'FIO':
                        $currentOrder['NAME'] = $propertyItem->getField('VALUE');
                        break;
                    case 'LAST_NAME':
                        global $USER;
                        $name = $propertyItem->getField('VALUE');
                        $currentOrder['LAST_NAME'] = $name;
                        $user = new CUser;
                        $fields = Array(
                            "LAST_NAME"         => $name,
                        );
                        $user->Update($USER->GetID(), $fields);
                        break;
                    case 'PHONE':
                        $currentOrder['PHONE'] = '+'.$propertyItem->getField('VALUE');
                        break;
                    case 'LOCATION':
                        $_SESSION["USER_LOCATION"] = $propertyItem->getField('VALUE');
                        $currentOrder["DELIVERY_MODE"] = $propertyItem->getField('VALUE')=='0000000278'?'DELIVERY_LOCAL':$propertyItem->getField('VALUE');
                        break; //TODO тут подумать над регионами сайта
                    case 'ADDRESS':
                        $currentOrder['ADDRESS'] = $propertyItem->getField('VALUE'); //TODO тут подумать над регионами сайта
                        break;
                }
            }
            addDealToCrm($currentOrder, "NEW", $products);
        }
    }
}

// Отключить поиск по описаниям товаров в Битрикс
AddEventHandler("search", "BeforeIndex", "BeforeIndexHandler");
function BeforeIndexHandler($arFields) {
    $arrIblock = array(11,14);
    $arDelFields = array("DETAIL_TEXT", "PREVIEW_TEXT") ;
    if (CModule::IncludeModule('iblock') && $arFields["MODULE_ID"] == 'iblock' && in_array($arFields["PARAM2"], $arrIblock) && intval($arFields["ITEM_ID"]) > 0){
        $dbElement = CIblockElement::GetByID($arFields["ITEM_ID"]) ;
        if ($arElement = $dbElement->Fetch()){
            foreach ($arDelFields as $value){
                if (isset ($arElement[$value]) && strlen($arElement[$value]) > 0){
                    $arFields["BODY"] = str_replace (CSearch::KillTags($arElement[$value]) , "", CSearch::KillTags($arFields["BODY"]) );
                }
            }
        }
        return $arFields;
    }
}

// Не перезаписывать детальное описание элементов из 1С
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate",  "dropName");
function dropName(&$arFields)
{
    if (@$_REQUEST['mode']=='import')
    {
//        AddMessage2Log($arFields['PROPERTY_VALUES']);
//        unset($arFields['NAME']); // Не перезаписывать названия элементов на сайте из 1С
        unset($arFields['DETAIL_TEXT']); // Не перезаписывать детальное описание элементов из 1С
        unset($arFields['PREVIEW_TEXT']);
        unset($arFields['ACTIVE']);
        unset($arFields['PROPERTY_VALUES'][593]);
        unset($arFields['PROPERTY_VALUES'][596]);
    }
}

\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    'sale',
    'onGenerateCoupon',
    function (\Bitrix\Main\Event $event) {
        return new \Bitrix\Main\EventResult(\Bitrix\Main\EventResult::SUCCESS, randCoupon());
    }
);

function randCoupon(){
    $code = rand(100000, 999999);
    return 'L'.$code;
}

AddEventHandler("main", "OnAfterUserRegister", "coupon");
function coupon(&$userId) {
    $coupon = rand(1,9).$userId['USER_ID'];
    $arCouponFields = array(
        "DISCOUNT_ID" => "49",
        "ACTIVE" => "Y",
        "ONE_TIME" => "O",
        "COUPON" => $coupon
    );

    $CID = new CCatalogDiscountCoupon();
    $CID->Add($arCouponFields);

    global $USER;
    $user = new CUser;
    $fields = Array(
        "UF_NOTIFY" => ['success&У вас есть купон на скидку$Вам доступен купон '.$coupon.' на 10%, используйте его при оформлении заказа-Y'],
    );
    $user->Update($USER->GetID(), $fields);
}

use Bitrix\Main\Loader;
use Bitrix\Sale\DiscountCouponsManager;
Loader::includeModule('sale');

\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    'sale',
    'onManagerCouponAdd',
    'handlerManagerCouponAdd'
);

function handlerManagerCouponAdd(\Bitrix\Main\Event $event)
{
    $fields = $event->getParameters();
    global $USER;

    $rsUser = CUser::GetByID($USER->GetID());
    $arUser = $rsUser->Fetch();

    foreach ($arUser["UF_NOTIFY"] as $key=>$n){
        if (strpos($n, $fields['COUPON'])>0)
            unset($arUser["UF_NOTIFY"][$key]);
    }

    $user = new CUser;
    $fields = Array(
        "UF_NOTIFY"       => $arUser["UF_NOTIFY"]
    );
    $user->Update($USER->GetID(), $fields);
}