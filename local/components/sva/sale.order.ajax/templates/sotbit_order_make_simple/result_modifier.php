<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */

use Sotbit\Origami\Helper\Config;

if (Config::get("IMAGE_FOR_OFFER") == 'PRODUCT' && $arResult["BASKET_ITEMS"] && $arResult['GRID']['ROWS'])
{
    $Basket = new \Sotbit\Origami\Image\Basket();
    $Basket->setMediumHeight(190);
    $Basket->setMediumWidth(190);
    $arProductID = [];

    foreach($arResult["BASKET_ITEMS"] as $arItem)
    {
        $arProductID[] = $arItem['PRODUCT_ID'];
    }

    $images = $Basket->getImages($arProductID);

    foreach($arResult['GRID']['ROWS'] as &$arRow)
    {
        $arRow['data'] = $Basket->changeImages($arRow['data'], $images[$arRow['data']['PRODUCT_ID']]);
    }
}
$user_name = $USER->GetFirstName();
$user_lastname = $USER->GetLastName();
if ($user_name)
    $arResult["ORDER_PROP"]["USER_PROPS_Y"][1]["VALUE"] = $user_name;
if ($user_lastname && strlen($arResult["ORDER_PROP"]["USER_PROPS_Y"][28]["VALUE"])<1)
    $arResult["ORDER_PROP"]["USER_PROPS_Y"][28]["VALUE"] = $user_lastname;
$address = [
    'ID'=>7,
    'NAME'=>'Адрес',
    'TYPE'=>'TEXT',
    'REQUIRED'=>'Y',
    'FIELD_NAME'=>'ORDER_PROP_7',
    'IS_ADDRESS'=>'Y',
];
$password = [
    'ID'=>4,
    'NAME'=>'Пароль из СМС',
    'SORT'=>122,
    'TYPE'=>'TEXT',
    'REQUIRED'=>'Y',
    'FIELD_NAME'=>'password',
    'USER_PROPS'=>'Y',
    'PROPS_GROUP_ID'=>1,
    'GROUP_NAME' => 'Личные данные'
];
$btn = [
    'ID'=>90,
    'NAME'=>'Не получили СМС? Отправить повторно',
    'REQUIRED'=>'N',
    'FIELD_NAME'=>'btn',
    'IS_BTN'=>'Y'
];
//$arResult["ORDER_PROP"]["USER_PROPS_Y"][7] = $address;
$arResult["ORDER_PROP"]["USER_PROPS_Y"][90] = $btn;
if (!$USER->IsAuthorized())
    $arResult["ORDER_PROP"]["USER_PROPS_Y"][4] = $password;
//$arResult["ORDER_PROP"]["USER_PROPS_Y"][2] = [
//                                                'ID'=>2,
//                                                'NAME'=>'email',
//                                                'REQUIRED'=>'N',
//                                                'IS_EMAIL'=>'Y',
//                                                'FIELD_NAME'=>'ORDER_PROP_2',
//                                                'FIELD_ID'=>'ORDER_PROP_EMAIL',
//                                                'VALUE'=>$arResult["ORDER_PROP"]["USER_PROPS_Y"][3]['VALUE'].'@temp.ru'
//                                            ];;



$arResult["PAY_SYSTEM"][1]["CHECKED"] = NULL;