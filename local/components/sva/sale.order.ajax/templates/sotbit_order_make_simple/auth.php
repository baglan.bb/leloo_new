<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<script>
    function ChangeGenerate(val)
    {
        if(val)
        {
            document.getElementById("sof_choose_login").style.display='none';
        }
        else
        {
            document.getElementById("sof_choose_login").style.display='block';
            document.getElementById("NEW_GENERATE_N").checked = true;
        }

        try{document.order_reg_form.NEW_LOGIN.focus();}catch(e){}
    }
</script>

<div class="col-12 col-md-4" id="orderAuth">
	<label for="userPhone">Введите ваш номер телефона</label>
	<div class="input-group mb-3">
		<div class="input-group-prepend">
			<span class="input-group-text" id="basic-addon1">
				<i class="fa fa-phone"></i>
			</span>
		</div>
		<input type="tel" class="form-control js-phone" id="userPhone" placeholder="Номер телефона" autocomplete="off">
	</div>
	<button type="button" disabled class="btn btn-danger col-12 mb-5" id="checkUser">Далее</button>

	<div style="display: none">
		<label for="password">Пароль</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control passMask" id="password" placeholder="Пароль" autocomplete="off" aria-label="Username" aria-describedby="basic-addon1">
			<div class="input-group-append" id="resend" data-action="forgotPass" style="cursor: pointer">
				<span class="input-group-text">Забыли пароль?</span>
			</div>
		</div>
		<div class="d-none nameFields">
			<label for="userName">Ваше имя</label>
			<div class="input-group mb-3">
				<input type="text" class="form-control" id="userName" placeholder="Имя" autocomplete="off">
			</div>
		</div>
<!--		<div class="d-none nameFields">-->
<!--			<label for="userLastName">Ваша фамилия</label>-->
<!--			<div class="input-group mb-3">-->
<!--				<input type="text" class="form-control" id="userLastName" placeholder="Фамилия" autocomplete="off">-->
<!--			</div>-->
<!--		</div>-->
		<input type="text" id="userID" hidden>
		<button type="button" disabled class="btn btn-danger col-12 mb-5" id="goAuth" data-url="/ajax_auth/auth.php">Продолжить <i class="fa-li fa fa-spinner fa-spin orderLoader"></i></button>
	</div>
</div>


<?php $telMask = \Sotbit\Origami\Config\Option::get('MASK',SITE_ID); ?>
<script>
  $('.js-phone').inputmask("<?= $telMask ?>");
  // $('.passMask').inputmask("999-999");
</script>