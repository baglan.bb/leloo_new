<?

if(is_string($arResult["ERROR_MESSAGE"]))
{
    $arResult["ERROR_MESSAGE"] = htmlspecialchars_decode($arResult["ERROR_MESSAGE"]);
}
elseif(is_array($arResult["ERROR_MESSAGE"]))
{
    foreach ($arResult["ERROR_MESSAGE"] as &$v)
    {
        $v = htmlspecialchars_decode($v);
    }
}