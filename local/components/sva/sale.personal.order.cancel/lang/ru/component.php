<?
$MESS['SALE_MODULE_NOT_INSTALL']="Модуль Интернет-магазин не установлен.";
$MESS['SALE_ACCESS_DENIED']="Для отмены заказа необходимо авторизоваться.";
$MESS["SPOC_TITLE"]="Отмена заказа №#ID#";
$MESS["SPOC_NO_ORDER"]="Заказ №#ID# не найден.";
$MESS['SPOC_ORDER_CANCELED'] = "Заказ №#ACCOUNT_NUMBER# отменен";
$MESS['SPOC_CANCEL_ORDER'] = "Для отмены оплаченного заказа ознакомьтесь с правилами <a href='/help/return/'>Возврата товара</a> и свяжитесь с оператором <a href='tel:+77000203312'>+ 7&nbsp;700&nbsp;020&nbsp;33&nbsp;12</a>";
?>