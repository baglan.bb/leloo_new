<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<? if(!$arParams['AUTH']):?>
	<? $uri = str_replace('forgot','',urldecode(POST_FORM_ACTION_URI));?>
	<form name="smsvalid" action="<?=$uri?>" method="POST" enctype="application/x-www-form-urlencoded">
		<div class="aBlock">
			<div class="input">
				<label><?=GetMessage("SMSC_SMS_VVEDITE_SVOY_NOMER_T")?></label>
			</div>
			<input type="text" class="text" name="PHONE_NUMBER" value="<?=$arParams["PHONE_NUMBER"]?>"><br/>
			<? if($arResult['VALIDATION']['SHOW_PASS'] && $arResult['VALIDATION']['USER_GOT'] && !$arParams['FORGOT']):?>
				<div class="input passwd">
					<label><?=GetMessage("SMSC_SMS_VVEDITE_PAROLQ")?></label>
				</div>
				<input type="text" class="text" name="CODE"><br/>
			<? elseif($arResult['VALIDATION']['SHOW_PASS']):?>
				<div class="input passwd">
					<label><?=GetMessage("SMSC_SMS_VAM_VYSLAN_PAROLQ_PO")?> SMS:</label>
				</div>
				<input type="text" class="text" name="CODE"><br/>
			<? endif;?>
			<? if($arResult['VALIDATION']['USER_GOT']):?>
				<? $forgot = (strpos(POST_FORM_ACTION_URI,'?') === false)?POST_FORM_ACTION_URI.'?forgot&PHONE_NUMBER='.$arParams["PHONE_NUMBER"]:POST_FORM_ACTION_URI.'&forgot&PHONE_NUMBER='.$arParams["PHONE_NUMBER"];?>
				<p><a href="<?=$forgot?>"><?=GetMessage("SMSC_SMS_ZABYLI_SVOY_PAROLQ")?></a></p>
			<? endif;?>
			<div class="inputn">
				<input type="submit" name="save" value=".">
				<div>
					<span class="web web2"><?=$arParams['CHECK']?></span>
				</div>
			</div>
		</div>
	</form>
<? else:?>
<?=GetMessage("SMSC_SMS_VY_AVTORIZOVANY")?><? endif;?>
<?# debmes($arResult)?>