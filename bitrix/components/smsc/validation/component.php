<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
// ���� �� ���������� ������ 
if (!CModule::IncludeModule("smsc.sms"))
{
	ShowError(GetMessage("SMSC_SMS_MODULE_NOT_INSTALLED"));
	return;
}

// �������� ���������
$arParams["PHONE_NUMBER"]	= trim($arParams["PHONE_NUMBER"]);
$arParams["MIN_PASS"]		= intval($arParams["MIN_PASS"]);
$arParams["MAX_PASS"]		= intval($arParams["MAX_PASS"]);
$arParams["SMS"]		= trim($arParams["SMS"]);
$arParams["TIME"]		= intval($arParams["TIME"])*60;
$arParams['CODE']		= (isset($_REQUEST['CODE']))?intval($_REQUEST['CODE']):0;
$arParams['BACKURL']		= (isset($_REQUEST['backurl']) && strlen(urldecode($_REQUEST['backurl']))>0)?urldecode($_REQUEST['backurl']):'';

$arResult['VALIDATION'] = array(
	'VALID'		=> false,
	'CODE'		=> (isset($_SESSION['SMSC_VALIDATION']['CODE']) && strlen($_SESSION['SMSC_VALIDATION']['CODE'])>0 && !isset($_REQUEST['reset']))?$_SESSION['SMSC_VALIDATION']['CODE']:'',
	'LAST_TIME'	=> (intval($_SESSION['SMSC_VALIDATION']['LAST_TIME'])<=0 || isset($_REQUEST['reset']))?time():$_SESSION['SMSC_VALIDATION']['LAST_TIME']
);
$time_last = intval(time() - $arResult['VALIDATION']['LAST_TIME']);
$arResult['VALIDATION']['TIME_OUT'] = ($time_last > $arParams["TIME"] && $arParams["TIME"]!=0)?true:false;

if($arParams["MAX_PASS"] > $arParams["MIN_PASS"] && SMSC_Send::CheckPhoneNumber($arParams["PHONE_NUMBER"])
   && ($time_last > $arParams["TIME"] || $arParams["TIME"]==0 || $time_last <= 1)
   && !$arParams['CODE']
   && !$arResult['VALIDATION']['CODE']
   ) {
	
	$code = rand($arParams["MIN_PASS"],$arParams["MAX_PASS"]);
	$arResult['VALIDATION']['CODE'] = $code;
	$message = str_replace('#CODE#',$code,$arParams["SMS"]);
	$sms = new SMSC_Send;
	$sms->Send_SMS($arParams["PHONE_NUMBER"],$message);
	
}

if($arParams['CODE'] && $arParams['CODE'] == $arResult['VALIDATION']['CODE']) {
	$arResult['VALIDATION']['VALID'] = true;
	if($arParams['BACKURL']) LocalRedirect($arParams['BACKURL']);
}
$_SESSION['SMSC_VALIDATION'] = $arResult['VALIDATION'];

// ���������� ������
$this->IncludeComponentTemplate();

?>