<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule("sale");
if(!CModule::IncludeModule("sotbit.orderphone"))
{
    ShowError(GetMessage("SOP_MODULE_NOT_INSTALL"));
    return;
}

if ($this->StartResultCache())
{
    /* Displayed and required fields order form */
    if (!in_array('PHONE', $arParams['REQUIRED_FIELDS']))
        array_push($arParams['REQUIRED_FIELDS'], 'PHONE');
    foreach ($arParams['DISPLAYED_FIELDS'] as $key => $value) {
        $arResult['DISPLAY'][] = $value;
    }
    foreach ($arParams['REQUIRED_FIELDS'] as $key => $value) {
        $arResult['REQUIRE'][] = $value;
        if (!in_array($value, $arResult['DISPLAY'])) {
            $arResult['DISPLAY'][] = $value;
        }
    }

    /* Move Comment field to the end of the form */
    $key = array_search('COMMENT', $arResult['DISPLAY']);
    if($key !== false) {
        unset($arResult['DISPLAY'][$key]);
    }
    $arResult['DISPLAY'][] = 'COMMENT';

    if ($arParams['SMS_CONFIRM'] == true) {
        $arResult['SMS_CONFIRM'] = true;
    }

    if(isset($arParams["ORDER_PROPS"]) && !empty($arParams["ORDER_PROPS"]))
    {
        $arFilter["ID"] = $arParams["ORDER_PROPS"];
        $dbOrderProps = CSaleOrderProps::GetList(
            array("SORT" => "ASC"),
            $arFilter,
            false,
            false,
            array("ID", "NAME")
        );

        while ($arOrderProps = $dbOrderProps->Fetch())
        {
            $arResult["ORDER_PROPS"][$arOrderProps["ID"]] = $arOrderProps;
        }
    }
    if(isset($arParams["DELIVERY_ID"]) && $arParams["DELIVERY_ID"]>0 && isset($arParams["PAY_SYSTEM_ID"]) && $arParams["PAY_SYSTEM_ID"]>0)
    {
        if($arParams['USE_CAPTCHA'] == 'Y'){
            $arResult['CAPTCHACode'] = $APPLICATION->CaptchaGetCode();
            $arResult['isUseCaptcha'] = 'Y';
        }
        $this->IncludeComponentTemplate();
    }
}
?>
