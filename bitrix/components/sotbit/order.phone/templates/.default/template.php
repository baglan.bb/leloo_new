<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Sotbit\Origami\Helper\Config;
use Bitrix\Main\Page\Asset;

if (!CModule::IncludeModule("sotbit.orderphone") || !CSotbitOrderphone::GetDemo()) return;
\Bitrix\Main\Page\Asset::getInstance()->addJs($templateFolder . "/js/jquery.maskedinput.min.js");
Asset::getInstance()->addJs(SITE_DIR . "local/templates/.default/components/sotbit/order.phone/origami_default/script.js");
Asset::getInstance()->addCss(SITE_DIR . "local/templates/.default/components/sotbit/order.phone/origami_default/style.css");
?>

<div class="sotbit_order_phone_wrapper buy-in-click">
    <div class="sotbit_order_phone">
        <div class="sotbit_order_phone__title"><?= Loc::getMessage('OK_TITLE') ?></div>
        <form class="sotbit_order_phone_form">

            <div class="sotbit_order_success">
                <div class="popup-window-message-content">
                    <svg class="popup-window-icon-check">
                        <use xlink:href="/local/templates/sotbit_origami/assets/img/sprite.svg#icon_check_form"></use>
                    </svg>
                    <div>
                        <div class="popup-window-message-title"><?= GetMessage('OK_THANKS') ?></div>
                        <div style="font-size: 16px;"><?= GetMessage('OK_SUCCESS') ?></div>
                    </div>
                </div>
            </div>

            <div class="hide-on-success">

                <div class="popup_resizeable_content">

                    <div class="popup-error-message">
                        <div class="sotbit_order_error"></div>
                    </div>
                    <? if (empty($arResult["FORM_NOTE"])) : ?>
                        <? foreach ($arParams as $param => $val):
                            if (strpos($param, "~") !== false || is_array($val)) continue;
                            ?>
                            <input type="hidden" name="<?= $param ?>" value="<?= $val ?>"/>
                        <? endforeach ?>

                        <? foreach ($arResult['DISPLAY'] as $field): ?>
                            <div class="sotbit_order_phone__block">
                                <p class="sotbit_order_phone__block_title">
                                    <?= Loc::getMessage('OK_' . $field) ; if (in_array($field, $arResult['REQUIRE'])) echo '<span class="star">  *</span>'?></p>
                                <? if ($field != 'COMMENT'): ?>
                                    <input type="text" name="order_<?=mb_strtolower($field)?>" value="<?= $arResult['USER'][$field] ?>"
                                        <? if (in_array($field, $arResult['REQUIRE'])) echo 'data-req-'.$field.'="y"' ?>/>
                                <? else : ?>
                                    <textarea name="order_comment" <? if (in_array($field, $arResult['REQUIRE'])) echo 'data-req-'.$field.'="y"' ?>></textarea>
                                <? endif ?>
                            </div>
                        <? endforeach ?>

                        <? if ($arResult["isUseCaptcha"] == "Y") {
                            ?>
                            <div class="sotbit_order_phone__block">
                                <div class="feedback_block__captcha">
                                    <p class="popup-window-field_description"><?= GetMessage('CAPTCHA_TITLE') ?>
                                    </p>
                                    <div class="feedback_block__captcha_input">
                                        <input type="text" name="captcha_word" size="30" maxlength="50" value=""
                                               required/>
                                    </div>
                                    <div class="feedback_block__captcha_img">
                                        <input type="hidden" name="captcha_sid"
                                               value="<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>"/>
                                        <img
                                                src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>"/>
                                        <div class="captcha-refresh"  onclick="reloadCaptcha(this,'<?= SITE_DIR ?>');return false;">
                                            <svg class="icon_refresh" width="16" height="14">
                                                <use
                                                        xlink:href="/local/templates/sotbit_origami/assets/img/sprite.svg#icon_refresh"></use>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?
                        }
                        ?>

                        <div class="confidential-field">
                            <input type="checkbox" id="UF_CONFIDENTIAL" name="UF_CONFIDENTIAL" class="checkbox__input"
                                   checked="checked">
                            <label for="UF_CONFIDENTIAL" class="checkbox__label fonts__middle_comment">
                                <?= Loc::getMessage('OK_CONFIDENTIAL', ['#CONFIDENTIAL_LINK#' => Config::get('CONFIDENTIAL_PAGE',
                                    $arParams['SITE_ID'])]) ?>
                            </label>
                        </div>


                        <div class="popup-window-submit_button">
                            <input type="submit" name="submit" value="<?= Loc::getMessage('OK_SEND') ?>"/>
                        </div>
                    <? endif; ?>

                </div>

            </div>

        </form>
    </div>
</div>
<script src="<?= $this->__component->__template->__folder ?>/js/jquery.maskedinput.min.js"></script>
<script>
    $(function () {
        $(".sotbit_order_phone").on("submit", "form", submitOrderPhone);

        maska = $(".sotbit_order_phone form input[name='TEL_MASK']").eq(0).val();
        maska = $.trim(maska);
        if (maska != "") $(".sotbit_order_phone form input[name='order_phone']").mask(maska, {placeholder: "_"});

        function submitOrderPhone(e) {
            e.preventDefault();
            let name = $(this).find("input[name='order_name']").val();
            let email = $(this).find("input[name='order_email']").val();
            let fio = $(this).find("input[name='order_fio']").val();
            let zip = $(this).find("input[name='order_zip']").val();
            let city = $(this).find("input[name='order_city']").val();
            let prop_location = $(this).find("input[name='order_location']").val();
            let address = $(this).find("input[name='order_address']").val();
            let company = $(this).find("input[name='order_company']").val();
            let company_adr = $(this).find("input[name='order_company_adr']").val();
            let inn = $(this).find("input[name='order_inn']").val();
            let kpp = $(this).find("input[name='order_kpp']").val();
            let contact_person = $(this).find("input[name='order_contact_person']").val();
            let fax = $(this).find("input[name='order_fax']").val();
            let comment = $(this).find("textarea[name='order_comment']").val();

            v = $(this).find("input[name='TEL_MASK']").val();
            v = $.trim(v);
            req = strReplace(v);
            let _this = $(this);
            v = $(this).find("input[name='order_phone']").val();

            $(this).find('input').removeClass('error');
            $(this).find('.checkbox__label').removeClass('error');
            $(this).find(".sotbit_order_error").hide();
            $(this).find(".sotbit_order_timedate").hide();
            $(this).find(".sotbit_order_success").hide();


            let error = false;

            let reqName = this.querySelector('[data-req-name]');
            if (reqName) {
                if (name.length <= 0) {
                    $(this).find("input[name='order_name']").addClass('error');
                    error = true;
                }
            }

            let reqFio = this.querySelector('[data-req-fio]');
            if (reqFio) {
                if (fio.length <= 0) {
                    $(this).find("input[name='order_fio']").addClass('error');
                    error = true;
                }
            }

            let reqZip = this.querySelector('[data-req-zip]');
            if (reqZip) {
                if (zip.length <= 0) {
                    $(this).find("input[name='order_zip']").addClass('error');
                    error = true;
                }
            }

            let reqCity = this.querySelector('[data-req-city]');
            if (reqCity) {
                if (city.length <= 0) {
                    $(this).find("input[name='order_city']").addClass('error');
                    error = true;
                }
            }

            let reqLocation = this.querySelector('[data-req-location]');
            if (reqLocation) {
                if (prop_location.length <= 0) {
                    $(this).find("input[name='order_location']").addClass('error');
                    error = true;
                }
            }

            let reqAddress = this.querySelector('[data-req-address]');
            if (reqAddress) {
                if (address.length <= 0) {
                    $(this).find("input[name='order_address']").addClass('error');
                    error = true;
                }
            }

            let reqCompany = this.querySelector('[data-req-company]');
            if (reqCompany) {
                if (company.length <= 0) {
                    $(this).find("input[name='order_company']").addClass('error');
                    error = true;
                }
            }

            let reqCompany_adr = this.querySelector('[data-req-company_adr]');
            if (reqCompany_adr) {
                if (company_adr.length <= 0) {
                    $(this).find("input[name='order_company_adr']").addClass('error');
                    error = true;
                }
            }

            let reqInn = this.querySelector('[data-req-inn]');
            if (reqInn) {
                if (inn.length <= 0) {
                    $(this).find("input[name='order_inn']").addClass('error');
                    error = true;
                }
            }

            let reqKpp = this.querySelector('[data-req-kpp]');
            if (reqKpp) {
                if (kpp.length <= 0) {
                    $(this).find("input[name='order_kpp']").addClass('error');
                    error = true;
                }
            }

            let reqContact_person = this.querySelector('[data-req-contact_person]');
            if (reqContact_person) {
                if (contact_person.length <= 0) {
                    $(this).find("input[name='order_contact_person']").addClass('error');
                    error = true;
                }
            }

            let reqFax = this.querySelector('[data-req-fax]');
            if (reqFax) {
                if (fax.length <= 0) {
                    $(this).find("input[name='order_fax']").addClass('error');
                    error = true;
                }
            }


            if (v.search(req) == -1 || v.length <= 0) {
                $(this).find("input[name='order_phone']").addClass('error');
                error = true;
            }
            if ($(this).find("input[name='UF_CONFIDENTIAL']:checked").length == 0) {
                $(this).find('.checkbox__label').addClass('error');
                error = true;
            }

            let reqEmail = this.querySelector('[data-req-email]');
            if (reqEmail) {
                let pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
                if (!pattern.test(email)) {
                    $(this).find("input[name='order_email']").addClass('error');
                    error = true;
                }
            }

            let reqComment = this.querySelector('[data-req-comment]');
            if (reqComment) {
                if (comment.length <= 0) {
                    $(this).find("textarea[name='order_comment']").addClass('error');
                    error = true;
                }
            }

            if (!error) {
                $(this).find("input[type='text']").removeClass("red");
                ser = $(this).serialize();
                userType = 0;

                BX.ajax.post("/bitrix/components/sotbit/order.phone/ajax.php", ser, function (data) {
                    data = $.trim(data);
                    if (data.indexOf("SUCCESS") >= 0) {
                        _this.find(".sotbit_order_success").show();
                        _this.find(".sotbit_order_error").hide();
                        _this.find(".hide-on-success").hide();
                        id = data.replace("SUCCESS", "").split(',')[0];
                        userPassword = data.replace("SUCCESS", "").split(',')[1];
                        localHref = $('input[name="LOCAL_REDIRECT"]').val();
                        orderID = $('input[name="ORDER_ID"]').val();
                        if (userPassword.length > 0) {
                            _this.find(".sotbit_order_timedate").show();
                            _this.find(".time_date_login").text('<?=Loc::getMessage('FORM_LOGIN'); ?> ' + data.replace("SUCCESS", "").split(',')[2]);
                            _this.find(".time_date_pass").text('<?=Loc::getMessage("FORM_PASSWORD"); ?> ' + userPassword);
                        }

                        if (typeof (localHref) != "undefined" && localHref != "") {
                            location.href = localHref + "?" + orderID + "=" + id;
                        }
                    } else {
                        _this.find(".sotbit_order_success").hide();
                        _this.find(".sotbit_order_timedate").hide();
                        _this.find(".sotbit_order_error").show().html(data);
                    }
                });
            }
        }

        function strReplace(str) {
            str = str.replace("+", "\\+");
            str = str.replace("(", "\\(");
            str = str.replace(")", "\\)");
            str = str.replace(/[0-9]/g, "[0-9]{1}");
            return new RegExp(str, 'g');
        }
    });
</script>
<script>
    ;(function resizeOrderPhonePopup() {
        let wrapper,
            wrappers = document.querySelectorAll(".wrap-popup-window");

        for (let i = 0; i < wrappers.length; i++) {
            if (wrappers[i].querySelector(".popup_resizeable_content")) {
                wrapper = wrappers[i];
                break;
            }
        }

        let popupResizeableContent = wrapper.querySelector(".popup_resizeable_content"),
            popupWindow = wrapper.querySelector(".popup-window"),
            popupContent = wrapper.querySelector(".popup-content"),
            popupTitle = wrapper.querySelector(".sotbit_order_phone__title"),
            submitBtn = wrapper.querySelector(".popup-window-submit_button");

        resizePopupContent();
        putTitleShadow();
        setUpListeners();

        function setUpListeners() {
            popupResizeableContent.addEventListener("scroll", putTitleShadow);

            window.addEventListener("resize", () => {
                resizePopupContent();
                putTitleShadow();
            });

            popupContent.addEventListener("load", () => {
                resizePopupContent();
                putTitleShadow();
            });

            submitBtn.addEventListener("click", () => {
                resizePopupContent();
                putTitleShadow();
            });
        }

        function resizePopupContent() {
            let clientHeight = document.documentElement.clientHeight * 0.97,
                newHeight;

            popupResizeableContent.style.overflowY = "hidden";
            popupResizeableContent.style.height = "auto";

            if ((popupContent.clientHeight > (popupWindow.clientHeight + 2)) || (popupContent.clientHeight > clientHeight)) {

                newHeight = (clientHeight < popupWindow.clientHeight) ?
                    (clientHeight - popupTitle.clientHeight) :
                    (popupWindow.clientHeight - popupTitle.clientHeight);

                popupResizeableContent.style.overflowY = "auto";
                popupResizeableContent.style.height = newHeight + "px";
            }
        }

        function putTitleShadow() {
            let scrolled = popupResizeableContent.scrollTop;

            if (scrolled === 0) {
                popupTitle.style.boxShadow = "none";
            } else {
                popupTitle.style.boxShadow = "0 2px 5px 3px rgba(0,0,0,.1)";
            }
        }

    })();
</script>
