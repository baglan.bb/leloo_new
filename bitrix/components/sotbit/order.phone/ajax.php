<?
define("NO_KEEP_STATISTIC", true);
define("STOP_STATISTICS", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("main");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");
CModule::IncludeModule("sotbit.orderphone");
CModule::IncludeModule("form");

global $APPLICATION;
$request = $_REQUEST;

/* Convert encoding */
foreach ($request as &$value) {
    $value = \Bitrix\Main\Text\Encoding::convertEncodingToCurrent($value);
}

/* Phone confirmation */
if ($request['SMS_CONFIRM'] == 'Y' && !empty($request['order_phone']) && $request['PHONE_CHECKED'] == 'N') {

    if (empty($_SESSION['SMS_ATTEMPT']))
        $_SESSION['SMS_ATTEMPT'] = 0;

    if ($request['SMS_RESEND'] == 'Y' || $_SESSION['SMS_ATTEMPT'] < 1) {
        if ($_SESSION['SMS_ATTEMPT'] < 3) {
            $error_message = \CSotbitOrderphone::sendSMSCode($request['order_phone']);
            echo ($error_message) ? $error_message[0] : 'sent';
        } else {
            echo 'exceeded';
        }
    } else {
        if (!empty($request['SMS_CODE'])) {
            $confirm = false;
            $request_hash = md5($_SESSION['fixed_session_id'] . trim($request['SMS_CODE']));
            foreach ($_SESSION['SMS_HASH'] as $hash) {
                if ($hash === $request_hash) $confirm = true;
            }
            echo ($confirm) ? 'confirm' : 'wrongcode';
        } else {
            echo 'wrongcode';
        }
    }
    exit;
}

/* Clear sms vars after form closed */
if ($request['SMS_CLEAR'] == 'Y') {
    unset($_SESSION['SMS_ATTEMPT']);
    unset($_SESSION['SMS_HASH']);
    exit;
}

foreach($request as $id=>$val) {
    $arParams[$id] = $val;
}

$fields = [];

$arFilter = array("PERSON_TYPE_ID" => $arParams["PERSON_TYPE"]);

$dbOrderProps = CSaleOrderProps::GetList(
    array("SORT" => "ASC"),
    $arFilter,
    false,
    false,
    array("ID", "CODE")
);

while ($arOrderProps = $dbOrderProps->Fetch()) {
    if ($arOrderProps["ID"] == $arParams["ORDER_NAME_PROP"])
        $fields['NAME'] = $request["order_" . mb_strtolower($arOrderProps["CODE"])];

    $arProps[] = $arOrderProps["CODE"];
}

foreach ($arProps as $prop) {
    $lowerProp = mb_strtolower($prop);
    if (!empty($request["order_" . $lowerProp])) {
        $fields[$prop] = $request["order_" . $lowerProp];
    }
}

if (isset($request["order_comment"])) {
    $fields['COMMENT'] = $request["order_comment"];
}
if (isset($request["captcha_sid"])) {
    $fields['CAPTCHA_SID'] = $request["captcha_sid"];
}
if (isset($request["captcha_word"])) {
    $fields['CAPTCHA_WORD'] = $request["captcha_word"];
}


if($fields && count($arParams)>0) {
    if (
        $arParams['USE_CAPTCHA'] == 'Y' &&
        !$APPLICATION->CaptchaCheckCode($fields['CAPTCHA_WORD'], $fields['CAPTCHA_SID'])
    ) {
        echo GetMessage("FORM_WRONG_CAPTCHA");
        die();
    }
    $order = new CSotbitOrderphone($arParams, $fields);
    $arUserDate = $order->StartAjax();
    if (empty($order->error)) {
        echo "SUCCESS".$order->orderID . ',' . $arUserDate['PASSWORD'] . ',' . $arUserDate['LOGIN'];
        $arEventFields = array("SUBJECT" => "Заказ купить в 1 клик", "ORDER_ID" => $order->orderID);
        CEvent::SendImmediate("NEW_ORDER_POSTPAY", "s1", $arEventFields);
    } else {
        echo $order->error[0];
    }
}
?>
