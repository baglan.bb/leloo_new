<?php require $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include.php';
require '../../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$post=false;
if(isset($_POST["dateFrom"]) && isset($_POST["dateTo"])){
	$post=true;
    $date = new DateTime($_POST["dateFrom"]." 00:00:00");
    $date = $date->format('d.m.Y H:i:s');

	$dateTo= new DateTime($_POST["dateTo"]." 00:00:00");
    $dateTo = $dateTo->format('d.m.Y H:i:s');
    $strTime = strtotime($date)*1000;
    $strTimeTo = strtotime($dateTo)*1000;
}
else{
    if (date('G') < 9 && date('w')==1)
        $day = date('j')-3;
    elseif (date('G') < 9)
        $day = date('j')-1;
    else
        $day = date('j');
    $date = $day.'.'.date('m.Y')." 11:00:00+6";


    $dayK = date('j')-13;
        $m = date('m');
        $Y = date('Y');
        if ($dayK < 1){
            $dayK = 31+$dayK;
            $m=date('m')-1;
            if ($m<1){
                $m=12;
                $Y--;
            }
        }

    $dateK = $dayK.'.'.$m.'.'.$Y." 00:00:00+6";
    $strTime = strtotime($dateK)*1000;

}


if (CModule::IncludeModule("sale")):
	if(!$post)
		$arFilter = Array(
			">=DATE_INSERT" => $date,
			"ALLOW_DELIVERY" => "Y",
			"DELIVERY_ID" => 1,
		);
	else
        $arFilter = Array(
            ">=DATE_INSERT" => $date,
            "<=DATE_INSERT" => $dateTo,
            "PAYED" => "Y",
        );
    $rsSales = CSaleOrder::GetList(array("DATE_INSERT" => "DESC"), $arFilter, false, false, '*');


    //Создаем экземпляр класса электронной таблицы
    $spreadsheet = new Spreadsheet();

    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    $spreadsheet = $reader->load("temp.xlsx");

    //Получаем текущий активный лист
    $sheet = $spreadsheet->getActiveSheet();

    $i = 8;
    while ($arSales = $rsSales->Fetch())
    {
        $arFilter =Array (
            "ORDER_ID" => $arSales["ID"],
            "ORDER_PROPS_ID" => array(6,3,7),
        );
        $db_sales_ord = CSaleOrderPropsValue::GetList(array(), $arFilter);
        $continue = false;
        while ($ar_salesord = $db_sales_ord->Fetch())
        {
            switch ($ar_salesord["ORDER_PROPS_ID"]){
                case 6:
                    if ($ar_salesord["VALUE"] != 3){
                        $continue = true;
                        break 2;
                    }
                    break;
                case 3:
                    $phone = $ar_salesord["VALUE"];
                    break;
                case 7:
                    $address = $ar_salesord["VALUE"];
                    break;
            }
        }
        
        if ($continue)
            continue;

        $sheet->setCellValue('A'.$i, $i-7);
        $sheet->setCellValue('B'.$i, $arSales["DATE_PAYED"]);
//        $sheet->setCellValue('C'.$i, date('d.m.Y',strtotime($arSales["DATE_PAYED"] . "+1 days")));
        $sheet->setCellValue('D'.$i, 'Сайт');
        $sheet->setCellValue('E'.$i, $arSales["ID"]);
        $sheet->setCellValue('F'.$i, $arSales["USER_NAME"].' '.$arSales["USER_LAST_NAME"]);
        $sheet->setCellValue('G'.$i, ($arSales["PAYED"]=='Y')?'Оплачен':'Оплата при получении');
        $sheet->setCellValue('H'.$i, $arSales["PRICE"].' тг');
        $sheet->setCellValue('I'.$i, ($arSales["PAYED"]=='Y')?'Предоплата':'');
        $sheet->setCellValue('J'.$i, $phone);
        $sheet->setCellValue('K'.$i, $address);
        $sheet->setCellValue('L'.$i, $arSales["USER_DESCRIPTION"]);
        $i++;
    }


    $httpClient = new \Bitrix\Main\Web\HttpClient();
    $httpClient->setHeader('X-Auth-Token', 'a5TR1Wab6k6JS9ZUeR4qt4JUJ9Jy3+/JHsCwp3ItFOM=');
    $httpClient->setHeader('Content-Type', 'application/vnd.api+json');
    if (!$post)
    	$result = $httpClient->get('https://kaspi.kz/shop/api/v2/orders?page[number]=0&page[size]=1000&filter[orders][state]=DELIVERY&filter[orders][creationDate][$ge]='.$strTime);
	else
    	$result = $httpClient->get('https://kaspi.kz/shop/api/v2/orders?page[number]=0&page[size]=1000&filter[orders][state]=ARCHIVE&filter[orders][creationDate][$ge]='.$strTime.'&filter[orders][creationDate][$le]='.$strTimeTo);

    $obj = json_decode($result);
    $kaspiOrders = array();
    foreach ($obj->data as $key=>$d){
        if (($d->attributes->deliveryAddress->town != 'Алматы' || $d->attributes->isKaspiDelivery) && !$post) continue;
        if (date('d.m.Y', $d->attributes->plannedDeliveryDate/1000) != date('d.m.Y')) continue;

        $kaspiOrders[$key]['DATE'] = date('d.m.Y H:i:s', substr($d->attributes->creationDate, 0, -3));
        $kaspiOrders[$key]['CODE'] = $d->attributes->code;
        $kaspiOrders[$key]['USER'] = $d->attributes->customer->firstName.' '.$d->attributes->customer->lastName;
        $kaspiOrders[$key]['PAYED'] = 'Оплачен';
        $kaspiOrders[$key]['PAY_TYPE'] = ($d->attributes->paymentMode=='PREPAID')?'Предоплата':'Оплата в кредит';
        $kaspiOrders[$key]['PRICE'] = $d->attributes->totalPrice;
        $kaspiOrders[$key]['PHONE'] = $d->attributes->customer->cellPhone;
        $kaspiOrders[$key]['ADDRESS'] = $d->attributes->deliveryAddress->formattedAddress;
        $kaspiOrders[$key]['STATUS'] = $d->attributes->status;
        $kaspiOrders[$key]['DELIVERY_DATE'] = $d->attributes->plannedDeliveryDate;
        $kaspiOrders[$key]['COMMENT'] = $d->attributes->cancellationComment;
    }

    $k = array_unique_key($kaspiOrders, 'PHONE');

    foreach ($k as $elem){
		$sheet->setCellValue('A'.$i, $i-7);
		$sheet->setCellValue('B'.$i, $elem["DATE"]);
        $sheet->setCellValue('C'.$i, date('d.m.Y', $elem["DELIVERY_DATE"]/1000));
        $sheet->setCellValue('D'.$i, 'Kaspi');
		$sheet->setCellValue('E'.$i, $elem["CODE"]);
		$sheet->setCellValue('F'.$i, $elem["USER"]);
		$sheet->setCellValue('G'.$i, $elem["PAYED"]);
		$sheet->setCellValue('I'.$i, $elem["PAY_TYPE"]);
		$sheet->setCellValue('H'.$i, $elem["PRICE"].' тг');
		$sheet->setCellValue('J'.$i, $elem["PHONE"]);
		$sheet->setCellValue('K'.$i, $elem["ADDRESS"]);
		$sheet->setCellValue('L'.$i, $elem["COMMENT"]);
//		$sheet->setCellValue('M'.$i, $elem["STATUS"]);
		$i++;
	}



    $sheet->setCellValue('D2', date('d.m.Y'));
    $writer = new Xlsx($spreadsheet);
    if (!$post)
        $writer->save(date('d.m.Y').'.xlsx');
    else
        $writer->save('orderlist.xlsx');

    if (!$post && !isset($_GET["direct"])){
        $arEventFields = array("SUBJECT" => "Список заказов за ".date('d.m.Y'), "LINK" => 'https://leloo.kz/cron/excel/'.date('d.m.Y').'.xlsx', "EMAIL" => "contentmanager@leloo.kz");
        CEvent::SendImmediate("NEW_ORDER_LIST", "s1", $arEventFields);
	}

    if (!$post)
    	echo date('d.m.Y').'.xlsx'.' created. Link <a href="/cron/excel/'.date('d.m.Y').'.xlsx">download</a>';
    else
        echo json_encode(array('status'=>'success', 'file'=>'/cron/excel/orderlist.xlsx'));

endif;

function array_unique_key($array, $key) {
    $tmp = $key_array = array();
    $i = 0;

    foreach($array as $val) {
        if (!in_array($val[$key], $key_array)) {
            $key_array[$i] = $val[$key];
            $tmp[$i] = $val;
        }
        else{
        	foreach ($tmp as $k=>$t){
        		if ($val["PHONE"] == $t["PHONE"] && $val["ADDRESS"] == $t["ADDRESS"]){
        			$tmp[$k]["PRICE"] += $val["PRICE"];
        			$tmp[$k]["DATE"] = $val["DATE"];
				}
			}
		}
        $i++;
    }
    return $tmp;
}
