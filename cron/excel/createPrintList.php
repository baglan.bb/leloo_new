<?php require $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include.php';
require '../../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if (date('G') < 9 && date('w')==1)
    $day = date('j')-3;
elseif (date('G') < 9)
    $day = date('j')-1;
else
    $day = date('j');
$date = $day.'.'.date('m.Y')." 11:00:00+6";


$dayK = date('j')-13;
$m = date('m');
$Y = date('Y');
if ($dayK < 1){
    $dayK = 31+$dayK;
    $m=date('m')-1;
    if ($m<1){
        $m=12;
        $Y--;
    }
}

$dateK = $dayK.'.'.$m.'.'.$Y." 00:00:00+6";
$strTime = strtotime($dateK)*1000;

if (CModule::IncludeModule("sale")):
    $arFilter = Array(
        ">=DATE_INSERT" => $date,
        "ALLOW_DELIVERY" => "Y",
        "DELIVERY_ID" => 1,
    );
    $rsSales = CSaleOrder::GetList(array("DATE_INSERT" => "DESC"), $arFilter, false, false, '*');


    //Создаем экземпляр класса электронной таблицы
    $spreadsheet = new Spreadsheet();

    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    $spreadsheet = $reader->load("print.xlsx");

    //Получаем текущий активный лист
    $sheet = $spreadsheet->getActiveSheet();

    $i = 1;
    $j = 0;
    while ($arSales = $rsSales->Fetch())
    {
        $arFilter =Array (
            "ORDER_ID" => $arSales["ID"],
            "ORDER_PROPS_ID" => array(6,3,7),
        );
        $db_sales_ord = CSaleOrderPropsValue::GetList(array(), $arFilter);
        $continue = false;
        while ($ar_salesord = $db_sales_ord->Fetch())
        {
            switch ($ar_salesord["ORDER_PROPS_ID"]){
                case 6:
                    if ($ar_salesord["VALUE"] != 3){
                        $continue = true;
                        break 2;
                    }
                    break;
                case 3:
                    $phone = $ar_salesord["VALUE"];
                    break;
                case 7:
                    $address = $ar_salesord["VALUE"];
                    break;
            }
        }
        
        if ($continue)
            continue;

        $sheet->setCellValue('H'.($i+2), $arSales["DATE_INSERT"]);
        $sheet->setCellValue('D'.($i+2), 'Сайт');
        $sheet->setCellValue('B'.($i+2), $arSales["ID"]);
        $sheet->setCellValue('A'.($i+8), $arSales["USER_NAME"].' '.$arSales["USER_LAST_NAME"]);
        $sheet->setCellValue('E'.($i+2), ($arSales["PAYED"]=='Y')?'Оплачен':'Оплата при получении');
        $sheet->setCellValue('A'.($i+5), $phone);
        $sheet->setCellValue('A'.($i+13), $address);
        $j++;
        if ($j%3==0 && $j!=0)
            $i+=15;
        else
            $i+=16;
    }


    $httpClient = new \Bitrix\Main\Web\HttpClient();
    $httpClient->setHeader('X-Auth-Token', 'a5TR1Wab6k6JS9ZUeR4qt4JUJ9Jy3+/JHsCwp3ItFOM=');
    $httpClient->setHeader('Content-Type', 'application/vnd.api+json');
    if (!$post)
    	$result = $httpClient->get('https://kaspi.kz/shop/api/v2/orders?page[number]=0&page[size]=1000&filter[orders][state]=DELIVERY&filter[orders][creationDate][$ge]='.$strTime);
	else
    	$result = $httpClient->get('https://kaspi.kz/shop/api/v2/orders?page[number]=0&page[size]=1000&filter[orders][state]=ARCHIVE&filter[orders][creationDate][$ge]='.$strTime.'&filter[orders][creationDate][$le]='.$strTimeTo);

    $obj = json_decode($result);
    $kaspiOrders = array();
    foreach ($obj->data as $key=>$d){
        if (($d->attributes->deliveryAddress->town != 'Алматы' || $d->attributes->isKaspiDelivery) && !$post) continue;

        $kaspiOrders[$key]['DATE'] = date('d.m.Y H:i:s', substr($d->attributes->creationDate, 0, -3));
        $kaspiOrders[$key]['CODE'] = $d->attributes->code;
        $kaspiOrders[$key]['USER'] = $d->attributes->customer->firstName.' '.$d->attributes->customer->lastName;
        $kaspiOrders[$key]['PAYED'] = 'Оплачен';
        $kaspiOrders[$key]['PAY_TYPE'] = ($d->attributes->paymentMode=='PREPAID')?'Предоплата':'Оплата в кредит';
        $kaspiOrders[$key]['PRICE'] = $d->attributes->totalPrice;
        $kaspiOrders[$key]['PHONE'] = $d->attributes->customer->cellPhone;
        $kaspiOrders[$key]['ADDRESS'] = $d->attributes->deliveryAddress->formattedAddress;
        $kaspiOrders[$key]['STATUS'] = $d->attributes->status;
        $kaspiOrders[$key]['COMMENT'] = $d->attributes->cancellationComment;
    }

    $k = array_unique_key($kaspiOrders, 'PHONE');

    foreach ($k as $elem){
        if (date('d.m.Y', $elem["DELIVERY_DATE"]/1000) != date('d.m.Y')) continue;

        $sheet->setCellValue('H'.($i+2), $elem["DATE"]);
        $sheet->setCellValue('D'.($i+2), 'Kaspi');
        $sheet->setCellValue('B'.($i+2), $elem["CODE"]);
        $sheet->setCellValue('A'.($i+8), $elem["USER"]);
        $sheet->setCellValue('E'.($i+2), $elem["PAYED"]);
        $sheet->setCellValue('A'.($i+5), '8'.$elem["PHONE"]);
        $sheet->setCellValue('A'.($i+13), $elem["ADDRESS"]);
        $j++;
        if ($j%3==0)
            $i+=15;
        else
            $i+=16;
    }



    $writer = new Xlsx($spreadsheet);
    if (!$post)
        $writer->save(date('d.m.Y').'-p.xlsx');
    else
        $writer->save('orderlist.xlsx');

    if (!$post && !isset($_GET["direct"])){
        $arEventFields = array("SUBJECT" => "Таблица для печати на коробки ".date('d.m.Y'), "LINK" => 'https://leloo.kz/cron/excel/'.date('d.m.Y').'-p.xlsx', "EMAIL" => "sklad@leloo.kz");
        CEvent::SendImmediate("NEW_ORDER_LIST", "s1", $arEventFields);
	}

    if (!$post)
        echo date('d.m.Y').'-p.xlsx'.' created. Link <a href="/cron/excel/'.date('d.m.Y').'-p.xlsx">download</a>';
    else
        echo json_encode(array('status'=>'success', 'file'=>'/cron/excel/orderlist.xlsx'));

endif;

function array_unique_key($array, $key) {
    $tmp = $key_array = array();
    $i = 0;

    foreach($array as $val) {
        if (!in_array($val[$key], $key_array)) {
            $key_array[$i] = $val[$key];
            $tmp[$i] = $val;
        }
        else{
        	foreach ($tmp as $k=>$t){
        		if ($val["PHONE"] == $t["PHONE"] && $val["ADDRESS"] == $t["ADDRESS"]){
        			$tmp[$k]["PRICE"] += $val["PRICE"];
        			$tmp[$k]["DATE"] = $val["DATE"];
				}
			}
		}
        $i++;
    }
    return $tmp;
}
