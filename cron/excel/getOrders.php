<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новая страница");
?>
	<form id="getOrdersForm" action="createOrderList.php">
		<div class="form-group">
			<label for="exampleInputEmail1">Дата от</label>
			<input type="date" class="form-control" id="exampleInputEmail1" name="dateFrom">
			<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">Дата до</label>
			<input type="date" class="form-control" name="dateTo">
		</div>
		<button type="submit" class="btn btn-danger">Submit</button>
	</form>

	<!-- Modal -->
	<div class="modal fade" id="getOrdersModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Отчет сформирован</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					...
				</div>
				<div class="modal-footer">
					<a href=""><button type="button" class="btn btn-primary mr-2">Скачать</button></a>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="getOrdersModalWait" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Подождите</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					Идет формирование отчета
				</div>
			</div>
		</div>
	</div>

<script type="text/javascript">
  $( document ).ready(function() {
    $("#getOrdersForm").on( "submit", function( event ) {
      event.preventDefault();
      $('#getOrdersModalWait').modal('show');
      var data= new FormData(this);
      var url= $(this).attr('action');
      $.ajax({
        type: 'POST',
        url: url,
        data: data,
        cache:false,
        contentType:false,
        processData:false,
        success: function (result) {
          var responce = JSON.parse(result);
          $('#getOrdersModalWait').modal('hide');
          $('#getOrdersModal').modal('show');
          $("#getOrdersModal a").attr("href", responce.file);
          $("#getOrdersModal .modal-body").text(responce.file);
        },
        error: function () {
          location.reload();
        }
      });
    })
  });
</script>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>


