<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(!CModule::IncludeModule("iblock"))
    return;

$xw = xmlwriter_open_memory();
xmlwriter_set_indent($xw, 1);
$res = xmlwriter_set_indent_string($xw, ' ');

xmlwriter_start_document($xw, '1.0', 'UTF-8');

// Первый элемент
xmlwriter_start_element($xw, 'kaspi_catalog');

// Атрибут 'att1' для элемента 'tag1'
xmlwriter_start_attribute($xw, 'date');
xmlwriter_text($xw, 'string');
xmlwriter_end_attribute($xw);
xmlwriter_start_attribute($xw, 'date');
xmlwriter_text($xw, 'kaspiShopping');
xmlwriter_end_attribute($xw);
xmlwriter_start_attribute($xw, 'xmlns:xsi');
xmlwriter_text($xw, 'http://www.w3.org/2001/XMLSchema-instance');
xmlwriter_end_attribute($xw);
xmlwriter_start_attribute($xw, 'xsi:schemaLocation');
xmlwriter_text($xw, 'kaspiShopping http://kaspi.kz/kaspishopping.xsd');
xmlwriter_end_attribute($xw);


// Создаем дочерний элемент
xmlwriter_start_element($xw, 'company');
xmlwriter_text($xw, 'ТОО "Best Shop');
xmlwriter_end_element($xw); // tag11
xmlwriter_start_element($xw, 'merchantid');
xmlwriter_text($xw, 'Leloo');
xmlwriter_end_element($xw); // tag11


xmlwriter_end_element($xw); // tag1


// CDATA
xmlwriter_start_element($xw, 'testc');
xmlwriter_write_cdata($xw, "This is cdata content");
xmlwriter_end_element($xw); // testc

xmlwriter_start_element($xw, 'testc');
xmlwriter_start_cdata($xw);
xmlwriter_text($xw, "test cdata2");
xmlwriter_end_cdata($xw);
xmlwriter_end_element($xw); // testc

// Инструкции по обработке
xmlwriter_start_pi($xw, 'php');
xmlwriter_text($xw, '$foo=2;echo $foo;');
xmlwriter_end_pi($xw);

xmlwriter_end_document($xw);

echo xmlwriter_output_memory($xw);

