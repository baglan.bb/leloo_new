<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if (!CModule::IncludeModule("iblock")) {
    return;
}

function getSKU($product)
{
    if (strlen($product["PROPERTY_CML2_ARTICLE_VALUE"])>0) {
        return $product["PROPERTY_CML2_ARTICLE_VALUE"];
    }else {
        return str_ireplace('&','_',mb_strimwidth($product["NAME"],0,30));
    }
}

/* create a dom document with encoding utf8 */
$domtree=new DOMDocument('1.0','UTF-8'); //kaspi
$domtreeHalyk=new DOMDocument('1.0','UTF-8'); //halyk

/* create the root element of the xml tree KASPI*/
$xmlRoot=$domtree->createElement("kaspi_catalog");
$xmlRoot->setAttribute('xmlns','kaspiShopping');
$xmlRoot->setAttribute('xmlns:xs','http://www.w3.org/2001/XMLSchema');
$xmlRoot->setAttribute('xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
$xmlRoot->setAttribute('date','');
$xmlRoot=$domtree->appendChild($xmlRoot);
/* append it to the document created */
$xmlRoot->appendChild($domtree->createElement('company','ТОО "Best Shop"'));
$xmlRoot->appendChild($domtree->createElement('merchantid','Leloo'));
$offers=$domtree->createElement("offers");
$offers=$xmlRoot->appendChild($offers);

/* create the root element of the xml tree HALYK*/
$xmlHalyk=$domtreeHalyk->createElement("goods");
$xmlHalyk=$domtreeHalyk->appendChild($xmlHalyk);

//Собираем данные о товарах
$iblock_id=11;
$arSelect=["IBLOCK_ID","ID","NAME","PROPERTY_CML2_ARTICLE","catalog_PRICE_1"];
$arFilter=["IBLOCK_ID"=>$iblock_id,"ACTIVE"=>"Y","!PROPERTY_KASPI_VALUE"=>"Да"];
$res=CIBlockElement::GetList([],$arFilter,false,false,$arSelect);
while ($arItem=$res->Fetch()) {
    $arIds[]=$arItem['ID'];    //формируем массив ID элементов - он нам понадобится для красивой выборки ТП
    $arProduct[$arItem['ID']]=$arItem;    //формируем массив всех товаров, к нему будем прикреплять ТП
}
$res=CCatalogSKU::getOffersList(
    $arIds,    // массив ID товаров
    $iblockID=$iblock_id,
    // указываете ID инфоблока только в том случае, когда ВЕСЬ массив товаров из одного инфоблока и он известен
    $skuFilter=["ACTIVE"=>"Y"],    // дополнительный фильтр предложений. по умолчанию пуст.
    $fields=["PROPERTY_OBYEM","NAME","catalog_PRICE_1"],
    // массив полей предложений. даже если пуст - вернет ID и IBLOCK_ID
    $propertyFilter=[]
);
foreach ($res as $key=>$arItem) {
    $arProduct[$key]["OFFERS"]=$arItem;
}

foreach ($arProduct as $product) {
    if ($product["OFFERS"]) {
        foreach ($product["OFFERS"] as $off) {
            /* offer Kaspi */
            $offer = $domtree->createElement("offer");
            $offer = $offers->appendChild($offer);
            $offer->setAttribute('sku', (strlen($product["PROPERTY_CML2_ARTICLE_VALUE"])>0?$product["PROPERTY_CML2_ARTICLE_VALUE"]:str_ireplace('&','_',$product["NAME"])).'-'.$off["PROPERTY_OBYEM_VALUE"]);
            $offer->appendChild($domtree->createElement('model',str_ireplace('&','_',$product["NAME"]).'-'.$off["PROPERTY_OBYEM_VALUE"]));
            $availabilities = $domtree->createElement("availabilities");
            $availabilities = $offer->appendChild($availabilities);
            $availability = $domtree->createElement("availability");
            $availability = $availabilities->appendChild($availability );
            $availability->setAttribute('storeId', 'PP1');
            $availability->setAttribute('available', $off["CATALOG_AVAILABLE"]=="Y"?"yes":"no");
            $offer->appendChild($domtree->createElement('price',round($off["CATALOG_PRICE_1"])));

            //good Halyk
            if($off["CATALOG_PRICE_1"]<1 || $off["CATALOG_QUANTITY"]<1) continue;

            $good = $domtreeHalyk->createElement("good");
            $good = $xmlHalyk->appendChild($good);
            $good->setAttribute('sku', getSKU($product).'-'.$off["PROPERTY_OBYEM_VALUE"]);
            $good->appendChild($domtreeHalyk->createElement('name',str_ireplace(['&','"','/'],['&amp;','&quot;','_'],mb_strimwidth($product["NAME"], 0, 245, "..")).'-'.$off["PROPERTY_OBYEM_VALUE"]));
            $good->appendChild($domtreeHalyk->createElement('category', ''));
            $stocks = $domtreeHalyk->createElement("stocks");
            $stocks = $good->appendChild($stocks);
            $stock = $domtreeHalyk->createElement("stock");
            $stock = $stocks->appendChild($stock );
            $stock->setAttribute('availability', $off["CATALOG_QUANTITY"]);
            $stock->setAttribute('storeId', 'leloo_pp1');
            $good->appendChild($domtreeHalyk->createElement('price',round($off["CATALOG_PRICE_1"])));
            $good->appendChild($domtreeHalyk->createElement('loanPeriod',3));
        }
    }else {
        /* offer */
        $offer=$domtree->createElement("offer");
        $offer=$offers->appendChild($offer);
        $offer->setAttribute('sku',$product["PROPERTY_CML2_ARTICLE_VALUE"]);
        $offer->appendChild($domtree->createElement('model',$product["NAME"]));
        $availabilities=$domtree->createElement("availabilities");
        $availabilities=$offer->appendChild($availabilities);
        $availability=$domtree->createElement("availability");
        $availability=$availabilities->appendChild($availability);
        $availability->setAttribute('storeId','PP1');
        $availability->setAttribute('available',$product["CATALOG_AVAILABLE"]=="Y"?"yes":"no");
        $offer->appendChild($domtree->createElement('price',round($product["CATALOG_PRICE_1"])));

        //good Halyk
        if($product["CATALOG_PRICE_1"]<1 || $product["CATALOG_QUANTITY"]<1) continue;

        $good=$domtreeHalyk->createElement("good");
        $good=$xmlHalyk->appendChild($good);
        $good->setAttribute('sku',getSKU($product));
        $good->appendChild($domtreeHalyk->createElement('name',
            str_ireplace(['&','"','/'],['&amp;','&quot;','_'],mb_strimwidth($product["NAME"],0,250,"..."))));
        $good->appendChild($domtreeHalyk->createElement('category',''));
        $stocks=$domtreeHalyk->createElement("stocks");
        $stocks=$good->appendChild($stocks);
        $stock=$domtreeHalyk->createElement("stock");
        $stock=$stocks->appendChild($stock);
        $stock->setAttribute('availability',$product["CATALOG_QUANTITY"]);
        $stock->setAttribute('storeId','leloo_pp1');
        $good->appendChild($domtreeHalyk->createElement('price',round($product["CATALOG_PRICE_1"])));
        $good->appendChild($domtreeHalyk->createElement('loanPeriod',3));
    }
}

$domtree->save('kaspi.xml');
$domtreeHalyk->save('halyk.xml');

echo 'done!';
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");