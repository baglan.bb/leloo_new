<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if (!CModule::IncludeModule("iblock")) {
    return;
}

function getSKU($product)
{
    if (strlen($product["PROPERTY_CML2_ARTICLE_VALUE"])>0) {
        return $product["PROPERTY_CML2_ARTICLE_VALUE"];
    }else {
        return str_ireplace('&','_',mb_strimwidth($product["NAME"],0,30));
    }
}

/* create a dom document with encoding utf8 */
$domtree=new DOMDocument('1.0','UTF-8'); //google

/* create the root element of the xml tree KASPI*/
$xmlRoot=$domtree->createElement("rss");
$xmlRoot->setAttribute('xmlns:g','http://base.google.com/ns/1.0');
$xmlRoot->setAttribute('version','2.0');

$xmlRoot=$domtree->appendChild($xmlRoot);
$channel=$domtree->createElement("channel");
$channel=$xmlRoot->appendChild($channel);
$title = $domtree->createElement("title", "Google Merchant");
$title = $channel->appendChild($title);
$link = $domtree->createElement("link", "https://leloo.kz/");
$link = $channel->appendChild($link);
$description = $domtree->createElement("description", "leloo.kz");
$description = $channel->appendChild($description);


//Собираем данные о товарах
$iblock_id=11;
$arSelect=["IBLOCK_ID","ID","NAME","PROPERTY_CML2_ARTICLE","PROPERTY_BREND","catalog_PRICE_1", "IBLOCK_SECTION_ID", "DETAIL_PICTURE", "DETAIL_TEXT", "CODE", "DETAIL_PAGE_URL"];
$arFilter=["IBLOCK_ID"=>$iblock_id,"ACTIVE"=>"Y","!PROPERTY_KASPI_VALUE"=>"Да"];
$res=CIBlockElement::GetList([],$arFilter,false,false,$arSelect);
while ($arItem=$res->Fetch()) {
    $arIds[]=$arItem['ID'];    //формируем массив ID элементов - он нам понадобится для красивой выборки ТП
    $arProduct[$arItem['ID']]=$arItem;    //формируем массив всех товаров, к нему будем прикреплять ТП

    $section = CIBlockSection::GetByID($arItem['IBLOCK_SECTION_ID']);
    if($ar_section = $section->GetNext())
        $arProduct[$arItem['ID']]['LINK'] = $ar_section['SECTION_PAGE_URL'];
    //TODO IBLOCK_SECTION_ID get section code and paste to link
}

$res=CCatalogSKU::getOffersList(
    $arIds,    // массив ID товаров
    $iblockID=$iblock_id,
    // указываете ID инфоблока только в том случае, когда ВЕСЬ массив товаров из одного инфоблока и он известен
    $skuFilter=["ACTIVE"=>"Y"],    // дополнительный фильтр предложений. по умолчанию пуст.
    $fields=["PROPERTY_OBYEM","NAME","catalog_PRICE_1","PREVIEW_TEXT"],
    // массив полей предложений. даже если пуст - вернет ID и IBLOCK_ID
    $propertyFilter=[]
);
foreach ($res as $key=>$arItem) {
    $arProduct[$key]["OFFERS"]=$arItem;
}

foreach ($arProduct as $product) {
    if ($product["OFFERS"]) {
        foreach ($product["OFFERS"] as $off) {
            $offer = $domtree->createElement("item");
            $offer = $channel->appendChild($offer);
            $offer->appendChild($domtree->createElement('title',str_ireplace('&','_',$product["NAME"]).'-'.$off["PROPERTY_OBYEM_VALUE"]));
            $offer = $channel->appendChild($offer);
            $offer->appendChild($domtree->createElement('link','https://leloo.kz'.$product["LINK"].$product["CODE"].'/'));
            $offer = $channel->appendChild($offer);
            $offer->appendChild($domtree->createElement('description',$product["DETAIL_TEXT"]));
            $offer = $channel->appendChild($offer);
            $offer->appendChild($domtree->createElement('g:image_link','https://leloo.kz'.CFile::GetPath($product["DETAIL_PICTURE"])));
            $offer = $channel->appendChild($offer);
            $offer->appendChild($domtree->createElement('g:price',$off["CATALOG_PRICE_1"].' '.$off['CATALOG_CURRENCY_1']));
            $offer = $channel->appendChild($offer);
            $offer->appendChild($domtree->createElement('g:condition','new'));
            $offer = $channel->appendChild($offer);
            $offer->appendChild($domtree->createElement('g:id',getSKU($product).'-'.$off["PROPERTY_OBYEM_VALUE"]));
            $offer = $channel->appendChild($offer);
            $offer->appendChild($domtree->createElement('g:brand',$product["PROPERTY_BREND_VALUE"]));
            $offer = $channel->appendChild($offer);
            $offer->appendChild($domtree->createElement('g:mpn',getSKU($product).'-'.$off["PROPERTY_OBYEM_VALUE"]));
            $offer = $channel->appendChild($offer);
            $offer->appendChild($domtree->createElement('g:availability',$off["CATALOG_AVAILABLE"]=="Y"?"in_stock":"out_of_stock"));

            $ship = $domtree->createElement("g:shipping");
            $ship = $offer->appendChild($ship);
            $ship->appendChild($domtree->createElement('g:country','KZ'));
        }
    }else {
        $offer = $domtree->createElement("item");
        $offer = $channel->appendChild($offer);
        $offer->appendChild($domtree->createElement('title',str_ireplace('&','_',$product["NAME"])));
        $offer = $channel->appendChild($offer);
        $offer->appendChild($domtree->createElement('link','https://leloo.kz'.$product["LINK"].$product["CODE"].'/'));
        $offer = $channel->appendChild($offer);
        $offer->appendChild($domtree->createElement('description',$product["DETAIL_TEXT"]));
        $offer = $channel->appendChild($offer);
        $offer->appendChild($domtree->createElement('g:image_link','https://leloo.kz'.CFile::GetPath($product["DETAIL_PICTURE"])));
        $offer = $channel->appendChild($offer);
        $offer->appendChild($domtree->createElement('g:price',$product["CATALOG_PRICE_1"].' '.$product['CATALOG_CURRENCY_1']));
        $offer = $channel->appendChild($offer);
        $offer->appendChild($domtree->createElement('g:condition','new'));
        $offer = $channel->appendChild($offer);
        $offer->appendChild($domtree->createElement('g:id',getSKU($product)));
        $offer = $channel->appendChild($offer);
        $offer->appendChild($domtree->createElement('g:brand',$product["PROPERTY_BREND_VALUE"]));
        $offer = $channel->appendChild($offer);
        $offer->appendChild($domtree->createElement('g:mpn',getSKU($product)));
        $offer = $channel->appendChild($offer);
        $offer->appendChild($domtree->createElement('g:availability',$product["CATALOG_AVAILABLE"]=="Y"?"in_stock":"out_of_stock"));

        $ship = $domtree->createElement("g:shipping");
        $ship = $offer->appendChild($ship);
        $ship->appendChild($domtree->createElement('g:country','KZ'));
    }
}

$domtree->save('google.xml');

echo 'done!';
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
