<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
CModule::IncludeModule("sale");
CModule::IncludeModule("paybox.pay");
$APPLICATION->RestartBuffer();

/*
 * Configuration and parameters
 */
$strScriptName = PayBoxSignature::getOurScriptName();

$arrRequest = PayBoxIO::getRequest();
$accountNumber = $arrRequest['pg_order_id'];
$payment = null;
$ar = explode("/", $accountNumber);
$orderID = $ar[0];

/** @var \Bitrix\Sale\Order $order */
$order = \Bitrix\Sale\Order::load($orderID);

/** @var \Bitrix\Sale\PaymentCollection $paymentCollection */
$paymentCollection = $order->getPaymentCollection();

foreach ($paymentCollection as $orderPayment)
{
    if ($orderPayment->getField('ACCOUNT_NUMBER') == $accountNumber)
    {
        $payment = $orderPayment;
    }
}

/**/

if ($payment == null)
{
    die("Платеж не найден");
}

$paymentSystrem = $payment->getPaySystem();
$paymentSystremName = $payment->getPaymentSystemName();

$objShop = CSalePaySystemAction::GetList('', array("PS_NAME" => $paymentSystremName));
$arrShop = $objShop->Fetch();
if (!empty($arrShop))
{
    $arrShopParams = unserialize($arrShop['PARAMS']);
}
else
{
    PayBoxIO::makeResponse($strScriptName, '', 'error',
        'Please re-configure the module PayBox.PAY in Bitrix CMS. The payment system should have a name ' . $paymentSystremName);
}

$strSecretKey = $arrShopParams['SHOP_SECRET_KEY']['VALUE'];

$strSalt = $arrRequest["pg_salt"];

$nOrderAmount = $arrRequest["pg_amount"];
$orderID = intval($arrRequest["pg_order_id"]);
$strStatusFailed = $arrRequest["STATUS_FAILED"];

foreach($arrRequest as $key => $value)
{
    if(substr($key, 0, 3) !== "pg_")
    {
        unset($arrRequest[$key]);
    }
}
/*
 * Signature
 */

if (!PayBoxSignature::check($arrRequest['pg_sig'], $strScriptName, $arrRequest, $strSecretKey))
{
    PayBoxIO::makeResponse($strScriptName, $strSecretKey, 'error', 'signature is not valid', $strSalt);
}

if (!($arrOrder = CSaleOrder::GetByID($orderID)))
{
    PayBoxIO::makeResponse($strScriptName, $strSecretKey, 'error', 'order not found', $strSalt);
}

if ($nOrderAmount != $arrOrder['PRICE'])
{
    PayBoxIO::makeResponse($strScriptName, $strSecretKey, 'error', 'amount is not correct', $strSalt);
}

if ($arrRequest["pg_result"] == 1)
{
    if ($arrOrder['PAYED'] == "Y")
    {
        PayBoxIO::makeResponse($strScriptName, $strSecretKey, "ok", "Order alredy payed", $strSalt);
    }

    if ($arrOrder['CANCELED'] == "Y")
    {
        CSaleOrder::Update($orderID, array(
            'STATUS_ID' => $strStatusFailed,
            'PS_STATUS' => $strStatusFailed,
            'PS_STATUS_CODE' => "0",
            'PS_SUM' => $arrRequest['pg_amount'],
            'PS_CURRENCY' => $arrRequest['pg_currency'],
            'PS_RESPONSE_DATE' => Date(CDatabase::DateFormatToPHP(CLang::GetDateFormat("FULL", LANG))),
        ));

        PayBoxIO::makeResponse($strScriptName, $strSecretKey, 'rejected',
            'Order canceled', $strSalt);

        return false;
    }

    $r = $payment->setField('PAID', "Y");

    if(!$r)
    {
        PayBoxIO::makeResponse($strScriptName, $strSecretKey, "error", "Order can\'t be payed", $strSalt);
    }
    else
    {
//        if($paymentCollection->isPaid())
//        {
//            /** @var \Bitrix\Sale\ShipmentCollection $shipmentCollection */
//            $shipmentCollection = $order->getShipmentCollection();
//            foreach($shipmentCollection as $shipment)
//            {
//                if($shipment->isSystem())
//                    continue;
//
//                $setResult = $shipment->setField("DEDUCTED", "Y");
//            }
//            $shipmentCollection->save();
//
//        }
        $order->setField('STATUS_ID', "P"); //Переводим заказ в статус "Опалчен, формируется к отправке"
        $order->save();
        updDeal($orderID, 'Оплачен картой', "PREPARATION");
        PayBoxIO::makeResponse($strScriptName, $strSecretKey, "ok", "Order payed", $strSalt);
    }
}
/*
 * Order cancel
 */
else
{
    if ($arrOrder['CANCELED'] == "Y")
    {
        PayBoxIO::makeResponse($strScriptName, $strSecretKey, 'ok',
            'Order alredy canceled', $strSalt);
    }

    if ($arrOrder['PAYED'] == "Y")
    {
        PayBoxIO::makeResponse($strScriptName, $strSecretKey, "error",
            "Order alredy paid", $strSalt);
    }

    if (!CSaleOrder::CancelOrder($nOrderId, "Y", !empty($arrRequest['pg_failure_description']) ? $arrRequest['pg_failure_description'] : ''))
    {
        PayBoxIO::makeResponse($strScriptName, $strSecretKey, "error",
            "Order can\'t be cancel", $strSalt);
    }

    CSaleOrder::Update($nOrderId, array(
        'STATUS_ID' => $strStatusFailed,
        'PS_STATUS' => $strStatusFailed,
        'PS_STATUS_CODE' => "1",
        'PS_SUM' => $arrRequest['pg_amount'],
        'PS_CURRENCY' => $arrRequest['pg_currency'],
        'PS_RESPONSE_DATE' => Date(CDatabase::DateFormatToPHP(CLang::GetDateFormat("FULL", LANG))),
    ));

    PayBoxIO::makeResponse($strScriptName, $strSecretKey, "ok",
        "Order cancel", $strSalt);
}
