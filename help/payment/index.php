<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Удобная оплата в интернет магазине Leloo.kz проходит через карту любого банка. Заходи и читай подробнее, как оплатить.Быстрая доставка по всему Казахстану!");
$APPLICATION->SetPageProperty("keywords", "оплата в интернет магазине, оплата косметики, правила оплаты, корейская косметика");
$APPLICATION->SetPageProperty("title", "Способ оплаты");
$APPLICATION->SetTitle("Способы оплаты - помощь Leloo.kz");
?><h4>Оплата заказа</h4>
 В интернет-магазине покупка товара возможна только по предоплате. Оплата производится через систему PayBox.Cистема доступна для держателей любых&nbsp;банковских платежных карт&nbsp;Visa и MasterCard, выпущенных в любом банке.&nbsp; <br>
 <br>
 <br>
<h4>
Банковские карты </h4>
<h5><img width="60" src="https://paybox.money/images/site/services/methods/visa.png" height="21"">&nbsp; &nbsp;<img width="50" alt="mastercard_PNG7.png" src="/upload/medialibrary/b0f/b0f97b7103f7de1705f3e2b5f8028614.png" height="30" title="mastercard_PNG7.png">&nbsp;&nbsp;&nbsp;<img width="50" alt="logo-maestro.png" src="/upload/medialibrary/45c/45c0d24a530e4e5008f637d77d82207b.png" height="31" title="logo-maestro.png"> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;</h5>
 <br>
<h4>Безопасность передаваемой информации</h4>
<p>
	 Оплачивать товары и услуги с помощью PayBox.money абсолютно безопасно. При оплате информация передается в зашифрованном виде по протоколу SSL 3.0 и сохраняются только на специализированном сервере платежного метода. Таким образом, интернет-магазин не имеет доступа к Вашим платежным данным.
</p>
<div>
	 Покупатель вводит свои платежные реквизиты не на сайте интернет-магазина, а непосредственно в системе авторизации PayBox, следовательно, платежные реквизиты карточки покупателя не будут доступны никому, кроме сотрудников банка, имеющих соответствующие полномочия.<br>
</div>
 <br>
 <br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>