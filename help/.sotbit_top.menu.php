<?
$aMenuLinks = Array(
    Array(
        "Оплата",
        "/help/payment/index.php",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Возврат товара",
        "/help/return/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Доставка",
        "/help/delivery/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Как оформить заказ",
        "/help/checkout/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Купить в кредит/рассрочку",
        "/help/kaspi/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Публичная оферта",
        "/help/oferta/",
        Array(),
        Array(),
        ""
    )
);

global $USER;
if ($USER->IsAdmin()){
    array_push($aMenuLinks,
        Array(
            "Сформировать заказы",
            "/cron/excel/getOrders.php",
            Array(),
            Array(),
            ""
        ),
        Array(
            "Обновить скидки",
            "/cron/updateDiscount.php",
            Array(),
            Array(),
            ""
        ),
        Array(
            "Обновить kaspi.xml",
            "/cron/kaspi.php",
            Array(),
            Array(),
            ""
        )
    );
}

?>