<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Интернет-магазин \"Leloo\"");
$APPLICATION->SetTitle("Как купить в кредит/ рассрочку");
?><style>
	img{
		max-width: 100%;
	}
</style>
<p>
	 Интернет-магазин Leloo.kz, cовместно с&nbsp;АО "Kaspi Bank", рад сообщить о возможности покупки наших товаров в кредит, либо в рассрочку!&nbsp;Для того, чтобы купить товар с нашего сайта в рассрочку или в кредит, Вам нужно пройти&nbsp;быструю процедуру оформления заказа. Как это делается:<br>
</p>
<h4><span style="font-size: 15pt;"><br>
 </span></h4>
<h4><span style="font-size: 15pt;">1. Выберите подходящий&nbsp;товар на нашем сайте и нажмите кнопку “Купить в кредит”</span></h4>
<p>
	 Для этого Вам нужно выбрать товар и перейти в "карточку товара".
</p>
 <span> <source srcset="/upload/medialibrary/k/01.jpg" media="(max-width: 620px)"> <img src="/upload/medialibrary/98c/98c40b758cf8c7b6c995c49a7f691b75.png"> </span>
<p>
	 После этого Вы перейдете на сайт Kaspi.kz, где нужно будет заполнить данные.<br>
</p>
<h4><span style="font-size: 15pt;"><br>
 </span></h4>
<h4><span style="font-size: 15pt;">2. Оформите вашу заявку на сайте kaspi.kz</span></h4>
<p>
</p>
<p>
	 В первом шаге нужно выбрать способ доставки. Это может быть "Самовывоз", либо доставка "Курьером".
</p>
 <span> <source srcset="/upload/medialibrary/k/02.jpg" media="(max-width: 620px)"> <img src="/upload/medialibrary/8b8/8b832ae9ddbbb959043fd96ce2534c48.png"> </span>
<p>
	 Если Вы выбрали доставку курьером, то&nbsp;необходимо заполнить адрес доставки.
</p>
<p>
 <span style="font-size: 15pt;"><b><br>
 </b></span>
</p>
<p>
 <span style="font-size: 15pt;"><b>2.1. Во втором шаге Вам необходимо выбрать способ оплаты "В рассрочку" (до 3-х платежей), либо "В кредит" (на 6 месяцев).&nbsp;</b></span><br>
</p>
 <span> <source srcset="/upload/medialibrary/k/03.jpg" media="(max-width: 620px)"> <img src="/upload/medialibrary/7c7/7c770706b153d343f686ec24313ed3fa.png"> </span>
<p>
 <span style="font-size: 15pt;"><b><br>
 </b></span>
</p>
<p>
 <span style="font-size: 15pt;"><span style="font-size: 13pt;">Далее Вам необходимо ввести свои контактные данные: Телефон, Имя и ИИН.</span><br>
 </span>
</p>
 <span> <source srcset="/upload/medialibrary/k/04.jpg" media="(max-width: 620px)"> <img src="/upload/medialibrary/f65/f6517a9681302d198e3245ea267653c3.png"> </span>
<p>
	 И после этого Вам необходимо подтвердить Ваш телефон, введя код, который придет по SMS.<br>
</p>
<p>
 <img width="809" src="/upload/medialibrary/069/0695c37bdee38f5d7c02ab8db6742d5d.png" height="411"><br>
</p>
<p>
 <br>
</p>
<h4><span style="font-size: 15pt;">3. </span>После одобрения, ожидайте курьера - при выборе "доставки", либо при "самовывозе" можете забрать в удобный для вас день с ПН - ПТ с 09.00 до 18.00.</h4>
<p>
</p>
<hr style="height: 0px;">
<p>
	 * Если Вы оформляете покупку в кредит через Каспи&nbsp;Банк&nbsp;в первый раз, Вам придется заключить договор о кредитовании в одном из&nbsp;отделений банка. Если Вы уже пользовались услугами Банка при кредитовании и у Вас хорошая кредитная история, Вам одобрят покупку продукции. Таким образом, Вы можете оформить кредит, не отходя от компьютера, за 5 минут, а мы доставим его&nbsp;до Вашей двери!
</p>
<p>
 <br>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>