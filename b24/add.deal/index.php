<?
//require_once (__DIR__.'/crest.php');
require $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include.php';
date_default_timezone_set('Asia/Almaty');
$date = date('d.m.Y H:i:s', strtotime('-899 seconds'));
$strTime = strtotime($date)*1000;
$dateD = date('d.m.Y H:i:s', strtotime('-1 day'));
$strTimeD = strtotime($dateD)*1000;

$httpClient = new \Bitrix\Main\Web\HttpClient();
$httpClient->setHeader('X-Auth-Token', 'a5TR1Wab6k6JS9ZUeR4qt4JUJ9Jy3+/JHsCwp3ItFOM=');
$httpClient->setHeader('Content-Type', 'application/vnd.api+json');

$result = $httpClient->get('https://kaspi.kz/shop/api/v2/orders?page[number]=0&page[size]=1000&filter[orders][state]=NEW&filter[orders][creationDate][$ge]='.$strTime);
$kaspiOrders = decodeKaspiData($result); //тут массив с продуктами должен содержать id продуктов как БУС
foreach ($kaspiOrders as $order){
    addDealToCrm($order, "PREPARATION", $order['PRODUCTS'], true);
}

$signing = $httpClient->get('https://kaspi.kz/shop/api/v2/orders?page[number]=0&page[size]=1000&filter[orders][state]=SIGN_REQUIRED&filter[orders][creationDate][$ge]='.$strTime);
$signingOrders = decodeKaspiData($signing); //тут массив с продуктами должен содержать id продуктов как БУС
foreach ($signingOrders as $orderSign){
    addDealToCrm($orderSign, "NEW", $orderSign['PRODUCTS'], true);
}
die();

//проверяем заказы на каски со статусом к доставке
$delivery = $httpClient->get('https://kaspi.kz/shop/api/v2/orders?page[number]=0&page[size]=1000&filter[orders][state]=DELIVERY&filter[orders][creationDate][$ge]='.$strTimeD);
$deliveryOrders = decodeKaspiData($delivery);
$orderIds = [];
foreach ($deliveryOrders as $order){
    array_push($orderIds, $order['ID']);
}
//ищем заказы к доставке в CRM
$getDial = CRest::call(
    "crm.deal.list",
    [
        'order'=> [ "ID"=> "ASC" ],
        'filter'=> [ "UF_CRM_1608103552942"=> $orderIds ],
        'select'=> [ "ID", "UF_CRM_1608103552942" ]
    ]
);
//если нашли, меняем статус, удаляем из массива
if ($getDial['total']>0){
    foreach ($getDial['result'] as $dial){
        $r = CRest::call(
            'crm.deal.update',
            [
                "id" => $dial['ID'],
                "fields" =>
                    [
                        "STAGE_ID" => "EXECUTING"
                    ]
            ]
        );
        if ($r['result'])
            foreach($deliveryOrders as $key => $item){
                if ($item['ID'] == $dial['UF_CRM_1608103552942'])
                    unset($deliveryOrders[$key]);
            }
    }
}
//оставшееся добавляем сразу к доставке
foreach ($deliveryOrders as $order){
    addDealToCrm($order, "EXECUTING", $order['PRODUCTS'], true);
}