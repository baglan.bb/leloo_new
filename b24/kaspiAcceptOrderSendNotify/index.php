<?
//require_once (__DIR__.'/crest.php');
require $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include.php';

$url='https://kaspi.kz/shop/api/v2/orders';
$httpClient=new \Bitrix\Main\Web\HttpClient();
$httpClient->setHeader('X-Auth-Token','a5TR1Wab6k6JS9ZUeR4qt4JUJ9Jy3+/JHsCwp3ItFOM=');
$httpClient->setHeader('Content-Type','application/vnd.api+json');

$payload=[
    'data'=>
        [
            "type"=>"orders",
            "id"=>$_GET['id'],
            "attributes"=>[
                "status"=>"ACCEPTED_BY_MERCHANT"
            ]
        ]
];

$jsonPayload=json_encode($payload);

$response=$httpClient->post($url,$jsonPayload);
$res = json_decode($response);

$result = CRest::call(
		'im.notify',
		[
		    'to'=> 1092,
            'message'=> 'Заказ id: '.$res['data']['id'].' #BR# Статус - '.$res['data']['attributes']['status']=='ACCEPTED_BY_MERCHANT'?'Принят':$res['data']['attributes']['status'],
            'type' => 'SYSTEM'
        ]
	);